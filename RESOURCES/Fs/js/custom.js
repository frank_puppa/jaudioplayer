var last_myhref;
var constToastCloseTimeoutDEFINE = 1000; //msec
var constToastCloseTimeoutLongDEFINE = 1500; //msec
var constToastCloseTimeout = constToastCloseTimeoutDEFINE;


var mp3Genres = ["Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", "New Age", "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial", "Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", "Euro-Techno", "Ambient", "Trip Hop", "Vocal", "Jazz+Funk", "Fusion", "Trance", "Classical", "Instrumental", "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise", "Alternative Rock", "Bass", "Soul", "Punk", "Space", "Meditative", "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic", "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta Rap", "Top 40", "Christian Rap", "Pop/Punk", "Jungle", "Native American", "Cabaret", "New Wave", "Phychedelic", "Rave", "Showtunes", "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk", "Folk/Rock", "National Folk", "Swing", "Fast-Fusion", "Bebob", "Latin", "Revival", "Celtic", "Blue Grass", "Avantegarde", "Gothic Rock", "Progressive Rock", "Psychedelic Rock", "Symphonic Rock", "Slow Rock", "Big Band", "Chorus", "Easy Listening", "Acoustic", "Humour", "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass", "Primus", "Porn Groove", "Satire", "Slow Jam", "Club", "Tango", "Samba", "Folklore", "Ballad", "power Ballad", "Rhythmic Soul", "Freestyle", "Duet", "Punk Rock", "Drum Solo", "A Capella", "Euro-House", "Dance Hall", "Goa", "Drum & Bass", "Club-House", "Hardcore", "Terror", "indie", "Brit Pop", "Negerpunk", "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal", "Black Metal", "Crossover", "Comteporary Christian", "Christian Rock", "Merengue", "Salsa", "Trash Metal", "Anime", "JPop", "Synth Pop" ];
var digitalIOMode = ["Free", "Player", "Binary Code", "Playlist Direct", "Keypad", "Suspension"];
var digitalOutputMode = ["Remote", "Play", "Blink", "Fault" ,"Timer"];
// --------------------------

var musicitemsobj = {};
var spotitemsobj = {};
var sensoritemsobj = {};
var plitemsobj = {};

var lastSelectedPl = -1;

var schedulerData = {};
var lastSelectedSched = -1;

// --------------------------

function DisplayDstSwitchDates()
   {
        var year = new Date().getYear();
        if (year < 1000)
            year += 1900;
        var firstSwitch = 0;
        var secondSwitch = 0;
        var lastOffset = 99;
        // Loop through every month of the current year
        for (i = 0; i < 12; i++)
        {
            // Fetch the timezone value for the month
            var newDate = new Date(Date.UTC(year, i, 0, 0, 0, 0, 0));
            var tz = -1 * newDate.getTimezoneOffset() / 60;
            // Capture when a timzezone change occurs
            if (tz > lastOffset)
                firstSwitch = i-1;
            else if (tz < lastOffset)
                secondSwitch = i-1;
            lastOffset = tz;
        }
        // Go figure out date/time occurences a minute before
        // a DST adjustment occurs
        var secondDstDate = FindDstSwitchDate(year, secondSwitch);
        var firstDstDate = FindDstSwitchDate(year, firstSwitch);
        if (firstDstDate == null && secondDstDate == null)
            return 'Daylight Savings is not observed in your timezone.';
        else
            return 'Last minute before DST change occurs in ' +
               year + ': ' + firstDstDate + ' and ' + secondDstDate;
    }//
    function FindDstSwitchDate(year, month)
    {
        // Set the starting date
        var baseDate = new Date(Date.UTC(year, month, 0, 0, 0, 0, 0));
        var changeDay = 0;
        var changeMinute = -1;
        var baseOffset = -1 * baseDate.getTimezoneOffset() / 60;
        var dstDate;
        // Loop to find the exact day a timezone adjust occurs
        for (day = 0; day < 50; day++)
        {
            var tmpDate = new Date(Date.UTC(year, month, day, 0, 0, 0, 0));
            var tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;
            // Check if the timezone changed from one day to the next
            if (tmpOffset != baseOffset)
            {
                var minutes = 0;
                changeDay = day;
                // Back-up one day and grap the offset
                tmpDate = new Date(Date.UTC(year, month, day-1, 0, 0, 0, 0));
                tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;
                // Count the minutes until a timezone chnage occurs
                while (changeMinute == -1)
                {
                    tmpDate = new Date(Date.UTC(year, month, day-1, 0, minutes, 0, 0));
                    tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;
                    // Determine the exact minute a timezone change
                    // occurs
                    if (tmpOffset != baseOffset)
                    {
                        // Back-up a minute to get the date/time just
                        // before a timezone change occurs
                        tmpOffset = new Date(Date.UTC(year, month,
                                             day-1, 0, minutes-1, 0, 0));
                        changeMinute = minutes;
                        break;
                    }
                    else
                        minutes++;
                }
                // Add a month (for display) since JavaScript counts
                // months from 0 to 11
                dstDate = tmpOffset.getMonth() + 1;
                // Pad the month as needed
                if (dstDate < 10) dstDate = "0" + dstDate;
                // Add the day and year
                dstDate += '/' + tmpOffset.getDate() + '/' + year + ' ';
                // Capture the time stamp
                tmpDate = new Date(Date.UTC(year, month,
                                   day-1, 0, minutes-1, 0, 0));
                dstDate += tmpDate.toTimeString().split(' ')[0];
                return dstDate;


            }
        }
    }

// --------------------------


jQuery.extend({
    stringify  : function stringify(obj) {
        var t = typeof (obj);
        if (t != "object" || obj === null) {
            // simple data type
            if (t == "string") obj = '"' + obj + '"';
            return String(obj);
        } else {
            // recurse array or object
            var n, v, json = [], arr = (obj && obj.constructor == Array);

            for (n in obj) {
                v = obj[n];
                t = typeof(v);
                if (obj.hasOwnProperty(n)) {
                    if (t == "string") v = '"' + v + '"'; else if (t == "object" && v !== null) v = jQuery.stringify(v);
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
            }
            return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
        }
    }
});

$.strPad = function(i,l,s) {
	var o = i.toString();
	if (!s) { s = '0'; }
	while (o.length < l) {
		o = s + o;
	}
	return o;
};

$.toFlags = function(val, bits) {
	return $.strPad(parseInt(val).toString(2),bits).split("").reverse();
}

$.timeTo = function(type, timestr) {
	times = timestr.split(":");

	switch (type) {
		case "h":
			return parseInt(times[0]);
			break;
		case "m":
			return parseInt(times[1]);
			break;
		case "s":
			return parseInt(times[2]);
			break;
	}

	return -1;
}

$.timeToSeconds = function(timestr) {
	times = timestr.split(":");
	if (times.length==3)
		return parseInt(times[0]*3600)+parseInt(times[1]*60)+parseInt(times[2]);
	if (times.length==2)
		return parseInt(times[0]*60)+parseInt(times[1]);
	return times[0];
}

$.secondsToTime = function(seconds, sep) {


	var h = parseInt(seconds/3600);
	seconds = seconds - h*3600;
	var m = parseInt(seconds/60);
	seconds = seconds - m*60;
	if (typeof sep === "undefined")
		return $.strPad(h, 2)+":"+$.strPad(m, 2)+":"+$.strPad(seconds, 2);

	return $.strPad(h, 2)+"h"+$.strPad(m, 2)+"m"+$.strPad(seconds, 2)+"s";


}


$.toast = function(msg) {
	$("#toast").text(msg);
	$("#toast").popup("open");
}

$.strPad = function(i,l,s) {
	var o = i.toString();
	if (!s) { s = '0'; }
	while (o.length < l) {
		o = s + o;
	}
	return o;
}

$(document).on("pagecreate", "#mainpage", function() {
    $(document).on("swipeleft swiperight", "#mainpage", function(e) {
        if ($(".ui-page-active").jqmData("panel") !== "open") {
            if (e.type === "swiperight") {
                $("#pnMenu").panel("open");
            }
        }
    });

    $(document).on("click", ".btApply", function(e) {
        if (confirm("Apply current data?"))
			$.reqSetData();
    });

	$("#toast").on('popupafteropen', function() {
		setTimeout( function(){ $('#toast').popup('close') }, constToastCloseTimeout);
	})

});

$(document).on("pageinit", function() {
	$("a.menulink").each(function () {
		var myhref = $(this).attr("content");
		$(this).click(function() {
			$.ajax(myhref, {
				//cache: false
			})
			.done(function(html) {
				$("#maincontent").html(html);
				// utile per reload
				last_myhref = myhref;
			})
			.error(function() {
				$.toast("Error: '" + myhref + "' not found!");
			})
			.always(function() {
				$("#maincontent").trigger("create");
				$("#pnMenu").panel("close");
			});

		});
	});

});

$(document).on("pagecontainershow", function() {
	$('#maincontent').load("deviceinfo.html");
});

$.reloadPage = function() {
	var result = $.ajax(last_myhref, {
		//cache: false
	})
	.done(function(html) {
		$("#maincontent").html(html);
	})
	.error(function() {
		$.toast("Error: '" + last_myhref + "' not found!");
	})
	.always(function() {
		$("#maincontent").trigger("create");
		$("#pnMenu").panel("close");
	});
	return result;
}

$.globalInit = function() {
	$("#mainpage").trigger("create");	
	
	$("input[data-type='range'].immediate").on("slidestop", function() {
		$.reqSetData(this);
	});

	$("input[data-type='range'].immediate").on("keypress", function(e) {
		if (e.keyCode == 13) {
			var fVal = parseFloat($(this).val());
			if ((fVal>$(this).attr("max")) || (fVal<$(this).attr("min"))) {
				$.toast("Invalid value");
				return false;
			}
			$.reqSetData(this);
		}
	});

	$("select.immediate").change(function() {
		$.reqSetData(this);
	});

	$(".clReload").click(function() {
		$.reloadPage();
	});

	$(".btCancelPopup").once('click', function() {
		$(".mypopup").popup("close");
	});

	$(".clMediaButtons").click(function() {
		reqdata = {TYPE: $(this).attr('cmdtype')};
		switch ($(this).attr('cmdtype')) {
			case 'play':
				var target = $(".clTrackitem.ui-btn-active");
				$.extend(reqdata, {FILE_TYPE: target.attr('FILE_TYPE'), TRACK_IDX: target.attr("idx"), TRACK_NAME: target.attr('trackname')});
				$("#btPause").parent("div").removeClass("ui-disabled");
				$("#btStop").parent("div").removeClass("ui-disabled");
				break;
			case 'startplaylist':
				var target = $("#PL_ITEMS option:selected");
				$.extend(reqdata, {FILE_TYPE: 'playlist', PL_IDX: target.attr("idx"), PL_NAME: target.val()});
				break;
		}

		$.reqData("cmd", reqdata);
	});

    $('.mypopup').draggable({
        cursor: 'move'
    });	
	
}

$.INITdivDeviceFunction = function(val) {
		var DIOMval = $('#DIOM').val();
		$('select#DIOM option').remove();
		switch (parseInt(val)) {
			case 0: //StandardPlayer
				$("#mnPlayer").removeClass('ui-disabled');
				$("#mnPlaylist").addClass('ui-disabled');
				$("#mnPlaylistCreator").addClass('ui-disabled');
				$("#mnScheduler").addClass('ui-disabled');

				$("#divPlayDelay").hide();
				$("#divDigitalIn").hide();
				$("#divRestartPlay").hide();

				$("select#DIOM").append($('<option value="0">').text(digitalIOMode[0]));
				$("select#DIOM").append($('<option value="1">').text(digitalIOMode[1]));
				
				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");

				$("#divDigitalIOMode").show();
				$("#divPowerOnAutoPlay").show();
				$("#divLoopPlay").show();
				$("#divRandomPlay").show();

				$.INITdivDigitalIOMode($('select#DIOM').val());
				break;

			case 1: //PlaylistSequence
				$("#mnPlayer").addClass('ui-disabled');
				$("#mnPlaylist").removeClass('ui-disabled');
				$("#mnPlaylistCreator").removeClass('ui-disabled');
				$("#mnScheduler").addClass('ui-disabled');

				$("#divPlayDelay").hide();
				$("#divDigitalIn").hide();
				$("#divRestartPlay").hide();

				$("select#DIOM").append($('<option value="0">').text(digitalIOMode[0]));
				$("select#DIOM").append($('<option value="1">').text(digitalIOMode[1]));
				
				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");

				$("#divDigitalIOMode").show();
				$("#divPowerOnAutoPlay").show();
				$("#divLoopPlay").show();
				$("#divRandomPlay").show();

				$.INITdivDigitalIOMode($('select#DIOM').val());
				break;
			case 2: //AdvancedPlayer 1
				$("#mnPlayer").removeClass('ui-disabled');
				$("#mnPlaylist").addClass('ui-disabled');
				$("#mnPlaylistCreator").addClass('ui-disabled');
				$("#mnScheduler").addClass('ui-disabled');

				
				$("select#DIOM").append($('<option value="2">').text(digitalIOMode[2]));
				$("select#DIOM").append($('<option value="4">').text(digitalIOMode[4]));
				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");

				$("#divDigitalIOMode").show();
				$("#divPowerOnAutoPlay").show();

				$.INITdivDigitalIOMode($('select#DIOM').val());
				break;
			case 3: //AdvancedPlayer 2
				$("#mnPlayer").addClass('ui-disabled');
				$("#mnPlaylist").removeClass('ui-disabled');
				$("#mnPlaylistCreator").removeClass('ui-disabled');
				$("#mnScheduler").addClass('ui-disabled');

				$("select#DIOM").append($('<option value="2">').text(digitalIOMode[2]));
				$("select#DIOM").append($('<option value="3">').text(digitalIOMode[3]));
				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");


				$("#divDigitalIOMode").show();
				$("#divPowerOnAutoPlay").show();

				$.INITdivDigitalIOMode($('select#DIOM').val());
				break;
			case 4: //Scheduler
				$("#mnPlayer").addClass('ui-disabled');
				$("#mnPlaylist").addClass('ui-disabled');
				$("#mnPlaylistCreator").addClass('ui-disabled');
				$("#mnScheduler").removeClass('ui-disabled');

				$("#divDigitalIn").hide();

				$("#divPlayDelay").hide();
				$("#divDigitalInTimeout").hide();
				$("#divRestartPlay").hide();

				$("#divLoopPlay").hide();
				$("#divRandomPlay").hide();
				$("#divPowerOnAutoPlay").hide();

				$("select#DIOM").append($('<option value="0">').text(digitalIOMode[0]));
				$("select#DIOM").append($('<option value="5">').text(digitalIOMode[5]));
				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");

				break;
			default:
				$("#divDigitalIOMode").hide();
				$("#divPlayDelay").hide();
				$("#divDigitalInTimeout").hide();
				$("#divRestartPlay").hide();
				$("#divLoopPlay").hide();
				$("#divRandomPlay").hide();
				$("#divPowerOnAutoPlay").hide();
				break;
		}

}

$.INITdivDigitalIOMode = function(val) {
		$("#divDigitalIn").show();
		switch (parseInt(val)) {
			case 0: //Free
			case 5: //Suspension
				$("#mnIOSettings").removeClass('ui-disabled');
				$("#divDigitalIn").hide();
				$("#divPlayDelay").hide();
				$("#divDigitalInTimeout").hide();

				$("#divRestartPlay").hide();
				$("#divDigitalInIObits").hide();

				$("#divPowerOnAutoPlay").show();
				break;
			case 1: //Player
				$("#mnIOSettings").removeClass('ui-disabled');
				$("#divDigitalInTimeout").show();
				$("#divPlayDelay").show();
				$("#divLoopPlay").show();
				$("#divRandomPlay").show();
				$("#divPowerOnAutoPlay").show();

				$("#divDigitalInIObits").hide();
				$("#divDigitalInContinuousPlay").hide();
				$("#divDigitalInInterrupt").hide();
				$("#divRestartPlay").hide();
				break;
			case 2: //BinaryCode
				$("#mnIOSettings").removeClass('ui-disabled');
				$("#divPowerOnAutoPlay").hide();
				$("#divLoopPlay").hide();
				$("#divRandomPlay").hide();

				$("#divDigitalInIObits").show();
				$("#divDigitalInTimeout").show();
				$("#divDigitalInContinuousPlay").show();
				$("#divDigitalInInterrupt").show();

				$("#divPlayDelay").show();
				$("#divRestartPlay").show();
				
				$("label[for='DIBC']").text("# bits for Binary Code (1-8)")
				break;
			case 3: //Playlist
				$("#divPowerOnAutoPlay").hide();
				$("#mnIOSettings").removeClass('ui-disabled');

				$("#divDigitalInTimeout").show();
				$("#divDigitalInContinuousPlay").show();
				$("#divDigitalInInterrupt").show();

				$("#divDigitalInIObits").show();
				$("#divPlayDelay").show();
				$("#divRestartPlay").show();
				$("label[for='DIBC']").text("# In for Playlist Direct (1-8)")
				break;
			case 4: //Keypad
				$("#mnIOSettings").addClass('ui-disabled');
				$("#divPowerOnAutoPlay").hide();
				$("#divDigitalInIObits").hide();

				$("#divPlayDelay").show();
				$("#divRestartPlay").show();
				$("#divDigitalInTimeout").show();
				$("#divDigitalInContinuousPlay").show();
				$("#divDigitalInInterrupt").show();
				$("#divPlayDelay").show();
				$("#divRestartPlay").show();


				break;
			default:
				$("#divDigitalInIObits").hide();
				$("#divDigitalIOMode").hide();
				$("#divPlayDelay").hide();
				$("#divDigitalInTimeout").hide();
				$("#divRestartPlay").hide();
				$("#divLoopPlay").hide();
				$("#divRandomPlay").hide();
				$("#divPowerOnAutoPlay").hide();
				break;
		}

}

$.audioAUEQ5ONInitDiv = function(val) {

	if (val==0) {
		$("#divAUEQ5ONdata").show();
		$("#divAUEQ5ONdata2").hide();
	} else {
		$("#divAUEQ5ONdata").hide();
		$("#divAUEQ5ONdata2").show();
	}

}

$.audioAUADMIXONInitDiv = function(val) {

/* 	if (val==1) {
		$("#divAUADMIXONdata").show();
	} else {
		$("#divAUADMIXONdata").hide();
	} */
}

$.clockInit = function() {

	$.globalInit();

	//alert(DisplayDstSwitchDates());
	
	if (!$("#RTCCD option").length)
		for (i=1; i<=31; i++)
			$("#RTCCD").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCM option").length)
		for (i=1; i<=12; i++)
			$("#RTCCM").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCY option").length)
		for (i=2016; i<=2050; i++)
			$("#RTCCY").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCHH option").length)
		for (i=0; i<=23; i++)
			$("#RTCCHH").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCMM option").length)
		for (i=0; i<=59; i++)
			$("#RTCCMM").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCSS option").length)
		for (i=0; i<=59; i++)
			$("#RTCCSS").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	//$("select").selectmenu("refresh");

	$(".btApplyClock").click(function(e, notConfirm) {

		if (typeof notConfirm === "undefined")
			if (!confirm("Apply current data?"))
				return false;

		$.reqSetData().done(function() {
				clearTimeout(clock);
				sec = parseInt($("#RTCCHH").val())*3600 + parseInt($("#RTCCMM").val())*60 + parseInt($("#RTCCSS").val());
				$("#myclock").text($.secondsToTime(sec));
				clock = setTimeout(updateClock, 1000);

				$("#mydate").text($("#RTCCWD option:selected").text() + " " + $.strPad($("#RTCCD").val(),2) + "/" + $.strPad($("#RTCCM").val(),2) + "/" + $.strPad($("#RTCCY").val(),2));

		});
	})

	$(".btSyncClock").click(function() {
        if (!confirm("Syncronize clock?"))
			return false;

		var now = new Date();

		$("#RTCCHH").val(now.getHours());
		$("#RTCCMM").val(now.getMinutes());
		$("#RTCCSS").val(now.getSeconds());

		$("#RTCCD").val(now.getDate());
		$("#RTCCM").val(now.getMonth()+1);
		$("#RTCCY").val(now.getFullYear());

		var weekday = now.getDay()==0?7:now.getDay();
		$("#RTCCWD").val(weekday);

		$("select").selectmenu("refresh");

		$(".btApplyClock").trigger("click", [true]);
	})
	
	$("#NTPEN").change(function() {
		if ($(this).val()==1) {
			$(".ntpfield").removeClass("ui-disabled")
			$("#divNtpEnabled").slideDown();
		} else {
			$(".ntpfield").addClass("ui-disabled")
			$("#divNtpEnabled").slideUp();
		}		
	})
	
	$(".customsel").change(function() {
		$(this).parents('.divCustomsel').find('input').val($(this).val())
	})

	$(".custominput").on('keypress', function(e) {
		if (String.fromCharCode(e.keyCode).match(/[^0-9a-zA-Z_.-]/g))
			return false;
	});	
	
	$(".custominput").change(function() {
		var customselobj = $(this).parents('.divCustomsel').find('select')
		customselobj.val($(this).val())
		if (customselobj.val()==null)
			customselobj.val('custom')
		customselobj.selectmenu("refresh")
	})
	
	$("#NTPSYNCNOW").click(function() {
		$.reqSetData($("#NTPSYNCNOW"))
			.done(function() {
				$.reloadPage();
			});
	})		
}

$.audioInit = function() {

	$.globalInit();

	$.audioAUEQ5ONInitDiv($("#AUEQ5ON").val());
	$.audioAUADMIXONInitDiv($("#AUADMIXON").val());


	$("#AUEQ5ON").change(function() {
		$.audioAUEQ5ONInitDiv($(this).val());
	});

	$("#AUADMIXON").change(function() {
		$.audioAUADMIXONInitDiv($(this).val());
	});

	$.checkAUOT = function() {
		switch ($("#AUOT").val()) {
			case "0": //stereo
				$("#AUOI").selectmenu("disable");
				break;
			case "1": // mono
				$("#AUOI").selectmenu("enable");
				break;
		}
	}

	$.checkAUOT();

	$("#AUOT").change(function() {
		$.checkAUOT();
	});

}

$.deviceinfoInit = function() {
	$.globalInit();

	$.reqGetData()
		.always(function() {
			$.INITdivDeviceFunction($.inArray($("#DEVFN").text(), tblDEVFN));
			$.INITdivDigitalIOMode($.inArray($("#DIOM").text(), tblDIOM));
		});

}

$.devfuncInit = function() {

	$.globalInit();

	$.INITdivDeviceFunction($("#DEVFN").val());
	//$.INITdivDigitalIOMode($("#DIOM").val());
	$("#DEVFN").change(function() {
        if (!confirm("Apply new Device Function?\nNOTE: System will be reinitialized!"))
			return false;
		
		$.reqSetData($("#DEVFN"),"silent")
			.done(function() {
				constToastCloseTimeout = constToastCloseTimeoutLongDEFINE;
				$("#toast").on('popupafterclose', function() {
					location.reload();
				})				
				$.toast("Please wait... the system is doing its initialization");
				constToastCloseTimeout = constToastCloseTimeoutDEFINE;
				
			});					
	});
	$("#DIOM").change(function() {
		$.INITdivDigitalIOMode($(this).val());
	});
	$("#DIINT").change(function() {
		if (parseInt($(this).val())) {
			$("#divRestartPlay").removeClass("ui-disabled");
		} else {
			$("#divRestartPlay").addClass("ui-disabled");
			$("#DIRP").val("0").slider("refresh");
		}
	});
	
	
}

$.iosetInit = function(data) {

	if (!$.isPlainObject(data))
		data = $.parseJSON(data);

	$.globalInit();

	// init flags
	$(".isflag").each(function() {
		var flag = $(this).attr("id");
		var idvar = "";
		idvar = flag.substring(0, flag.indexOf("_"));
		flag = parseInt(flag.substring(flag.indexOf("_")+1, flag.length));
		$(this).val( + (($("#" + idvar).val() & (1 << (flag-1)))>0));
		$(this).trigger("change");
		var value = $(this).val()!="" ? $(this).val() : $(this).attr("value");
		if (parseInt(value)==0) {
			// OUTPUT
			$("#DOM"+flag).parent().show();

			if ($("#DOM"+flag).val()=="2") {
				// BLINK mode
				$("#DOPM_"+flag).next().hide();
				$("#DOPT"+flag).parent().hide();
			} else {
				$("#DOPM_"+flag).next().show();
				if (parseInt($("#DOPM_"+flag).val())) {
					$("#DOPT"+flag).parent().show();
				} else {
					$("#DOPT"+flag).parent().hide();
				}
			}
		} else {
			// INPUT
			$("#DOM"+flag).parent().hide();
			//$("#DOPM_"+flag).val(0).trigger("change");
			if (parseInt($("#DOPM_"+flag).val())) {
				$("#DOPT"+flag).parent().show();
			} else {
				$("#DOPT"+flag).parent().hide();
			}
			$("#DOPM_"+flag).next().hide();
		}
	});

	// init values
	$("[id^='DOSET']").each(function() {		
		// verifica se nascondere l'impostazione VALUE
		var flag = $(this).attr("id").slice(-1);
		var mask = (1 << (flag-1));
		if ($("#DISTS").val() & mask) {
			$(this).val(1);
		}
		if ($("#DIOC_"+flag).val()==1) {
			// input mode
			var mytext = $(this).val()==1 ? 'ON' : 'OFF';
			$(this).parent("td").html('<span id="DOSET' + flag + '" >' + mytext + '</span>');
		}

	});

	switch (parseInt(data['res']['get']['DIOM'])) {

		case 1: // Player
			for (var i=1; i<= 6; i++) {
				$('#tblIO tr:eq(' + i + ')').find("th:eq(0)").addClass("ui-disabled");
				$('#tblIO tr:eq(' + i + ')').find("td:eq(0)").addClass("ui-disabled");
				$('#tblIO tr:eq(' + i + ')').find("td:eq(2)").addClass("ui-disabled");
				$('#tblIO tr:eq(' + i + ')').find("td:eq(3)").addClass("ui-disabled");
				$('#tblIO tr:eq(' + i + ')').find("td:eq(4)").addClass("ui-disabled");
				$('#tblIO tr:eq(' + i + ')').find("td:eq(5)").addClass("ui-disabled");
				$('#tblIO tr:eq(' + i + ')').find("td:eq(6)").addClass("ui-disabled");
			}		
			break;

		case 2: // binarycode
			var DIBCvar = parseInt(data['res']['get']['DIBC']);
			for (var i=1; i<= DIBCvar; i++) {
				$('#tblIO tr:eq(' + i + ')').find("th:eq(0)").addClass("ui-disabled");
				$('#tblIO tr:eq(' + i + ')').find("td:eq(0)").addClass("ui-disabled");
				$('#tblIO tr:eq(' + i + ')').find("td:eq(2)").html('');
				$('#tblIO tr:eq(' + i + ')').find("td:eq(3)").html('');
				$('#tblIO tr:eq(' + i + ')').find("td:eq(4)").html('');
				$('#tblIO tr:eq(' + i + ')').find("td:eq(5)").html('<span id="DIOC_' + i + '" value="1" class="isflag ui-disabled">IN</span>');
			}

			break;

		case 4: // keyboard16
			$('#tblIO td:nth-child(2),#tblIO th:nth-child(2)').remove();
			$('#tblIO td:nth-child(3),#tblIO th:nth-child(3)').remove();
			$('#tblIO td:nth-child(3),#tblIO th:nth-child(3)').remove();
			$('#tblIO td:nth-child(3),#tblIO th:nth-child(3)').remove();

			break;
	}

	switch (parseInt(data['res']['get']['DEVFN'])) {

		case 4: // scheduler
			break;
		default:
			$('select.clDOM option[value="4"]').remove();
			$('select.clDOM').selectmenu("refresh");
			break;
	}

	$("[id^='DOPM_']").change(function() {
		var flag = $(this).attr("id");
		var flag = parseInt(flag.substring(flag.indexOf("_")+1, flag.length));
		if (parseInt($(this).val())) {
			$("#DOPT"+flag).parent().show();
		} else {
			$("#DOPT"+flag).parent().hide();
		}
	});

	$("[id^='DIOC_']").change(function() {
		var flag = $(this).attr("id");
		var flag = parseInt(flag.substring(flag.indexOf("_")+1, flag.length));
		if (parseInt($(this).val())==0) {
			$("#DOM"+flag).parent().show();
			$("#DOPM_"+flag).next().show();
		} else {
			$("#DOM"+flag).parent().hide();
			$("#DOPM_"+flag).val(0).trigger("change");
			$("#DOPM_"+flag).next().hide();
		}
	});

	$(".clDOM").change(function() {
		var flag = $(this).attr("id");
		flag = parseInt(flag.substring(flag.length - 1));
		if ($(this).val()=="2") {
			// BLINK mode
			$("#DOPM_"+flag).next().hide();
			$("#DOPT"+flag).parent().hide();
		} else {
			$("#DOPM_"+flag).next().show();
			if ($("#DOPM_"+flag).val()=="1") {
				$("#DOPT"+flag).parent().show();
			}
		}
	});

	$(".clDOPT").blur(function() {
		// validate value
		var value = parseFloat($(this).val());
		if (value > $(this).attr("max"))
			$(this).val($(this).attr("max"));
		if (value < $(this).attr("min"))
			$(this).val($(this).attr("min"));
	});

	$("#btApplyIO").click(function() {
		if (!confirm("Apply current data?"))
			return false;

		$("#DIOIM").val(0);
		$("#DOPM").val(0);
		$("#DIOC").val(0);

		$(".isflag").each(function() {
			var flag = $(this).attr("id");
			var idvar = "";
			idvar = flag.substring(0, flag.indexOf("_"));
			flag = parseInt(flag.substring(flag.indexOf("_")+1, flag.length));
			mask = (1 << (flag-1));
			target = $("#" + idvar);
			var value = $(this).val()!="" ? $(this).val() : $(this).attr("value");
			if (parseInt(value)) {
				target.val(parseInt(target.val()) + mask);
			}

		});

		$.reqSetData()
			.done(function() {

				// init values
				$("[id^='DOSET']").each(function() {
					// verifica se nascondere l'impostazione VALUE
					var flag = $(this).attr("id").slice(-1);
					if ($("#DIOC_"+flag).val()==1) {
						// input mode
						var mytext = $(this).val()==1 ? 'ON' : 'OFF';
						$(this).parent("td").html('<span id="DOSET' + flag + '" >' + mytext + '</span>');
					} else if (!$("#DOSET"+flag).is("select")) {
						$(this).parent("td").html('<select class="isfield immediate clDOSET" name="DOSET' + flag + '" id="DOSET' + flag + '"  data-role="slider" data-mini="true"><option value="0">OFF</option><option value="1">ON</option></select>');
						$("select#DOSET"+flag).slider().slider("refresh");
					}

				});

				$.globalInit();
			});

	});

}

$.sformat = function(s) {
      var fm = [
            Math.floor(s / 60), // MINUTES
            s % 60 // SECONDS
      ];
      return $.map(fm, function(v, i) { return ((v < 10) ? '0' : '') + v; }).join(':');
}


$.fillupTrackList = function(data, target) {
	var filetype = target.attr("FILE_TYPE");
	if (target.is("ol")) {
		$.each(data, function(key, val) {
			$.each(val, function(filename, options) {
				var trackClass = "";
				switch (filetype) {
					case 'sensor':
						trackClass = 'clSensorTrack';
						break;
					case 'spot':
						trackClass = 'clSpotTrack';
						break;
				}
				target.append(
					$("<li>").append(
						$('<a class="clTrackitem ' + trackClass + '" idx="'+key+'" trackname="'+filename+'" FILE_TYPE="'+filetype+'" href="#">').text(filename).append(
							$("<span>").attr("class", "ui-li-count").text($.sformat(options[0]))
						)
	 				)
				);
			});
		});
		target.listview().listview("refresh");


	} else if (target.is("select")) {
		$.each(data, function(key, val) {
			$.each(val, function(filename, options) {
				target.append($('<option/>', {value: filename, text: filename }).attr({idx: key, FILE_TYPE: filetype, opt: options}));
			});
			target.selectmenu("refresh");
		});
	}

}

$.playlistInit = function() {
	$.globalInit();


	$("#PL_ITEMS option").remove();
	$("#divMediaButtons").addClass("ui-disabled");
	
	$.initClickTrackItem = function() {
		$(".clTrackitem").click(function() {
			$(".clTrackitem").removeClass("ui-btn-active");
			$(this).addClass("ui-btn-active");
			$("#divMediaButtons").removeClass("ui-disabled");
		});
	}	
	
	if ($.isEmptyObject(plitemsobj)) {
		$.reqData("get", {PL_ITEMS: ''})
			.done(function() {
				if ($("#PL_ITEMS option").length==0) $("#divMediaButtons").addClass("ui-disabled");
				$.initClickTrackItem()
			});
	} else if ($("#PL_ITEMS li").length==0) {
		$.fillupTrackList(plitemsobj, $("#PL_ITEMS"));
		$.initClickTrackItem();
	}

}

$.plelementInit = function() {
	if ($("#PL_ITEMS option").length==0) {
		$("#btStartPl").parent("div").addClass("ui-disabled");
		$("#btPause").parent("div").addClass("ui-disabled");
		$("#btStop").parent("div").addClass("ui-disabled");
		return false;
	}

	$("#btStartPl").parent("div").removeClass("ui-disabled");
	$("#btPause").parent("div").removeClass("ui-disabled");
	$("#btStop").parent("div").removeClass("ui-disabled");

	$.reqData("get", {PL_ITEM_ELEM: $("#PL_ITEMS").val()})
		.done(function(data) {
			$("#plsortable").empty();
			if ($("#PL_ITEMS").val()==null)
				$("#pltitle").text('');
			else
				$("#pltitle").text($("#PL_ITEMS").val());

			$.each($.parseJSON(data).res.get.PL_ITEM_ELEM[$("#PL_ITEMS").val()].elements, function(key, val) {

				$("#plsortable").append(
					$("<li>").append(
						$('<a class="clTrackitem" idx="'+key+'" trackname="'+val.FILE_NAME+'" FILE_TYPE="' + val.FILE_TYPE + '" href="#">').text(val.FILE_NAME).append(
							$("<span>").attr("class", "ui-li-count").text($.sformat(val.id3tag[0]))
						)
					)
				);
			});
			$("#plsortable").listview("refresh");

			$(".clTrackitem").click(function() {
				$(".clTrackitem").removeClass("ui-btn-active");
				$(this).addClass("ui-btn-active");
				$("#divTrackCmds").removeClass("ui-disabled");
				$(".clTrackitem").removeClass("ui-disabled");
			});

		});
}

$.plcreatorInit = function() {
	$.globalInit();
	$("#PL_ITEMS option").remove();

	if ($.isEmptyObject(plitemsobj)) {
		$.reqData("get", {PL_ITEMS: ''})
			.done(function() {
				$.plelementInit();
			});
	} else if ($("#PL_ITEMS option").length==0) {
		$.fillupTrackList(plitemsobj, $("#PL_ITEMS"));
		$.plelementInit();
	}

	$.initTracksTab();

	$("#PL_ITEMS").change(function() {
		lastSelectedPl = $("#PL_ITEMS").val();
		$.plelementInit();
	})

	$("#btNext").click(function() {
		var target = $(".clTrackitem.ui-btn-active");
		if (target.length==0) return false;
		
		target.removeClass("ui-btn-active");
		
		target = target.parent("li").next().children("a");
		if (target.length==0) return false;
		target.addClass("ui-btn-active");
		
		$("#btPlay").trigger("click");
	})

	$("#btPrev").click(function() {
		var target = $(".clTrackitem.ui-btn-active");
		if (target.length==0) return false;
		
		target.removeClass("ui-btn-active");
		
		target = target.parent("li").prev().children("a");
		if (target.length==0) return false;
		target.addClass("ui-btn-active");
		
		$("#btPlay").trigger("click");
	})	
	
	$("#addtrack").click(function() {
		var dataitems = $("#tabs a.tabnav.ui-btn-active").attr("tabdataidx");
		var obj = $("#" + dataitems + " li a.ui-btn-active").parent("li");
		$("#" + dataitems + " li a.ui-btn-active").removeClass("ui-btn-active");
		$("#divTrackCmds").addClass("ui-disabled");
		if ($("#PL_ITEMS option").length==0) {
			$("#btPause").parent("div").addClass("ui-disabled");
			$("#btStop").parent("div").addClass("ui-disabled");
		}

		var obj2 = obj.clone();
		switch(dataitems) {
			case "MUSIC_ITEMS":
				obj2.attr("FILE_TYPE", "music");
				break;
			case "SPOT_ITEMS":
				obj2.attr("FILE_TYPE", "spot");
				break;
			case "SENSOR_ITEMS":
				obj2.attr("FILE_TYPE", "sensor");
				break;
		}

		$("#plsortable").append(obj2);

		$(".clTrackitem").click(function() {
			$(".clTrackitem").removeClass("ui-btn-active");
			$(this).addClass("ui-btn-active");
			$("#divTrackCmds").removeClass("ui-disabled");
			$(".clTrackitem").removeClass("ui-disabled");
		});

		$("#plsave").buttonMarkup({theme: 'c'});
	})

	$("#plsortable").sortable({
		update: function () {
			$("#plsave").buttonMarkup({theme: 'c'});
		}
	}).disableSelection();

	$.plButtonsEnable = function(bEnable) {
		if (bEnable) {
			$("#plsave").removeClass('ui-disabled');
			$("#pldelete").removeClass('ui-disabled');
			$("#plclone").removeClass('ui-disabled');
		} else {
			$("#plsave").addClass('ui-disabled');
			$("#pldelete").addClass('ui-disabled');
			$("#plclone").addClass('ui-disabled');
		}
	}

	$("#plsave").click(function() {
		if (!confirm("Are you sure to save current playlist?"))
			return false;

		var data = [];
		$.each($("#plsortable li a"), function(key, val) {
			data.push({FILE_NAME: $(val).attr('trackname'), FILE_TYPE: $(val).attr('FILE_TYPE'), id3tag: [$.timeToSeconds($(val).find("span.ui-li-count").text())]});
		})
		var newobj = {};
		newobj[$("#PL_ITEMS").val()] = data;
		console.log(newobj);
		$.reqSetData({PL_ITEM_ELEM: newobj}, true)
			.done(function() {
				$("#plsave").buttonMarkup({theme: 'a'});
			});
		
	})

	$("#plclear").click(function() {
		if (!confirm("Are you sure to clear current playlist?"))
			return false;

		$("#plsortable li").remove();
		$("#plsave").buttonMarkup({theme: 'c'});
	});

	$("#plremovetrack").click(function() {
		if (!confirm("Are you sure to remove selected track?"))
			return false;

		$("#plsortable .ui-btn-active").remove();

		$("#plsave").buttonMarkup({theme: 'c'});
	});

	$("#pldelete").click(function() {
		if (!confirm("Are you sure to delete current playlist?"))
			return false;
		var reqdata = {FILE_NAME: $("#PL_ITEMS").val(), FILE_TYPE: 'playlist', TYPE: 'deletefile'};
		$.reqData("cmd", reqdata)
			.done(function() {

				$("#PL_ITEMS").find("option:selected").remove();
				$("#PL_ITEMS").selectmenu("refresh");
				$("#PL_ITEMS").trigger("change");

				if ($("#PL_ITEMS option").length==0) {
					$.plButtonsEnable(false);
				}
				$.toast("Done");
				$("#plsave").buttonMarkup({theme: 'a'});
			});
	})

	$("#plnew, #plclone").click(function() {
		$("#plname").val("");
		switch ($(this).attr('cmd')) {
			case "clone":
				$("#createnewplPopup .popH").text("Create Clone Playlist");
				$("#createnewplPopup").find("#btSetNewPl").attr("cmd", "clone");
				break;
			case "new":
				$("#createnewplPopup .popH").text("Create New Playlist");
				$("#createnewplPopup").find("#btSetNewPl").attr("cmd", "new");
				break;

		}
		$("#createnewplPopup").popup("open");
	});

	$("#plname").on('keypress', function(e) {
		if (e.keyCode == 13) {
			$("#btSetNewPl").trigger("click");
		}
		if (String.fromCharCode(e.keyCode).match(/[^0-9a-zA-Z_]/g))
			return false;
	});

	$("#btSetNewPl").once("click", function() {
		if ($("#plname").val().length==0)
			return false;

		$("#plname").val($("#plname").val()+".m3u");

		switch ($(this).attr('cmd')) {
			case "new":
				$("#plsortable li").remove();
				break;
		}

		if ($("#plname").val().length > 0) {
			$("#PL_ITEMS").append("<option>" + $("#plname").val() + "</option>");
			$("#PL_ITEMS").val($("#plname").val()).selectmenu("refresh");
			$("#pltitle").text($("#PL_ITEMS").val());

			$.plButtonsEnable(true);

			$("#createnewplPopup").popup("close");

			var data = [];
			$.each($("#plsortable li a"), function(key, val) {
				data.push($(val).attr('trackname'));
			})
			var newobj = {};
			newobj[$("#PL_ITEMS").val()] = data;
			plitemsobj.push(newobj);
			$.reqSetData({PL_ITEM_ELEM: newobj}, true)
				.done(function() {
					$("#plsave").buttonMarkup({theme: 'a'});
				});
		}


	});

	if (lastSelectedPl != -1) {
		$("#PL_ITEMS").val(lastSelectedPl).selectmenu("refresh");
		$("#PL_ITEMS").trigger("change");
	}
	
}

$.initTracksTab = function() {
	
	if (!$.isEmptyObject(musicitemsobj) && ($("#MUSIC_ITEMS li").length==0)) {
		$.fillupTrackList(musicitemsobj, $("#MUSIC_ITEMS"));
	}
	
	$.initClickTrackItem = function() {
		$(".clTrackitem").click(function() {
			$(".clTrackitem").removeClass("ui-btn-active");
			$(this).addClass("ui-btn-active");
			$("#divTrackCmds").removeClass("ui-disabled");
			$(".clTrackitem").removeClass("ui-disabled");
		});
	}

	$("#tabs a.tabnav").click(function() {

		$(".clTrackitem").removeClass("ui-btn-active");
		$("#divTrackCmds").addClass("ui-disabled");
		if ($("#PL_ITEMS option").length==0) {
			$("#btPause").parent("div").addClass("ui-disabled");
			$("#btStop").parent("div").addClass("ui-disabled");
		}
		if ($(this).hasClass("musictab")) {
			if ($.isEmptyObject(musicitemsobj)) {
				$.reqData("get", {MUSIC_ITEMS: ''})
					.done(function() {
						$.initClickTrackItem();
					});
			} else if ($("#MUSIC_ITEMS li").length==0) {
				$.fillupTrackList(musicitemsobj, $("#MUSIC_ITEMS"));
			}
		} else if ($(this).hasClass("spottab")) {

			if ($.isEmptyObject(spotitemsobj)) {
				$.reqData("get", {SPOT_ITEMS: ''})
					.done(function() {
						$.initClickTrackItem();
					});
			} else if ($("#SPOT_ITEMS li").length==0) {
				$.fillupTrackList(spotitemsobj, $("#SPOT_ITEMS"));
			}
		} else if ($(this).hasClass("sensortab")) {

			if ($.isEmptyObject(sensoritemsobj)) {
				$.reqData("get", {SENSOR_ITEMS: ''})
					.done(function() {
						$.initClickTrackItem();
					});
			} else if ($("#SENSOR_ITEMS li").length==0) {
				$.fillupTrackList(sensoritemsobj, $("#SENSOR_ITEMS"));
			}
		}

	});

	$.initClickTrackItem();

	$(".tabnav.musictab").trigger("click");

}

$.playerInit = function() {

	$.globalInit();

	$.initTracksTab();

	$("#divmusic").trigger("click");

}

$.relsensInit = function() {

	$.globalInit();

	if ($("#DEVFN").val()==4) {
		// Scheduler MODE
		$("#divRelData").addClass("ui-disabled");
	}

	if ($("#RELM").val() == '1') {
		// Player relay mode
		$("#divRELADV").show();
	}

	$("#RELM").change(function() {
		if ($(this).val() == '1') {
			$("#divRELADV").show();
		} else {
			$("#divRELADV").hide();
		}
	});

	var initialVal = $("#RELSET").val();
	$("#RELSET").on("slidestop", function() {
		$("#RELSET").val(initialVal).slider('refresh');
		$("#RELSET").trigger("change");
	});

	$("#RELSET").on("change", function() {
		$.reqSetData(this, "silent");
	});

	$("#PL_ITEMS option").remove();
	$("#PL_ITEMS").append('<option>NONE</option>');
	if ($.isEmptyObject(plitemsobj)) {
		$.reqData("get", {PL_ITEMS: ''})
			.done(function() {				
				$.reqData("get", {SENSORPLN: ""}).done(function(data) {
						if (!$.isPlainObject(data)) data = $.parseJSON(data);
						$("#PL_ITEMS").val(data.res.get.SENSORPLN).selectmenu('refresh');
					});
			});
	} else if ($("#PL_ITEMS li").length==0) {
		$.fillupTrackList(plitemsobj, $("#PL_ITEMS"));
		$.reqData("get", {SENSORPLN: ''}).done(function(data) {
			if (!$.isPlainObject(data)) data = $.parseJSON(data);
			$("#PL_ITEMS").val(data.res.get.SENSORPLN).selectmenu('refresh');
		});
	}

	$("#PL_ITEMS").change(function() {
		$.reqData("set", {SENSORPLN: $(this).val()});
	})	
	
};

$.comrs485Init = function() {

	$.globalInit();

	/* $("#COMRS485").change(function() {
		if ($(this).val()==0) {
			$("#com_data").slideUp();
		} else {
			$("#com_data").slideDown();
		}
	}); */

	if ($("#COMRS485").val()==0) {
		$("#com_data").slideUp();
	} else {
		$("#com_data").slideDown();
	}
};


$.schedDataInit = function(scheddata) {
	paramData = {};
	paramData.schedules = [];
	paramData.events = [];

	$("#schtype").val(scheddata.options.schtype).selectmenu("refresh");
	$("input[id^=wday]").prop("checked", false).checkboxradio("refresh");
	$("#days").val("");

	switch (+scheddata.options.schtype) {
		case 0:
			// every day
			$("#checkweekdays").hide();
			$("#specificdays").hide();
			break;
		case 1:
			// weekly
			$.each(scheddata.options.days.split(","), function(idx, val) {
				if (val=="")
					return;
				$("#wday"+val).prop("checked", true).checkboxradio("refresh");
			});
			$("#checkweekdays").show();
			$("#specificdays").hide();
			break;
		default:
			// montly, specific days
			$("#checkweekdays").hide();
			$("#specificdays").show();
			break;
	}

	$.loadSchedData = function(myelements) {
		$.each(myelements, function(idx, elem) {
			switch (elem.eltype) {
				case 0:
					// playlist
					var calcend;
										
					for (var i=0; i<paramData.schedules.length; i++) {
						// search if is inside a playlist
						if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(elem.st))
							&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(elem.st))) {
							// aggiorna lunghezza playlist
							paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))+parseInt(elem.len));
							break;
						}
					}
					calcend = $.secondsToTime($.timeToSeconds(elem.st) + parseInt(elem.len));
					paramData.schedules.push({eltype: elem.eltype, start: elem.st, end: calcend, length: elem.len, title: elem.tit});
					
					
					break;
				case 1:
					// spot
					var calcend;
					for (var i=0; i<paramData.schedules.length; i++) {
						// search if is inside a playlist
						if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(elem.st))
							&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(elem.st))) {
							// aggiorna lunghezza playlist
							paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))+parseInt(elem.len));
							break;
						}
					}					
					calcend = $.secondsToTime($.timeToSeconds(elem.st) + parseInt(elem.len));
					paramData.schedules.push({eltype: elem.eltype, start: elem.st, end: calcend, length: elem.len, title: elem.tit, customclass: "spot"});

					break;
				case 2:
					// event
					paramData.events.push({eltype: elem.eltype, evstart: elem.st, title: elem.tit, type: elem.type, value: elem.val, idx: elem.idx });
			}
		});

		$('#mysched').graspSchedule();
	}

	if (typeof schedulerData[$("#schedlist").val()].elements === "undefined") {
		$.reqData("get", {SCHEDULER_ITEM_ELEM: $("#schedlist").val()})
			.done(function(data) {
				data = $.parseJSON(data).res.get.SCHEDULER_ITEM_ELEM[$("#schedlist").val()].elements;

				//schedulerData[$("#schedlist").val()].elements = [];
				schedulerData[$("#schedlist").val()].elements = data;

				$.loadSchedData(schedulerData[$("#schedlist").val()].elements);
			});
	} else {
		$.loadSchedData(schedulerData[$("#schedlist").val()].elements);
	}

}

$.clearSched = function() {
	$('#mysched').empty();
	$('#mysched').css("height", 0);
	paramData = {};
	paramData.schedules = [];
	paramData.events = [];
	$('#mysched').graspSchedule();

}

$.schedulerInit = function() {
	$.globalInit();
	
	$.schedButtonsEnable = function(bEnable) {
		if (bEnable) {
			$("#btDeleteScheduler").removeClass('ui-disabled');
			$(".schedCommands").removeClass('ui-disabled');
			$("#btSaveScheduler").removeClass('ui-disabled');
			$("#btCloneScheduler").removeClass('ui-disabled');
		} else {
			$("#btDeleteScheduler").addClass('ui-disabled');
			$(".schedCommands").addClass('ui-disabled');
			$("#btSaveScheduler").addClass('ui-disabled');
			$("#btCloneScheduler").addClass('ui-disabled');
		}
	}

	$.schedButtonsEnable(false);

	if ($.isEmptyObject(spotitemsobj)) {
		$.reqData("get", {SPOT_ITEMS: ''});
	}

	$("#PL_ITEMS option").remove();
	if ($.isEmptyObject(plitemsobj)) {
		$.reqData("get", {PL_ITEMS: ''});
	} else {
		$.fillupTrackList(plitemsobj, $("#PL_ITEMS"));
	}

	$.reqData("get", {DIOC: ''})
		.done(function() {
			$("#eventPopup").find("#idx").empty().selectmenu("refresh");
			$.each($.toFlags($("#DIOC").val(), 8), function(key, val) {
				if (val==0)
					$("#eventPopup").find("#idx").append("<option value=\"" + (parseInt(key)+1) + "\">Output " + (parseInt(key)+1) + "</option>");
			});
			$("#eventPopup").find("#idx").selectmenu("refresh");
		});

	$("#schtype").change(function() {
		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		switch (+$(this).val()) {
			case 0:
				// every day
				$("#checkweekdays").hide();
				$("#specificdays").hide();
				break;
			case 1:
				// weekly
				$("#checkweekdays").show();
				$("#specificdays").hide();
				break;
			default:
				// montly, specific days
				$("#checkweekdays").hide();
				$("#specificdays").show();
				break;
		}

	})


	$.initDrawSched = function() {

		$.each(schedulerData, function(index, scheddata) {

			$("#schedlist").append("<option>" + index + "</option>");

		});
		$("#schedlist").selectmenu("refresh");

		if ($("#schedlist option").length>0) {
			$.schedButtonsEnable(true);
			$.schedDataInit(schedulerData[$("#schedlist").val()]);
		}

	}

	if ($.isEmptyObject(schedulerData)) {
		$.reqData("get", {SCHEDULER_ITEMS: ''})
			.done(function(data) {
				data = $.parseJSON(data).res.get.SCHEDULER_ITEMS;

				schedulerData = data;

				$.initDrawSched();
			});
	} else {
		$.initDrawSched();
	}

	$("#btNewScheduler, #btCloneScheduler").click(function() {
		$("#schedname").val("").textinput("refresh");
		switch ($(this).attr('cmd')) {
			case "clone":
				$("#createnewschedPopup .popH").text("Create Clone Scheduler");
				$("#createnewschedPopup").find("#btSetNewSched").attr("cmd", "clone");
				break;
			case "new":
				$("#createnewschedPopup .popH").text("Create New Scheduler");
				$("#createnewschedPopup").find("#btSetNewSched").attr("cmd", "new");
				break;

		}
		$("#createnewschedPopup").popup("open");
	});

	$("#schedname").on('keypress', function(e) {
		if (e.keyCode == 13) {
			$("#btSetNewSched").trigger("click");
		}
		if (String.fromCharCode(e.keyCode).match(/[^0-9a-zA-Z_]/g))
			return false;
	});

	$("#days").on('keypress', function(e) {
		if (String.fromCharCode(e.keyCode).match(/[^0-9,-]/g))
			return false;
	})
	
	$("#btSetNewSched").once("click", function() {
		switch ($(this).attr('cmd')) {
			case "clone":
				schedulerData[$("#schedname").val()] = schedulerData[$("#schedlist").val()];
				break;
			case "new":
				schedulerData[$("#schedname").val()] = {options: {schtype: 0, days: ""}, elements: []};
				$.clearSched();
				$("#schtype").val(0).selectmenu("refresh");
				$("#schtype").trigger("change");

				break;

		}

		if ($("#schedname").val().length > 0) {
			$("#schedlist").append("<option>" + $("#schedname").val() + "</option>");
			$("#schedlist").val($("#schedname").val()).selectmenu("refresh");

			$.schedButtonsEnable(true);

			$("#btSaveScheduler").buttonMarkup({theme: 'c'});

			$.saveScheduler();

			$("#createnewschedPopup").popup("close");
		}


	});

	$.saveScheduler = function() {
		var data = {options: {}, elements: []};

		switch (parseInt($("#schtype").val())) {
			case 1: // weekly				
				// convert weekdays to "days"
				$("#days").val(",");
				$("[id^=wday]:checked").each(function() {
					$("#days").val($("#days").val() + $(this).attr("id").slice(-1) + ",");
				});				
				break;
			case 2: // montly
			
				break;
		}

		data.options = {schtype: +$("#schtype").val(), days: $("#days").val()}
		$.each(paramData.schedules, function(idx, elem) {
			data.elements.push({eltype: elem.eltype, st: elem.start, len: parseInt(elem.length), tit: elem.title});
		});
		$.each(paramData.events, function(idx, elem) {
			if (typeof elem.idx == "undefined")
				elem.idx = 0;
			data.elements.push({eltype: elem.eltype, st: elem.evstart, type: elem.type, val: elem.value, tit: elem.title, idx: elem.idx});
		});

		data.elements.sort(
				function (a, b) {
					var a = $.timeToSeconds(a.st);
					var b = $.timeToSeconds(b.st);
					if (a > b)
						return 1;
					if (a < b)
						return -1;
					return 0;
				}
		);
		var newobj = {};
		newobj[$("#schedlist").val()] = data;
		var dataSc = {SCHEDULER_ITEMS: newobj};

		$.reqSetData(dataSc, true)
			.done(function() {
				schedulerData[$("#schedlist").val()] = data;
				$("#btSaveScheduler").buttonMarkup({theme: 'a'});
			});
	}

	$("#btSaveScheduler").once("click", function() {
		if (!confirm("Save current Scheduler?"))
			return false;
		$.saveScheduler();
	})

	$("#btDeleteScheduler").once("click", function() {
		if (!confirm("Delete current Scheduler '" + $("#schedlist").find("option:selected").val() + "'?"))
			return false;

		var reqdata = {FILE_NAME: $("#schedlist").find("option:selected").val(), FILE_TYPE: 'scheduler', TYPE: 'deletefile'};
		$.reqData("cmd", reqdata)
			.done(function() {
				var key = $("#schedlist").val();
				delete schedulerData[key];

				$("#schedlist").find("option:selected").remove();
				$("#schedlist").selectmenu("refresh");
				$("#schedlist").trigger("change");

				if ($("#schedlist option").length==0) {

					$.schedButtonsEnable(false);

				}
				$.toast("Done");
			});

	})

	$("#schedlist").once("focus", function() {
		if ($("#btSaveScheduler").hasClass("ui-btn-c")) {
			$("#btSaveScheduler").trigger("click");
		}
	});

	$("#schedlist").once("change", function() {

		if ($("#schedlist option").length==0) {
			$.clearSched();
			return true;
		}
		
		lastSelectedSched  = $("#schedlist").val();
		
		$.schedDataInit(schedulerData[$("#schedlist").val()]);
		$("#schedlist").selectmenu("refresh");

		$('#mysched').graspSchedule();
		$("#btSaveScheduler").buttonMarkup({theme: 'a'});
	});


	$('#mysched').height($('#maincontent').outerHeight());


	$("#eventPopup").on('keypress', function(e) {
		if (e.keyCode == 13) {
			$("#eventPopup").find("#setevent").trigger("click");
		}
	});

	$("#btSetEvent").once('click', function(e) {
		var obj = {};

		obj.title = $("#eventPopup").find("#title").val();
		//0 = "relay", 1 = "output line", 2 = "sensor enable", 3 = "line in enable"
		switch ($("#eventPopup").find('[name="outtype"]:checked').val()) {
			case "relay":
				obj.type = 0;
				break;
			case "outline":
				obj.type = 1;
				obj.idx = parseInt($("#eventPopup").find("#idx").val());
				break;
			case "linein":
				obj.type = 3;
				break;
		}

		obj.value = parseInt($("#eventPopup").find('#outvalue').val());
		obj.evstart = $.strPad($("#eventPopup").find("#hours").val(), 2)+":"+$.strPad($("#eventPopup").find("#minutes").val(), 2)+":"+$.strPad($("#eventPopup").find("#seconds").val(),2);
		obj.eltype = 2;
		if (typeof $(this).attr("elemid") === "undefined") {
			// ---------------------------
			//  Add event
			// ---------------------------
			paramData.events.push(obj);
		} else {
			// ---------------------------
			//  Edit event
			// ---------------------------
			var id = parseInt($(this).attr("elemid"));
			paramData.events[id] = obj;
		}

		$('#mysched').graspSchedule();
		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");
	});

	$("#eventPopup [name=outtype]").once('change', function() {
		if ($(this).val() == "outline")
			$("#divOutLineIdx").removeClass('ui-disabled');
		else
			$("#divOutLineIdx").addClass('ui-disabled');

		if ($(this).val() == "linein")
			$("#lblOutvalue").text("LineIN Enable");
		else
			$("#lblOutvalue").text("Output value");
			
	});

	$("#btAddEvent").once('click', function() {

		// set event popup to default
		$("#btRemoveEvent").hide();
		$("#eventPopup .popH").text("Add Event");
		$("#eventPopup").find("#title").val("").attr("placeholder", "add event title here..").textinput("refresh");
		$("#eventPopup [name=outtype]").prop("checked",false).checkboxradio("refresh");
		$("#eventPopup").find("#relay").prop("checked",true).checkboxradio("refresh");
		$("#eventPopup").find("#outvalue").val(0).slider("refresh");
		$("#divOutLineIdx").addClass('ui-disabled');

		$("#btSetEvent").removeAttr("elemid"); // set for add

		if ($("#eventPopup").find('[name="outtype"]:checked').length==0) {
			// set "relay" by default
			$("#eventPopup").find("#relay").prop("checked",true);
		}

		$("#eventPopup").popup("open");
	});

	$("#btRemoveEvent").once('click', function() {
		if (!confirm("Are you sure to remove Event?"))
			return false;

		var id = parseInt($("#btSetEvent").attr("elemid"));
		paramData.events.splice(id, 1);

		$('#mysched').graspSchedule();
		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");
	});

	$("#btAddPlaylist").once('click', function() {
		// set playlist popup to default
		$("#btRemovePl").hide();
		$("#playlistPopup .popH").text("Add Playlist");

		$("#btSetPl").removeAttr("elemid"); // set for add

		$("#playlistPopup").find("#hours").val($.timeTo("h", paramData.lastTime));
		$("#playlistPopup").find("#minutes").val($.timeTo("m", paramData.lastTime));
		$("#playlistPopup").find("#seconds").val($.timeTo("s", paramData.lastTime)+1);

		$("#playlistPopup").popup("open");
	});


	$("#btSetPl").once('click', function(e) {
		var obj = {};
		obj.title = $("#PL_ITEMS").val();
		obj.idx = $("#PL_ITEMS option:selected").attr("idx");

		obj.start = $.strPad($("#playlistPopup #hours").val(), 2)+":"+$.strPad($("#playlistPopup #minutes").val(), 2)+":"+$.strPad($("#playlistPopup #seconds").val(),2);
		obj.length = parseInt($("#PL_ITEMS option:selected").attr("opt").split(",")[0]);

		obj.end = $.secondsToTime(parseInt($.timeToSeconds(obj.start))+parseInt(obj.length));

		obj.eltype = 0;
		for (var i=0; i<paramData.schedules.length; i++) {
			// search if is inside a playlist
			if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(obj.start))
				&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(obj.start))) {

				// aggiorna lunghezza playlist
				paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))+parseInt(obj.length));
				break;
			}

		}

		if (typeof $(this).attr("elemid") === "undefined") {
			// ---------------------------
			//  Add schedule
			// ---------------------------
			paramData.schedules.push(obj);
		} else {
			// ---------------------------
			//  Edit schedule
			// ---------------------------
			var id = parseInt($(this).attr("elemid"));
			for (var i=0; i<paramData.schedules.length; i++) {
				// search if it was inside a playlist
				if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(paramData.schedules[id].start))
					&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(paramData.schedules[id].start))) {
					// aggiorna lunghezza playlist
					paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))-parseInt(paramData.schedules[id].length));
					break;
				}

			}
			paramData.schedules[id] = obj;
		}

		$('#mysched').graspSchedule();
		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");

	});


	$("#btRemovePl").once('click', function() {
		if (!confirm("Are you sure to remove Playlist?"))
			return false;

		var id = parseInt($("#btSetPl").attr("elemid"));
		var obj = paramData.schedules[id];

		for (var i=0; i<paramData.schedules.length; i++) {
			if (i==id)
				continue;
			// search if is inside a playlist			
			if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(obj.start))
				&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(obj.start))) {
				// aggiorna lunghezza playlist
				paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))-parseInt(obj.length));
				break;
			}

		}

		paramData.schedules.splice(id, 1);

		$('#mysched').graspSchedule();
		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");
	});



	$("#btAddSpot").once('click', function() {

		// set spot popup to default
		$("#btRemoveSpot").hide();
		$("#spotPopup .popH").text("Add Spot");

		$("#btSetSpot").removeAttr("elemid"); // set for add

		$("#spotPopup").popup("open");
	});

	$.recursiveForwardShift = function(end_seconds, i, length) {
		i++;
		for (; i<paramData.schedules.length; i++) {
			if (+$.timeToSeconds(paramData.schedules[i].start)<+end_seconds) {
				paramData.schedules[i].start = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].start))+parseInt(length));
				paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))+parseInt(length));
				$.recursiveForwardShift($.timeToSeconds(paramData.schedules[i].end), i, length);
			}
		}
	}

	$.recursiveBackwardShift = function(end_seconds, i, length) {
		i++;
		for (; i<paramData.schedules.length; i++) {
			if (+$.timeToSeconds(paramData.schedules[i].start)>(parseInt(end_seconds)+parseInt(length))) {
				paramData.schedules[i].start = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].start))-parseInt(length));
				paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))-parseInt(length));
				$.recursiveBackwardShift($.timeToSeconds(paramData.schedules[i].end), i, length);
			}
		}
	}

	$("#btSetSpot").once('click', function(e) {
		var obj = {};
		obj.customclass = "spot";
		obj.title = $("#SPOT_ITEMS").val();
		obj.idx = $("#SPOT_ITEMS option:selected").attr("idx");

		obj.start = $.strPad($("#spotPopup #hours").val(), 2)+":"+$.strPad($("#spotPopup #minutes").val(), 2)+":"+$.strPad($("#spotPopup #seconds").val(),2);
		obj.length = $("#SPOT_ITEMS option:selected").attr("opt").split(",")[0];

		obj.end = $.secondsToTime(parseInt($.timeToSeconds(obj.start))+parseInt(obj.length));

		obj.eltype = 1;
		var parentid = -1;
		var elemid = parseInt($("#btSetSpot").attr("elemid"));


		if (typeof $(this).attr("elemid") === "undefined") {
			// ---------------------------
			//  Add schedule
			// ---------------------------
			paramData.schedules.push(obj);
		} else {
			// ---------------------------
			//  Edit schedule
			// ---------------------------	

			for (var i=0; i<elemid; i++) {
				// search if it is inside a playlist
				if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(paramData.schedules[elemid].start))
					&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(paramData.schedules[elemid].start))) {
					parentid = i;
					break;
				}

			}		
			for (var i=0; i<paramData.schedules.length; i++) {
				// search if it is inside a playlist
				if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(obj.start))
					&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(obj.start))) {
					
					if (parentid != i) {
						
						if (parentid != -1) {
							// was inside before
							paramData.schedules[parentid].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[parentid].end))-parseInt(obj.length));
						}
						
						// aggiorna lunghezza playlist
						paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))+parseInt(obj.length));

						// verifica se spostare altre playlist
						$.recursiveForwardShift($.timeToSeconds(paramData.schedules[i].end), i, obj.length);
					} 
					break;
				}

			}			
			
			
/*			
			for (var i=0; i<paramData.schedules.length; i++) {
				// search if it was inside a playlist
				if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(paramData.schedules[elemid].start))
					&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(paramData.schedules[elemid].start))) {

					paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))-parseInt(paramData.schedules[elemid].length));
					break;
				}

			}
*/

			paramData.schedules[elemid] = obj;
		}

		$('#mysched').graspSchedule();
		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");
	});

	$("#btRemoveSpot").once('click', function() {
		if (!confirm("Are you sure to remove Spot?"))
			return false;

		var id = parseInt($("#btSetSpot").attr("elemid"));
		var obj = paramData.schedules[id];
		var isInside=false;
		for (var i=0; i<paramData.schedules.length; i++) {
			// search if is inside a playlist
			if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(obj.start))
				&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(obj.start))) {

				paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))-parseInt(obj.length));
				isInside=true;
				break;
			}

		}

		paramData.schedules.splice(id, 1);

		$('#mysched').graspSchedule();
		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");
	});

	if (lastSelectedSched != -1) {
		$("#schedlist").val(lastSelectedSched).selectmenu("refresh");
		$("#schedlist").trigger("change");
	}
	
}

