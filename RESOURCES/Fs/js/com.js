// --------------------------
// CONSTANTS
// --------------------------

var constBoard = true;

// --------------------------
// hash tables
// --------------------------

var tblDEVFN = ["Standard Player", "Playlist Sequence", "Advanced Player", "Advanced Playlist", "Scheduler"];
var tblDIOM = ["Free", "Player", "Binary Code", "Playlist", "Keypad", "Suspension"];
var tblRELM = ["REMOTE", "PLAYER", "PLAYLIST", "TIMER", "ALARM"];



$.parseResp = function(target, value) {
	var targetobj = $(target);
	if (targetobj.hasClass("tracklist")) {
		
		if (!constBoard) {
			value = $.parseJSON(value);
		}	
		
		window[targetobj.attr("trackdata")] = value;
		$.fillupTrackList(value, targetobj);
			
	} else if (targetobj.is("select, input")) {
		if (targetobj.is('[factor]')) {
			value = (value*targetobj.attr('factor'));
		}
		if (targetobj.is('[step]')) {
			targetobj.val(value*targetobj.attr('step'));
		} 
		else {		
			targetobj.val(value);
		}
		
		if (targetobj.is('[data-role]'))
			targetobj.slider("refresh");								
		targetobj.trigger("change");
		
	} else {
		
		if (targetobj.hasClass("icoCheck")) {
			if (parseInt(value)==1) {
				targetobj.attr("title", "ON");
				targetobj.removeClass("icoCheckOFF");
				targetobj.addClass("icoCheckON");
			} else {
				targetobj.attr("title", "OFF");
				targetobj.removeClass("icoCheckON");
				targetobj.addClass("icoCheckOFF");
			}
		} 
		else if (targetobj.hasClass("formatbinary")) {
			value = $.strPad(parseInt(value , 10).toString(2), 8);
			value = value.split("");
			value.reverse()
			$.each(value, function (i, el) {
				var idx = i + 1;
				if (parseInt(el))
					targetobj.append("<div class=\"IOcont\"><span class=\"IObit\">" + idx + "</span><span class=\"IO IOin\" title=\"[" + idx + "] IN\">I</span></div>");
				else 
					targetobj.append("<div class=\"IOcont\"><span class=\"IObit\">" + idx + "</span><span class=\"IO IOout\" title=\"[" + idx + "] OUT\">O</span></div>");
			});
			/*
			$.each(value.split(""), function (i, el) {
				targetobj.append("<span >" + i + "</span>");
			});			
			*/
			//targetobj.text(value);									
		}
		else if (targetobj.is("[hashtable]")) {
			targetobj.text(eval(targetobj.attr("hashtable"))[value]);
		}
		else {
			if (targetobj.is('[factor]')) {
				value = (value*targetobj.attr('factor'));
			}			
			if (targetobj.is('[step]')) {
				targetobj.text(value*targetobj.attr('step'));
			} else {
				targetobj.text(value);
			}			
		}
	}	
	
}

$.getReqFields = function(targetobj) {
	var reqdata = {};
	if (typeof targetobj != 'undefined') {
		if (typeof $(targetobj).attr('id') != 'undefined') {
			reqdata[$(targetobj).attr('id')] = '';		
		}
	} else {
		$(".ui-field-contain > .isfield, .isfield, input.isfield, select.isfield").each(function() {
			if (typeof $(this).attr('id') != 'undefined')
				reqdata[$(this).attr('id')] = '';
		});
	}
	return reqdata;
}

$.setReqFields = function(targetobj) {
	var reqdata = {};
	if (typeof targetobj != 'undefined') {
		if ($.isPlainObject(targetobj))
			return targetobj;
		
		if (typeof $(targetobj).attr('id') != 'undefined') {
			
			if ($(targetobj).is('[step]')) {
				reqdata[$(targetobj).attr('id')] = $(targetobj).val()/$(targetobj).attr('step');
			} else {
				reqdata[$(targetobj).attr('id')] = $(targetobj).val();
			}				
		}
	} else {
		$("input.isfield, select.isfield").each(function() {
			if (typeof $(this).attr('id') != 'undefined') {
				
				if ($(this).is('[step]')) {
					reqdata[$(this).attr('id')] = $(this).val()/$(this).attr('step');
				} else {
					reqdata[$(this).attr('id')] = $(this).val();
				}
			}
		});	
	}
	
	return reqdata;
}

$.reqData = function(cmd, reqdata, targetobj, options) {
	if (typeof options === "undefined")
		options = "";
	
	var reqjson = '{"req":{"' + cmd + '":{';
	
	if ($.isEmptyObject(reqdata))
		return false;
	
	// --------------------------
	// casi particolari
	if (typeof reqdata["DIOM"] != 'undefined') {
		reqjson += '"DIOM":"' + reqdata["DIOM"] + '",';
		delete reqdata["DIOM"];
	}
	if (typeof reqdata["DIOC"] != 'undefined') {
		reqjson += '"DIOC":"' + reqdata["DIOC"] + '",';
		delete reqdata["DIOC"];		
	}		

	// --------------------------
	
	$.each(reqdata, function( index, value ) {
		if (!$.isPlainObject(value))
			reqjson += '"' + index + '":"' + value + '",';
		else {
			reqjson = reqjson.slice(0, -1);
			reqjson += $.stringify(reqdata);
			reqjson = reqjson.slice(0, -1);
			reqjson += ",";
		}
	});
	
	reqjson = reqjson.slice(0, -1);
	reqjson += "}}}";
	
	$("body").addClass('ui-disabled');
	$.mobile.loading('show');	

	if (constBoard) {
		var myurl = "jsondata";
		var mydata = "json=" + reqjson;
		var mycontentType = "application/json"; // send as JSON
	} else {
		var myurl = "jsondata.php";
		var mydata = {json:reqjson};
		var mycontentType = 'application/x-www-form-urlencoded; charset=UTF-8';
	}	
	
	var next = "";
	var tracklist = "";
	var request = $.ajax({
	  url: myurl,
	  method: "POST",
	  contentType: mycontentType,
	  data: mydata
	});

	request.done(function(data) {
		data = $.parseJSON(data);
		$.each(data, function( index, value ) {
			if (index=="res") {
				$.each(value, function( index, value ) {
					if (index=="get") {
						$.each(value, function( index, value ) {
							switch (index) {
								case "MUSIC_ITEMS":
								case "SPOT_ITEMS":
								case "SENSOR_ITEMS":
								case "PL_ITEMS":
									tracklist = index;									
									break;	
								case "NEXT":
									next = value;
									break;
							}
							$.parseResp('#' + index, value);														
						});
					}
				});				
			}
		});
		
		if ((cmd=="set") && (options != "silent"))
			$.toast("Done");
	});

	request.error(function(data) {
		$.toast("Error on 'reqData()'");
	});

	request.always(function() {
		if (next!="") {
			var data = {};
			data[tracklist] = '';
			data['NEXT'] = next;
			$.reqData("get", data);
		} else {
			$.mobile.loading('hide');
			$("body").removeClass('ui-disabled');
		}
	});
	return request;
}

$.reqGetData = function(targetobj) {	
	return $.reqData("get", $.getReqFields(targetobj), targetobj);
}

$.reqSetData = function(targetobj, options) {
	return $.reqData("set", $.setReqFields(targetobj), targetobj, options);
}
