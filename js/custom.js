var last_myhref;
var constToastCloseTimeoutDEFINE = 1000; //msec
var constToastCloseTimeoutLongDEFINE = 1500; //msec
var constToastCloseTimeout = constToastCloseTimeoutDEFINE;
var constIDLE_TIMEOUT = 9*60*1000 + 55*1000; // 9min 55sec  maneged in msec

var mp3Genres = ["Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", "New Age", "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial", "Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", "Euro-Techno", "Ambient", "Trip Hop", "Vocal", "Jazz+Funk", "Fusion", "Trance", "Classical", "Instrumental", "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise", "Alternative Rock", "Bass", "Soul", "Punk", "Space", "Meditative", "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic", "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta Rap", "Top 40", "Christian Rap", "Pop/Punk", "Jungle", "Native American", "Cabaret", "New Wave", "Phychedelic", "Rave", "Showtunes", "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", "Folk", "Folk/Rock", "National Folk", "Swing", "Fast-Fusion", "Bebob", "Latin", "Revival", "Celtic", "Blue Grass", "Avantegarde", "Gothic Rock", "Progressive Rock", "Psychedelic Rock", "Symphonic Rock", "Slow Rock", "Big Band", "Chorus", "Easy Listening", "Acoustic", "Humour", "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass", "Primus", "Porn Groove", "Satire", "Slow Jam", "Club", "Tango", "Samba", "Folklore", "Ballad", "power Ballad", "Rhythmic Soul", "Freestyle", "Duet", "Punk Rock", "Drum Solo", "A Capella", "Euro-House", "Dance Hall", "Goa", "Drum & Bass", "Club-House", "Hardcore", "Terror", "indie", "Brit Pop", "Negerpunk", "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal", "Black Metal", "Crossover", "Comteporary Christian", "Christian Rock", "Merengue", "Salsa", "Trash Metal", "Anime", "JPop", "Synth Pop" ];
//var digitalIOMode = ["Free", "Player", "Binary Code", "Playlist Direct", "Keypad", "Suspension"];
var digitalIOMode = ["Free", "Player", "Binary Code", "Playlist Direct", "Keypad", "Museum mode", "Priority message"];
var digitalOutputMode = ["Free", "Play", "Play+Sensor", "Play+BlinkInPause", "BlinkInPlay", "BlinkInPlayPlusSensor", "BlinkInStop", "BlinkInStopPlusSensor"];
//var digitalOutputMode = ["Free", "Play", "Blink", "Fault" ,"Timer"];

var wdays = ["", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

// --------------------------

var musicitemsobj = {};
var spotitemsobj = {};
var sensoritemsobj = {};
var plitemsobj = {};

var lastSelectedPl = -1;

var schedulerData = {};
var lastSelectedSched = -1;


var usb_None = 0;
var	usb_Ready = 1;
var	usb_Busy_UnderSync = 2;
var	usb_Busy_CopyFiles = 3;
var	usb_INVALID = 4;

var clock;
var idleTimer;

var currentLOGINTYPE;
// --------------------------

function DisplayDstSwitchDates()
   {
        var year = new Date().getYear();
        if (year < 1000)
            year += 1900;
        var firstSwitch = 0;
        var secondSwitch = 0;
        var lastOffset = 99;
        // Loop through every month of the current year
        for (i = 0; i < 12; i++)
        {
            // Fetch the timezone value for the month
            var newDate = new Date(Date.UTC(year, i, 0, 0, 0, 0, 0));
            var tz = -1 * newDate.getTimezoneOffset() / 60;
            // Capture when a timzezone change occurs
            if (tz > lastOffset)
                firstSwitch = i-1;
            else if (tz < lastOffset)
                secondSwitch = i-1;
            lastOffset = tz;
        }
        // Go figure out date/time occurences a minute before
        // a DST adjustment occurs
        var secondDstDate = FindDstSwitchDate(year, secondSwitch);
        var firstDstDate = FindDstSwitchDate(year, firstSwitch);
        if (firstDstDate == null && secondDstDate == null)
            return;
        else {
            result = [];
			result.push(firstDstDate.split(' '));
			result.push(secondDstDate.split(' '));
			return result;
		}
    }//
    function FindDstSwitchDate(year, month)
    {
        // Set the starting date
        var baseDate = new Date(Date.UTC(year, month, 0, 0, 0, 0, 0));
        var changeDay = 0;
        var changeMinute = -1;
        var baseOffset = -1 * baseDate.getTimezoneOffset() / 60;
        var dstDate;
        // Loop to find the exact day a timezone adjust occurs
        for (day = 0; day < 50; day++)
        {
            var tmpDate = new Date(Date.UTC(year, month, day, 0, 0, 0, 0));
            var tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;
            // Check if the timezone changed from one day to the next
            if (tmpOffset != baseOffset)
            {
                var minutes = 0;
                changeDay = day;
                // Back-up one day and grap the offset
                tmpDate = new Date(Date.UTC(year, month, day-1, 0, 0, 0, 0));
                tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;
                // Count the minutes until a timezone chnage occurs
                while (changeMinute == -1)
                {
                    tmpDate = new Date(Date.UTC(year, month, day-1, 0, minutes, 0, 0));
                    tmpOffset = -1 * tmpDate.getTimezoneOffset() / 60;
                    // Determine the exact minute a timezone change
                    // occurs
                    if (tmpOffset != baseOffset)
                    {
                        // Back-up a minute to get the date/time just
                        // before a timezone change occurs
                        tmpOffset = new Date(Date.UTC(year, month,
                                             day-1, 0, minutes-1, 0, 0));
                        changeMinute = minutes;
                        break;
                    }
                    else
                        minutes++;
                }

                dstDate = tmpOffset.getMonth() + 1;
				if (dstDate < 10) dstDate = "0" + dstDate;
				//dstDate = year + '/' + dstDate + '/';
                dstDate += '/' + tmpOffset.getDate();
                // Capture the time stamp
                tmpDate = new Date(Date.UTC(year, month,
                                   day-1, 0, minutes-1, 0, 0));
                dstDate += " " + tmpDate.toTimeString().split(' ')[0];
                return dstDate;


            }
        }
    }

// --------------------------


jQuery.extend({
    stringify  : function stringify(obj) {
        var t = typeof (obj);
        if (t != "object" || obj === null) {
            // simple data type
            if (t == "string") obj = '"' + obj + '"';
            return String(obj);
        } else {
            // recurse array or object
            var n, v, json = [], arr = (obj && obj.constructor == Array);

            for (n in obj) {
                v = obj[n];
                t = typeof(v);
				if ($.isNumeric(v)) t="numeric";
                if (obj.hasOwnProperty(n)) {
                    if (t == "string") v = '"' + v + '"'; else if (t == "object" && v !== null) v = jQuery.stringify(v);
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
            }
            return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
        }
    }
});

$.fn.ignore = function(sel){
  return this.clone().find(sel||">*").remove().end();
};

$.strPad = function(i,l,s) {
	var o = i.toString();
	if (!s) { s = '0'; }
	while (o.length < l) {
		o = s + o;
	}
	return o;
};

$.toFlags = function(val, bits) {
	return $.strPad(parseInt(val).toString(2),bits).split("").reverse();
}

$.timeTo = function(type, timestr) {
	if ($.isNumeric(timestr)) {
		timestr = $.secondsToTime(timestr)
	}
	times = timestr.split(":");

	switch (type) {
		case "h":
			return parseInt(times[0]);
			break;
		case "m":
			return parseInt(times[1]);
			break;
		case "s":
			return parseInt(times[2]);
			break;
	}

	return -1;
}

$.timeToSeconds = function(timestr) {
	times = timestr.split(":");
	if (times.length==3)
		return parseInt(times[0]*3600)+parseInt(times[1]*60)+parseInt(times[2]);
	if (times.length==2)
		return parseInt(times[0]*60)+parseInt(times[1]);
	return parseInt(times[0]);
}

$.secondsToTime = function(seconds, sep) {


	var h = parseInt(seconds/3600);
	seconds = seconds - h*3600;
	var m = parseInt(seconds/60);
	seconds = seconds - m*60;

	if (h==24) h=0;
	if (typeof sep === "undefined")
		return $.strPad(h, 2)+":"+$.strPad(m, 2)+":"+$.strPad(seconds, 2);

	return $.strPad(h, 2)+"h"+$.strPad(m, 2)+"m"+$.strPad(seconds, 2)+"s";


}


$.toast = function(msg) {
	$("#toast").text(msg);
	$("#toast").popup("open");
}

$.strPad = function(i,l,s) {
	var o = i.toString();
	if (!s) { s = '0'; }
	while (o.length < l) {
		o = s + o;
	}
	return o;
}

function goLogout() {

	if (last_myhref=="uploader.html") {
		// ignore Timeout
		clearTimeout(idleTimer);
		idleTimer = setTimeout(function() {goLogout()}, constIDLE_TIMEOUT);
		return true;
	}

	if (currentLOGINTYPE == "admin") {
		$.reqSetData({LOGOUT: 1}, "silent")
			.always(function() {
				$("#liHome").trigger("click");
			})
	}

}

$(document).on("pagecreate", "#mainpage", function() {
    $(document).on("swipeleft swiperight", "#mainpage", function(e) {
        if ($(".ui-page-active").jqmData("panel") !== "open") {
            if (e.type === "swiperight") {
                $("#pnMenu").panel("open");
            }
        }
    });

	// idle timer
	idleTimer = setTimeout(function() {goLogout()}, constIDLE_TIMEOUT);

    $(document).on("click", ".btApply", function(e) {
        if (confirm("Apply current data?"))
			$.reqSetData();
    });

	$("#toast").on('popupafteropen', function() {
		setTimeout( function(){ $('#toast').popup('close') }, constToastCloseTimeout);
	})

});

$(document).on("pageinit", function() {
	$("a.menulink").each(function () {
		var myhref = $(this).attr("content");
		$(this).once("click", function() {
			$.ajax(myhref, {
				//cache: false
			})
			.done(function(html) {
				$("#maincontent").html(html);
				// utile per reload
				last_myhref = myhref;
			})
			.error(function() {
				$.toast("Error: '" + myhref + "' not found!");
			})
			.always(function() {
				$("#maincontent").trigger("create");
				$("#pnMenu").panel("close");
			});

		});
	});

});

$(document).on("pagecontainershow", function() {
	$('#maincontent').load("deviceinfo.html");
});

$.reloadPage = function() {
	var result = $.ajax(last_myhref, {
		//cache: false
	})
	.done(function(html) {
		$("#maincontent").html(html);
	})
	.error(function() {
		$.toast("Error: '" + last_myhref + "' not found!");
	})
	.always(function() {
		$("#maincontent").trigger("create");
		$("#pnMenu").panel("close");
	});
	return result;
}

$.isStatusUSBOk = function() {

	if ((+$("#USBSTATUS").val() == 3) || (+$("#USBSTATUS").attr("value")==3) ||
		(+$("#USBSTATUS").val() == 2) || (+$("#USBSTATUS").attr("value")==2)){
		alert("USB is busy. Please wait.");
		$("#mnPlayer").addClass("ui-disabled");
		$("#mnPlaylist").addClass("ui-disabled");
		$("#mnPlaylistCreator").addClass("ui-disabled");
		$("#mnScheduler").addClass("ui-disabled");
		$(".btApply").addClass("ui-disabled");
		$("#divDeviceFunction").addClass("ui-disabled");
		$("#btCopy").addClass("ui-disabled");
		$("#btAbortCopy").show();
		return false;
	}
	else if (($("#USBSTATUS").val() == "0") || $("#USBSTATUS").attr("value")=="0") {
		$("#mnUSBcopy").addClass("ui-disabled");
	}
	$("#btAbortCopy").hide();

	return true;
}

$.isStatusVarsOK = function() {

	if ($("#LOGINTYPE").val()=="none") {
		return false;
	}

	if ($("#LOGINTYPE").val()=="multi") {
		return false;
	}

	if (+$("#SDPRESENT").val()==0) {
		alert("SD memory card not present!\nInsert SD card to proceed.");
		$('#maincontent').load("deviceinfo.html");
		return false;
	}

	if (!$.isStatusUSBOk())
		return false;


	return true;
}

$.globalInit = function() {
	$("#mainpage").trigger("create");

	$("input[data-type='range'].immediate").on("slidestop", function() {
		$.reqSetData(this);
	});

	$("input[data-type='range'].immediate").on("keypress", function(e) {
		if (e.keyCode == 13) {
			var fVal = parseFloat($(this).val());
			if ((fVal>$(this).attr("max")) || (fVal<$(this).attr("min"))) {
				$.toast("Invalid value");
				return false;
			}

			$.reqSetData(this);
		}
	});

	$("select.immediate").change(function(e, data) {
		if (data=="load")
			return false;

		$.reqSetData(this);
	});

	$(".clReload").once("click", function(e) {
		e.stopImmediatePropagation();
		$.reloadPage();
	});

	$(".btCancelPopup").once('click', function() {
		$(".mypopup").popup("close");
		return false;
	});

	$(".clMediaButtons").once('click', function() {
		reqdata = {TYPE: $(this).attr('cmdtype')};
		var cmdtype = $(this).attr('cmdtype');
		switch (cmdtype) {
			case 'play':
			case 'delete':
				var target = $(".clTrackitem.ui-btn-active");
				$.extend(reqdata, {FILE_TYPE: target.attr('FILE_TYPE'), TRACK_IDX: target.attr("idx"), TRACK_NAME: target.attr('trackname')});
				$("#btPause").parent("div").removeClass("ui-disabled");
				$("#btStop").parent("div").removeClass("ui-disabled");
				break;
			case 'startplaylist':
			case 'rewplaylist':
			case 'pauseplaylist':
				var target = $("#PL_ITEMS option:selected");
				if (target.length==0) {
					// playlist page
					target = $("#PL_ITEMS > li > a.ui-btn-active");
					$.extend(reqdata, {FILE_TYPE: 'playlist', PL_IDX: target.attr("idx"), PL_NAME: target.attr("trackname")});
				} else {
					$.extend(reqdata, {FILE_TYPE: 'playlist', PL_IDX: target.attr("idx"), PL_NAME: target.val()});
				}
				break;
		}

		if (cmdtype=="delete") {
			if (!confirm("Are you sure to delete file?")) return false;
		}
		$.reqData("cmd", reqdata)
			.done(function() {
				if (cmdtype=="delete") {
					musicitemsobj = {};
					spotitemsobj = {};
					sensoritemsobj = {};
					plitemsobj = {};
					$(".mypopup").popup("close");
					$.reloadPage();
				}
			})
	});

	$("#btMediaControl").once("click", function() {
		$("#divMediaControl").toggle(300);
	});

	/*
    $('.mypopup').draggable({
        cursor: 'move'
    });
	*/

	$.trackFilter = function(obj) {
		var pattern=$("#trackfilter").val();
		var genre=$("#trackfilterGenre").val();
		var myparent=$(obj).parents(".ui-body-a")[0];
		if ((pattern=="") && (genre==""))
			$(".clTrackitem, .clUsbfileItem").show();

		$(myparent).find(".clTrackitem, .clUsbfileItem").each(function() {
			if (pattern!="") {
				if (($(this).attr("id3tag_author").toLowerCase().indexOf(pattern.toLowerCase())>=0) ||
					($(this).attr("id3tag_title").toLowerCase().indexOf(pattern.toLowerCase())>=0) ||
					($(this).attr("id3tag_album").toLowerCase().indexOf(pattern.toLowerCase())>=0)){
					$(this).show();
				} else {
					$(this).hide();
					return true;
				}
			}

			if (genre!="") {
				if ($(this).attr("id3tag_genre")==genre) {
					$(this).show();
				} else {
					$(this).hide();
				}
			}

		})

	}

	$("#trackfilter").keyup(function() {
		$.trackFilter(this);
	})

	$("#trackfilter").change(function() {
		$.trackFilter(this);
	})

	$("#trackfilterGenre").empty();
	$("#trackfilterGenre").append("<option value=\"\" selected>All genres</option>");
	$.each(mp3Genres, function(index, value) {
		$("#trackfilterGenre").append("<option value=\"" + index + "\">" + value + "</option>");
	});
	$("#trackfilterGenre").append("<option value=\"playlist\">m3u Playlist</option>");
	$("#trackfilterGenre").selectmenu("refresh");

	$("#trackfilterGenre").change(function() {
		$.trackFilter(this);
	})

	$("#SHOWTITLE").change(function() {
		var val = +$(this).val();
		$(".clTrackitem, .clUsbfileItem").each(function() {
			if (val==1) {
				if ($(this).attr("id3tag_title") == "undefined")
					return true;
				var myspan=$(this).find("span");
				$(this).html($(this).attr("id3tag_title") + " - "  + $(this).attr("id3tag_author") + " - "  + $(this).attr("id3tag_album") + " - " + mp3Genres[$(this).attr("id3tag_genre")]);
				$(this).append(myspan);
			} else {
				var myspan=$(this).find("span");
				$(this).html($(this).attr("trackname"));
				$(this).append(myspan);
			}
		})
	})


	$('#maincontent').height($('#maincontent').outerHeight()+$('.ui-header').outerHeight())
	//$('.ui-mobile .ui-page-active').css({overflow: 'hidden'})
}

$.INITdivDeviceFunction = function(val) {
		var DIOMval = $('#DIOM').val();
		$('select#DIOM option').remove();
		switch (parseInt(val)) {
			case 0: //StandardPlayer
				$("#mnPlayer").removeClass('ui-disabled');
				$("#mnPlaylist").addClass('ui-disabled');
				$("#mnPlaylistCreator").addClass('ui-disabled');
				$("#mnScheduler").addClass('ui-disabled');
				$("#mnSensor").removeClass('ui-disabled');

				$("#divPlayDelay").hide();
				$("#divDigitalIn").hide();
				$("#divRestartPlay").hide();

				$("select#DIOM").append($('<option value="0">').text(digitalIOMode[0]));
				$("select#DIOM").append($('<option value="1">').text(digitalIOMode[1]));

				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");

				$("#divDigitalIOMode").show();
				$("#divPowerOnAutoPlay").show();
				$("#divLoopPlay").show();
				$("#divRandomPlay").show();

				$.INITdivDigitalIOMode($('select#DIOM').val());


				break;

			case 1: //PlaylistSequence
				$("#mnPlayer").addClass('ui-disabled');
				$("#mnPlaylist").removeClass('ui-disabled');
				$("#mnPlaylistCreator").removeClass('ui-disabled');
				$("#mnScheduler").addClass('ui-disabled');
				$("#mnSensor").removeClass('ui-disabled');

				$("#divPlayDelay").hide();
				$("#divDigitalIn").hide();
				$("#divRestartPlay").hide();

				$("select#DIOM").append($('<option value="0">').text(digitalIOMode[0]));
				$("select#DIOM").append($('<option value="1">').text(digitalIOMode[1]));

				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");

				$("#divDigitalIOMode").show();
				$("#divPowerOnAutoPlay").show();
				$("#divLoopPlay").show();
				$("#divRandomPlay").show();

				$.INITdivDigitalIOMode($('select#DIOM').val());
				break;
			case 2: //AdvancedPlayer 1
				$("#mnPlayer").removeClass('ui-disabled');
				$("#mnPlaylist").addClass('ui-disabled');
				$("#mnPlaylistCreator").addClass('ui-disabled');
				$("#mnScheduler").addClass('ui-disabled');
				$("#mnSensor").removeClass('ui-disabled');

				$("select#DIOM").append($('<option value="2">').text(digitalIOMode[2]));
				$("select#DIOM").append($('<option value="4">').text(digitalIOMode[4]));
				$("select#DIOM").append($('<option value="5">').text(digitalIOMode[5]));
				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");

				$("#divDigitalIOMode").show();
				$("#divPowerOnAutoPlay").hide();

				$.INITdivDigitalIOMode($('select#DIOM').val());
				break;
			case 3: //AdvancedPlayer 2
				$("#mnPlayer").addClass('ui-disabled');
				$("#mnPlaylist").removeClass('ui-disabled');
				$("#mnPlaylistCreator").removeClass('ui-disabled');
				$("#mnScheduler").addClass('ui-disabled');
				$("#mnSensor").removeClass('ui-disabled');

				$("select#DIOM").append($('<option value="2">').text(digitalIOMode[2]));
				$("select#DIOM").append($('<option value="3">').text(digitalIOMode[3]));
				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");


				$("#divDigitalIOMode").show();
				$("#divPowerOnAutoPlay").hide();

				$.INITdivDigitalIOMode($('select#DIOM').val());
				break;
			case 4: //Scheduler
				$("#mnPlayer").addClass('ui-disabled');
				$("#mnPlaylist").addClass('ui-disabled');
				$("#mnPlaylistCreator").addClass('ui-disabled');
				$("#mnScheduler").removeClass('ui-disabled');
				$("#mnSensor").addClass('ui-disabled');

				$("#divDigitalIn").hide();

				$("#divPlayDelay").hide();
				$("#divDigitalInTimeout").hide();
				$("#divRestartPlay").hide();

				$("#divLoopPlay").hide();
				$("#divRandomPlay").hide();
				$("#divPowerOnAutoPlay").hide();

				$("select#DIOM").append($('<option value="0">').text(digitalIOMode[0]));
				$("select#DIOM").append($('<option value="6">').text(digitalIOMode[6]));
				if ($("select#DIOM").find("option[value='" + DIOMval + "']").length)
					$("select#DIOM").val(DIOMval);
				$('select#DIOM').selectmenu("refresh");

				break;
			default:
				$("#divDigitalIOMode").hide();
				$("#divPlayDelay").hide();
				$("#divDigitalInTimeout").hide();
				$("#divRestartPlay").hide();
				$("#divLoopPlay").hide();
				$("#divRandomPlay").hide();
				$("#divPowerOnAutoPlay").hide();
				break;
		}

}

$.INITdivDigitalIOMode = function(val) {
		$("#divDigitalIn").show();
		switch (parseInt(val)) {
			case 0: //Free
				$("#mnIOSettings").removeClass('ui-disabled');
				$("#divDigitalIn").hide();
				$("#divPlayDelay").hide();
				$("#divDigitalInTimeout").hide();

				$("#divRestartPlay").hide();
				$("#divDigitalInIObits").hide();

				break;
			case 1: //Player
				$("#mnIOSettings").removeClass('ui-disabled');
				$("#divDigitalInTimeout").show();
				$("#divPlayDelay").show();
				$("#divLoopPlay").show();
				$("#divRandomPlay").show();

				$("#divDigitalInIObits").hide();
				$("#divDigitalInContinuousPlay").hide();
				$("#divDigitalInInterrupt").hide();
				$("#divRestartPlay").hide();
				break;
			case 2: //BinaryCode
				$("#mnIOSettings").removeClass('ui-disabled');
				$("#divLoopPlay").hide();
				$("#divRandomPlay").hide();

				$("#divDigitalInIObits").show();
				$("#divDigitalInTimeout").show();
				$("#divDigitalInContinuousPlay").show();
				$("#divDigitalInInterrupt").show();

				$("#divPlayDelay").show();
				$("#divRestartPlay").show();

				$("label[for='DIBC']").text("# bits for Binary Code (1-8)")
				$("#DIBC").attr("max", 8).slider("refresh");
				break;
			case 3: //Playlist
				$("#mnIOSettings").removeClass('ui-disabled');

				$("#divDigitalInTimeout").show();
				$("#divDigitalInContinuousPlay").show();
				$("#divDigitalInInterrupt").show();

				$("#divDigitalInIObits").show();
				$("#divPlayDelay").show();
				$("#divRestartPlay").show();
				$("label[for='DIBC']").text("# In for Playlist Direct (1-8)")
				$("#DIBC").attr("max", 8).slider("refresh");
				break;
			case 4: //Keypad
				$("#mnIOSettings").addClass('ui-disabled');
				$("#divDigitalInIObits").hide();

				$("#divPlayDelay").show();
				$("#divRestartPlay").show();
				$("#divDigitalInTimeout").show();
				$("#divDigitalInContinuousPlay").show();
				$("#divDigitalInInterrupt").show();
				$("#divPlayDelay").show();
				$("#divRestartPlay").show();
				break;
			case 5: //Museum mode
				$("#mnIOSettings").removeClass('ui-disabled');
				$("#divLoopPlay").hide();
				$("#divRandomPlay").hide();

				$("#divDigitalInIObits").show();
				$("#divDigitalInTimeout").show();
				$("#divDigitalInContinuousPlay").show();
				$("#divDigitalInInterrupt").show();

				$("#divPlayDelay").show();
				$("#divRestartPlay").show();

				$("label[for='DIBC']").text("# bits for Binary Code (1-6)")
				$("#DIBC").attr("max", 6).slider("refresh");
				break;
			case 6: //Priority message
				$("#mnIOSettings").removeClass('ui-disabled');
				$("#divDigitalIn").show();
				$("#divDigitalInIObits").show();
				$("label[for='DIBC']").text("# In for Priority message (1-6)")
				$("#DIBC").attr("max", 6).slider("refresh");
				//$("#divPlayDelay").hide();
				$("#divPlayDelay").show();
				$("#divDigitalInTimeout").hide();
				$("#divRestartPlay").hide();
				break;

			default:
				$("#divDigitalInIObits").hide();
				$("#divDigitalIOMode").hide();
				$("#divPlayDelay").hide();
				$("#divDigitalInTimeout").hide();
				$("#divRestartPlay").hide();
				$("#divLoopPlay").hide();
				$("#divRandomPlay").hide();
				break;
		}

}

$.audioAUEQ5ONInitDiv = function(val) {

	val = 0; //waiting EQ5 ready (to do)

	if (val==0) {
		$("#divAUEQ5ONdata").show();
		$("#divAUEQ5ONdata2").hide();
	} else {
		$("#divAUEQ5ONdata").hide();
		$("#divAUEQ5ONdata2").show();
	}

}

$.audioAUADMIXONInitDiv = function(val) {

/* 	if (val==1) {
		$("#divAUADMIXONdata").show();
	} else {
		$("#divAUADMIXONdata").hide();
	} */
}

updateClock = function() {
	sec = $.timeToSeconds($("#myclock").text())+1;
	if (sec==86400) sec=0;
	$("#myclock").text($.secondsToTime(sec));
	clock = setTimeout(updateClock, 1000);
}

$.clockInit = function() {

	$.globalInit();

	//alert(DisplayDstSwitchDates());


	$.initClockData = function() {
		clearTimeout(clock);
		sec = parseInt($("#RTCCHH").val())*3600 + parseInt($("#RTCCMM").val())*60 + parseInt($("#RTCCSS").val());
		$("#myclock").text($.secondsToTime(sec));
		clock = setTimeout(updateClock, 1000);

		$("#mydate").text($("#RTCCWD option:selected").text() + " " + $.strPad($("#RTCCD").val(),2) + "/" + $.strPad($("#RTCCM").val(),2) + "/" + $.strPad($("#RTCCY").val(),2));

		// NTP
		var out = DisplayDstSwitchDates();
		if (out !== null) {
			$("#DSTSDATE").val(out[0][0]);
			$("#DSTSTIME").val(out[0][1]);
			$("#DSTEDATE").val(out[1][0]);
			$("#DSTETIME").val(out[1][1]);
		}
		var time = $("#NTPSYNCTM").val().split(":");
		$("#synchour").val(time[0]).selectmenu("refresh")
		$("#syncmin").val(time[1]).selectmenu("refresh")

		$("#NTPLOG option").remove();
		$("#NTPLOG").append("<option>" + "UTC: " + $("#NTPLOG1UTC").val() + "; Local Time: " + $("#NTPLOG1LT").val() + "</option>");
		$("#NTPLOG").append("<option>" + "UTC: " + $("#NTPLOG2UTC").val() + "; Local Time: " + $("#NTPLOG2LT").val() + "</option>");
		$("#NTPLOG").append("<option>" + "UTC: " + $("#NTPLOG3UTC").val() + "; Local Time: " + $("#NTPLOG3LT").val() + "</option>");
		$("#NTPLOG").selectmenu("refresh")
	}

	$.reqGetData()
		.always(function() {

		})
		.done(function() {
			$.initClockData()
		});

	if (!$("#RTCCD option").length)
		for (i=1; i<=31; i++)
			$("#RTCCD").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCM option").length)
		for (i=1; i<=12; i++)
			$("#RTCCM").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCY option").length)
		for (i=2016; i<=2050; i++)
			$("#RTCCY").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCHH option").length)
		for (i=0; i<=23; i++)
			$("#RTCCHH").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCMM option").length)
		for (i=0; i<=59; i++)
			$("#RTCCMM").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	if (!$("#RTCCSS option").length)
		for (i=0; i<=59; i++)
			$("#RTCCSS").append("<option value=\"" + i + "\">" + $.strPad(i, 2) + "</option>");

	$("#NTPEN").once("change", function(e, data) {
		if (data=="load")
			return false;

		if ($(this).val()=="0") {
			$.reqSetData($("#NTPEN"));
		}
		else { //init NTP setup
			$.reqSetData($(".isntpfield")).done(function() {
				clearTimeout(clock);
				sec = parseInt($("#RTCCHH").val())*3600 + parseInt($("#RTCCMM").val())*60 + parseInt($("#RTCCSS").val());
				$("#myclock").text($.secondsToTime(sec));
				clock = setTimeout(updateClock, 1000);

				$("#mydate").text($("#RTCCWD option:selected").text() + " " + $.strPad($("#RTCCD").val(),2) + "/" + $.strPad($("#RTCCM").val(),2) + "/" + $.strPad($("#RTCCY").val(),2));

			});
		}
	})

	$(".btApplyNTP").once("click", function(e) {

		if (!confirm("Apply current NTP data?"))
			return false;

		$.reqSetData($(".isntpfield")).done(function() {
				clearTimeout(clock);
				sec = parseInt($("#RTCCHH").val())*3600 + parseInt($("#RTCCMM").val())*60 + parseInt($("#RTCCSS").val());
				$("#myclock").text($.secondsToTime(sec));
				clock = setTimeout(updateClock, 1000);

				$("#mydate").text($("#RTCCWD option:selected").text() + " " + $.strPad($("#RTCCD").val(),2) + "/" + $.strPad($("#RTCCM").val(),2) + "/" + $.strPad($("#RTCCY").val(),2));

				$.reqGetData($("#NTPSYNCDT"));
		});
	})

	$("#btSaveDateTime").once("click", function() {
		if (!confirm("Apply the new date and time?"))
			return false;

		$.reqSetData([$("#RTCCWD"), $("#RTCCD"), $("#RTCCM"), $("#RTCCY"), $("#RTCCHH"), $("#RTCCMM"), $("#RTCCSS")])
			.done(function() {
				clearTimeout(clock);
				sec = parseInt($("#RTCCHH").val())*3600 + parseInt($("#RTCCMM").val())*60 + parseInt($("#RTCCSS").val());
				$("#myclock").text($.secondsToTime(sec));
				clock = setTimeout(updateClock, 1000);

				$("#mydate").text($("#RTCCWD option:selected").text() + " " + $.strPad($("#RTCCD").val(),2) + "/" + $.strPad($("#RTCCM").val(),2) + "/" + $.strPad($("#RTCCY").val(),2));

			})
	})

	$(".btSyncClock").once("click", function() {
        if (!confirm("Syncronize clock?"))
			return false;

		var now = new Date();

		$("#RTCCHH").val(now.getHours());
		$("#RTCCMM").val(now.getMinutes());
		$("#RTCCSS").val(now.getSeconds());

		$("#RTCCD").val(now.getDate());
		$("#RTCCM").val(now.getMonth()+1);
		$("#RTCCY").val(now.getFullYear());

		var weekday = now.getDay()==0?7:now.getDay();
		$("#RTCCWD").val(weekday);

		$("select.cltime").selectmenu("refresh");

		//$(".btApplyClock").trigger("click", [true]);
		$.reqSetData([$("#RTCCWD"), $("#RTCCD"), $("#RTCCM"), $("#RTCCY"), $("#RTCCHH"), $("#RTCCMM"), $("#RTCCSS")])
			.done(function() {
				$.reqGetData()
					.done(function() {
						$.initClockData()
					})
			})
	})

	// ---------------------------------------------------
	// DST
	$("#DSTEN").once("change", function(e, data) {
		if (data=="load")
			return false;

		if ($(this).val()=="0") {
			$.reqSetData($("#DSTEN"));
		}
		else { //init DST setup
			$.reqSetData($(".isdstfield"));
		}
	})

	$(".btApplyDST").once("click", function(e) {

		if (!confirm("Apply current DST data?"))
			return false;

		$.reqSetData($(".isdstfield"));
	})

	$("#DSTEN").change(function() {
		if ($(this).val()==1) {
			$(".dstfield").removeClass("ui-disabled")
			$("#divDstEnabled").slideDown();
		} else {
			$(".dstfield").addClass("ui-disabled")
			$("#divDstEnabled").slideUp();
		}
		$(this).slider("refresh");
	})


	// ---------------------------------------------------
	// NTP
	$("#NTPEN").change(function() {
		if ($(this).val()==1) {
			$(".ntpfield").removeClass("ui-disabled")
			$("#divNtpEnabled").slideDown();
		} else {
			$(".ntpfield").addClass("ui-disabled")
			$("#divNtpEnabled").slideUp();
		}
		$(this).slider("refresh");
	})

	$(".customsel").change(function() {
		$(this).parents('.divCustomsel').find('input').val($(this).val())
	})

	$(".custominput").on('keypress', function(e) {
		if (String.fromCharCode(e.keyCode).match(/[^0-9a-zA-Z_.-]/g))
			return false;
	});

	$(".custominput").change(function() {
		var customselobj = $(this).parents('.divCustomsel').find('select')
		customselobj.val($(this).val())
		if (customselobj.val()==null)
			customselobj.val('custom')
		customselobj.selectmenu("refresh")
	})

	$("#NTPSYNCNOW").once("click", function() {
		if (!confirm("The NTP sync process uses the last saved NTP setup.\nIf you changed it,\npress the \"Save\" button before starting the sync process"))
			return false;

		$.reqSetData($("#NTPSYNCNOW"))
			.done(function() {
				constToastCloseTimeout = constToastCloseTimeoutDEFINE*2;
				$("#toast").once('popupafterclose', function() {
					$("#toast").once('popupafterclose', function() {});
					$.reloadPage();
				})
				$.toast("NTP is synchronizing!\nPlease wait...");
				constToastCloseTimeout = constToastCloseTimeoutDEFINE;
			});
	})

	$("#synchour, #syncmin").change(function() {
		$("#NTPSYNCTM").val($("#synchour").val() + ":" + $("#syncmin").val());
	})


}

$.audioInit = function() {

	$.globalInit();

	$.audioAUEQ5ONInitDiv($("#AUEQ5ON").val());
	$.audioAUADMIXONInitDiv($("#AUADMIXON").val());


	$("#AUEQ5ON").change(function() {
		$.audioAUEQ5ONInitDiv($(this).val());
	});

	$("#AUADMIXON").change(function() {
		$.audioAUADMIXONInitDiv($(this).val());
	});

	$.checkAUOT = function() {
		switch ($("#AUOT").val()) {
			case "0": //stereo
				$("#AUOI").selectmenu("disable");
				break;
			case "1": // mono
				$("#AUOI").selectmenu("enable");
				break;
		}
	}

	$.checkAUOT();

	$("#AUOT").change(function() {
		$.checkAUOT();
	});


	function updateAudiolin() {

		if (+$("#AULINL2").val()>+$("#AULINL").val())
			$("#AULINL2").val($("#AULINL").val()).slider("refresh")

		$.reqSetData([$("#AULINL"), $("#AULINL2")])
	}

	$("#AULINL").on("keypress", function(e) {
		if (e.keyCode == 13) {
			var fVal = parseFloat($(this).val());
			if ((fVal>$(this).attr("max")) || (fVal<$(this).attr("min"))) {
				$.toast("Invalid value");
				return false;
			}
			updateAudiolin()
		}
	});
	$("#AULINL2").on("keypress", function(e) {
		if (e.keyCode == 13) {
			var fVal = parseFloat($(this).val());
			if ((fVal>$(this).attr("max")) || (fVal<$(this).attr("min"))) {
				$.toast("Invalid value");
				return false;
			}
			updateAudiolin()
		}
	});

	$("#AULINL").on("slidestop", function() {
		updateAudiolin()
	})

	$("#AULINL2").on("slidestop", function() {
		updateAudiolin()
	})


}

$.deviceinfoInit = function() {
	$.globalInit();

	$.reqGetData()
		.always(function() {
			if ($.isStatusUSBOk()) {
				$.INITdivDeviceFunction($.inArray($("#DEVFN").text(), tblDEVFN));
				$.INITdivDigitalIOMode($.inArray($("#DIOM").text(), tblDIOM));
			}
		});
}

$.devfuncInit = function() {

	$.globalInit();

	$.INITdivDeviceFunction($("#DEVFN").val());
	$.INITdivDigitalIOMode($("#DIOM").val());

	$("#DEVFN").once("change", function() {
        if (!confirm("Apply new Device Function?\nNOTE: System will be reinitialized!"))
			return false;

		$.reqSetData($("#DEVFN"),"silent")
			.done(function() {

				constToastCloseTimeout = constToastCloseTimeoutLongDEFINE;
				$("#toast").once('popupafterclose', function() {
					$("#toast").once('popupafterclose', null);
					$.reloadPage();
				})
				$.toast("Please wait... the system is doing its initialization");
				constToastCloseTimeout = constToastCloseTimeoutDEFINE;

			});
	});
	$("#DIOM").change(function() {
		$.INITdivDigitalIOMode($(this).val());
	});
	$("#DIINT").change(function() {
		if (parseInt($(this).val())) {
			$("#divRestartPlay").removeClass("ui-disabled");
		} else {
			$("#divRestartPlay").addClass("ui-disabled");
			$("#DIRP").val("0").slider("refresh");
		}
	});


}

$.iosetInit = function() {

/* 		$.reqData("get", {DEVFN: '', DIOM: '', DIBC: ''})
		.always(function(data) {
			$.reqGetData()
				.always(function() {

				});
		});
 */

	$.reqGetData()
		.done(function(data) {

			$.globalInit();

			// init flags
			$(".isflag").each(function() {
				var flag = $(this).attr("id");
				var idvar = "";
				idvar = flag.substring(0, flag.indexOf("_"));
				flag = parseInt(flag.substring(flag.indexOf("_")+1, flag.length));
				$(this).val( + (($("#" + idvar).val() & (1 << (flag-1)))>0));
				$(this).trigger("change");
				var value = $(this).val()!="" ? $(this).val() : $(this).attr("value");
				if (parseInt(value)==0) {
					// OUTPUT
					$("#DOM"+flag).parent().show();

					if (($("#DOM"+flag).val()>="4")&&($("#DOM"+flag).val()<="5")) {
						// BLINK mode
						$("#DOPM_"+flag).next().hide();
						$("#DOPT"+flag).parent().hide();
					} else {
						$("#DOPM_"+flag).next().show();
						if (parseInt($("#DOPM_"+flag).val())) {
							$("#DOPT"+flag).parent().show();
						} else {
							$("#DOPT"+flag).parent().hide();
						}
					}
				} else {
					// INPUT
					$("#DOM"+flag).parent().hide();
					//$("#DOPM_"+flag).val(0).trigger("change");
					if (parseInt($("#DOPM_"+flag).val())) {
						$("#DOPT"+flag).parent().show();
					} else {
						$("#DOPT"+flag).parent().hide();
					}
					$("#DOPM_"+flag).next().hide();
				}
			});

			// init values
			$("[id^='DOSET']").each(function() {
				// verifica se nascondere l'impostazione VALUE
				var flag = $(this).attr("id").slice(-1);
				var mask = (1 << (flag-1));
				if ($("#DISTS").val() & mask) {
					$(this).val(1);
				}
				if ($("#DIOC_"+flag).val()==1) {
					// input mode
					var mytext = $(this).val()==1 ? 'ON' : 'OFF';
					$(this).parent("td").html('<span id="DOSET' + flag + '" >' + mytext + '</span>');
				} else {
					// output mode

				}

			});

			switch (parseInt($('#DIOM').val())) {

				case 0: // Free
					break;

				case 1: // Player
					for (var i=1; i<= 6; i++) {
						$('#tblIO tr:eq(' + i + ')').find("th:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(2)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(3)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(4)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(5)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(6)").addClass("ui-disabled");
					}
					break;

				case 2: // binarycode
					var DIBCvar = parseInt($('#DIBC').val());
					for (var i=1; i<= DIBCvar; i++) {
						$('#tblIO tr:eq(' + i + ')').find("th:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(2)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(3)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(4)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(5)").html('<span id="DIOC_' + i + '" value="1" class="isflag ui-disabled">IN</span>');
					}

					break;
				case 3: // playlist direct
					var DIBCvar = parseInt($('#DIBC').val());
					for (var i=1; i<= DIBCvar; i++) {
						$('#tblIO tr:eq(' + i + ')').find("th:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(2)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(3)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(4)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(5)").html('<span id="DIOC_' + i + '" value="1" class="isflag ui-disabled">IN</span>');
					}

					break;
				case 4: // keyboard16
					$('#tblIO td:nth-child(2),#tblIO th:nth-child(2)').remove();
					$('#tblIO td:nth-child(3),#tblIO th:nth-child(3)').remove();
					$('#tblIO td:nth-child(3),#tblIO th:nth-child(3)').remove();
					$('#tblIO td:nth-child(3),#tblIO th:nth-child(3)').remove();

					break;
				case 5: // Museum mode
					var DIBCvar = parseInt($('#DIBC').val());
					for (var i=1; i<= DIBCvar; i++) {
						$('#tblIO tr:eq(' + i + ')').find("th:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(2)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(3)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(4)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(5)").html('<span id="DIOC_' + i + '" value="1" class="isflag ui-disabled">IN</span>');
					}
					for (var i=7; i<= 8; i++) {
						$('#tblIO tr:eq(' + i + ')').find("th:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(2)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(3)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(4)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(5)").html('<span id="DIOC_' + i + '" value="1" class="isflag ui-disabled">IN</span>');
					}
					break;
				case 6: // Priority message
					var DIBCvar = parseInt($('#DIBC').val());
					for (var i=1; i<= DIBCvar; i++) {
						$('#tblIO tr:eq(' + i + ')').find("th:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(2)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(3)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(4)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(5)").html('<span id="DIOC_' + i + '" value="1" class="isflag ui-disabled">IN</span>');
					}
					for (var i=7; i<= 8; i++) {
						$('#tblIO tr:eq(' + i + ')').find("th:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(0)").addClass("ui-disabled");
						$('#tblIO tr:eq(' + i + ')').find("td:eq(2)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(3)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(4)").html('');
						$('#tblIO tr:eq(' + i + ')').find("td:eq(5)").html('<span id="DIOC_' + i + '" value="1" class="isflag ui-disabled">IN</span>');
					}
					break;
			}

			if (parseInt($('#DEVFN').val())==4) { // Scheduler
				$('select.clDOM option[value="3"]').text("Play + Blink in Suspension");
			}
			else  {
				$('select.clDOM option[value="3"]').text("Play + Blink in Pause");
			}
			$('select.clDOM').selectmenu("refresh");

			$("[id^='DOPM_']").change(function() {
				var flag = $(this).attr("id");
				var flag = parseInt(flag.substring(flag.indexOf("_")+1, flag.length));
				if (parseInt($(this).val())) {
					$("#DOPT"+flag).parent().show();
				} else {
					$("#DOPT"+flag).parent().hide();
				}
			});

			$("[id^='DIOC_']").change(function() {
				var flag = $(this).attr("id");
				var flag = parseInt(flag.substring(flag.indexOf("_")+1, flag.length));
				if (parseInt($(this).val())==0) {
					$("#DOM"+flag).parent().show();
					$("#DOPM_"+flag).next().show();
				} else {
					$("#DOM"+flag).parent().hide();
					$("#DOPM_"+flag).val(0).trigger("change");
					$("#DOPM_"+flag).next().hide();
				}
			});

			$(".clDOM").change(function() {
				var flag = $(this).attr("id");
				flag = parseInt(flag.substring(flag.length - 1));
				if (($(this).val()=="4") || ($(this).val()=="5")) {
					// BLINK mode in play or stop
					$("#DOPM_"+flag).next().hide();
					$("#DOPT"+flag).parent().hide();
				} else {
					$("#DOPM_"+flag).next().show();
					if ($("#DOPM_"+flag).val()=="1") {
						$("#DOPT"+flag).parent().show();
					}
				}
			});

			$(".clDOPT").blur(function() {
				// validate value
				var value = parseFloat($(this).val());
				if (value > $(this).attr("max"))
					$(this).val($(this).attr("max"));
				if (value < $(this).attr("min"))
					$(this).val($(this).attr("min"));
			});

			$("#btApplyIO").once("click", function() {
				if (!confirm("Apply current data?"))
					return false;

				$("#DIOIM").val(0);
				$("#DOPM").val(0);
				$("#DIOC").val(0);

				$(".isflag").each(function() {
					var flag = $(this).attr("id");
					var idvar = "";
					idvar = flag.substring(0, flag.indexOf("_"));
					flag = parseInt(flag.substring(flag.indexOf("_")+1, flag.length));
					mask = (1 << (flag-1));
					target = $("#" + idvar);
					var value = $(this).val()!="" ? $(this).val() : $(this).attr("value");
					if (parseInt(value)) {
						target.val(parseInt(target.val()) + mask);
					}

				});

				$.reqSetData()
					.done(function() {

						// init values
						$("[id^='DOSET']").each(function() {
							// verifica se nascondere l'impostazione VALUE
							var flag = $(this).attr("id").slice(-1);
							if ($("#DIOC_"+flag).val()==1) {
								// input mode
								var mytext = $(this).val()==1 ? 'ON' : 'OFF';
								$(this).parent("td").html('<span id="DOSET' + flag + '" >' + mytext + '</span>');
							} else if (!$("#DOSET"+flag).is("select")) {
								$(this).parent("td").html('<select class="isfield immediate clDOSET" name="DOSET' + flag + '" id="DOSET' + flag + '"  data-role="slider" data-mini="true"><option value="0">OFF</option><option value="1">ON</option></select>');
								$("select#DOSET"+flag).slider().slider("refresh");
							}

						});

						$.globalInit();
					});

			});
		})
}

$.sformat = function(s) {
      var fm = [
            Math.floor(s / 60), // MINUTES
            s % 60 // SECONDS
      ];
      return $.map(fm, function(v, i) { return ((v < 10) ? '0' : '') + v; }).join(':');
}


$.fillupTrackList = function(data, target) {
	var filetype = target.attr("FILE_TYPE");
	if (target.is("ol")) {
		$.each(data, function(key, val) {
			$.each(val, function(filename, options) {
				var trackClass = "";
				switch (filetype) {
					case 'sensor':
						trackClass = 'clSensorTrack';
						break;
					case 'spot':
						trackClass = 'clSpotTrack';
						break;
				}
				target.append(
					$("<li>").append(
						$('<a class="clTrackitem ' + trackClass + '" idx="'+key+'" trackname="'+filename+'" FILE_TYPE="'+filetype+'" option0="' + options[0] + '" id3tag_genre="' + options[1] + '" id3tag_author="' + options[2] + '" id3tag_title="' + options[3] + '" id3tag_album="' + options[4] + '" href="#">').text(filename).append(
							$("<span>").attr("class", "ui-li-count").text($.sformat(options[0]))
						)
	 				)
				);
			});
		});
		target.listview().listview("refresh");
		$("#SHOWTITLE").trigger("change");

	} else if (target.is("select")) {
		target.find('option').remove()
		$.each(data, function(key, val) {
			$.each(val, function(filename, options) {
				target.append($('<option/>', {value: filename, text: filename }).attr({idx: key, FILE_TYPE: filetype, opt: options}));
			});

		});

		var selected = target.val(); // cache selected value, before reordering
		var opts_list = target.find('option');
		opts_list.sort(function(a, b) { return $(a).text() > $(b).text() ? 1 : -1; });
		target.html('').append(opts_list);
		target.val(selected); // set cached selected value

		target.selectmenu("refresh");
	}

}

$.clTrackitemInit = function(e) {
	$(".clTrackitem").removeClass("ui-btn-active");
	$(this).addClass("ui-btn-active");
	$("#divTrackCmds").removeClass("ui-disabled");
	$(".clTrackitem").removeClass("ui-disabled");

	if ($(this).parents("#tabs").hasClass("notrackpopup"))
		return true;

	if ($(this).attr("file_type")=="playlist") {
		$("#ppPlControl > #popH").text("Playlist control");
		$("#ppPlControl > #tagsH").html("");
		$("#ppPlControl").popup("open");
	} else {
		$("#ppTrack > #popH").text($(e.target).attr("trackname"));
		$("#ppTrack > #tagsH").html("TAGS: " + $(e.target).attr("id3tag_title") + " / " + $(e.target).attr("id3tag_author") + " / " + $(e.target).attr("id3tag_album") + " / " + mp3Genres[$(e.target).attr("id3tag_genre")]);
		$("#ppTrack").popup("open");
	}


}

$.playlistInit = function() {
	$.globalInit();


	$("#PL_ITEMS option").remove();
	//$("#divMediaButtons").addClass("ui-disabled");

	$.reqGetData()
		.always(function() {

			if (!$.isStatusVarsOK()) {
				return false;
			}

			$.reqData("get", {PL_ITEMS: ''})
				.done(function(data) {
					if ($("#PL_ITEMS option").length==0) $("#divMediaButtons").addClass("ui-disabled");
					$(".clTrackitem").once("click", $.clTrackitemInit);

					if ($("#FAKETRINPL").val()!="") {
						alert("Some playlist have some fake tracks inside. Add missing tracks, go to Serivce menu and click on \"System Reboot and Scan SD content\" to solve the problem.\n\nPlaylist with fake tracks:\n" + $("#FAKETRINPL").val())
					}


				});
		})
}

$.plelementInit = function() {
	if ($("#PL_ITEMS option").length==0) {
		$("#btStartPl").parent("div").addClass("ui-disabled");
		$("#btRewPl").parent("div").addClass("ui-disabled");
		$("#btPausePl").parent("div").addClass("ui-disabled");
		$("#btStop").parent("div").addClass("ui-disabled");
		return true;
	}

	$("#btStartPl").parent("div").removeClass("ui-disabled");
	$("#btRewPl").parent("div").removeClass("ui-disabled");
	$("#btPausePl").parent("div").removeClass("ui-disabled");
	$("#btStop").parent("div").removeClass("ui-disabled");

	return $.reqData("get", {PL_ITEM_ELEM: $("#PL_ITEMS").val()})
		.done(function(data) {
			$("#plsortable").empty();
			if ($("#PL_ITEMS").val()==null)
				$("#pltitle").text('');
			else
				$("#pltitle").text($("#PL_ITEMS").val());

			if (typeof $.parseJSON(data).res.get.PL_ITEM_ELEM[$("#PL_ITEMS").val()] === 'undefined')
				return true;
			$.each($.parseJSON(data).res.get.PL_ITEM_ELEM[$("#PL_ITEMS").val()].elements, function(key, val) {

				$("#plsortable").append(
					$("<li>").append(
						$('<a class="clTrackitem" idx="'+key+'" trackname="'+val.FILE_NAME+'" FILE_TYPE="' + val.FILE_TYPE + '" id3tag_len="'+val.id3tag[0]+'" id3tag_genre="'+val.id3tag[1] + '" id3tag_author="' + val.id3tag[2] + '" id3tag_album="' + val.id3tag[4] + '" id3tag_title="'+val.id3tag[3]+'" href="#">').text(val.FILE_NAME).append(
							$("<span>").attr("class", "ui-li-count").text($.sformat(val.id3tag[0]))
						)
					)
				);
			});
			$("#plsortable").listview("refresh");

			$(".clTrackitem").once("click", function() {
				$(".clTrackitem").removeClass("ui-btn-active");
				$(this).addClass("ui-btn-active");
				$("#divTrackCmds").removeClass("ui-disabled");
				$(".clTrackitem").removeClass("ui-disabled");
			});
			$("#TRACKSORT").trigger("change");
		});
}

$.plcreatorInit = function() {
	$.globalInit();
	$("#PL_ITEMS option").remove();

	// sort function callback
	function sort_li(a, b) {
		switch (+($("#TRACKSORT").val())) {
			case 0:
				return ($(b).find("a").attr("trackname")) < ($(a).find("a").attr("trackname")) ? 1 : -1;    
			case 1:
				return ($(b).find("a").attr("id3tag_title")) < ($(a).find("a").attr("id3tag_title")) ? 1 : -1;    
			case 2:
				return ($(b).find("a").attr("id3tag_author")) < ($(a).find("a").attr("id3tag_author")) ? 1 : -1;    
			case 3:
				return ($(b).find("a").attr("id3tag_album")) < ($(a).find("a").attr("id3tag_album")) ? 1 : -1;    
			case 4:
				return ($(b).find("a").attr("id3tag_genre")) < ($(a).find("a").attr("id3tag_genre")) ? 1 : -1;    
		}
		
	}	
	
	$("#TRACKSORT").change(function() {
	
		var val = +$(this).val();
		$(".clTrackitem, .clUsbfileItem").each(function() {
			if (val>0) {
				if ($(this).attr("id3tag_title") == "undefined")
					return true;
				var myspan=$(this).find("span");
				$(this).html($(this).attr("id3tag_title") + " - "  + $(this).attr("id3tag_author") + " - "  + $(this).attr("id3tag_album") + " - " + mp3Genres[$(this).attr("id3tag_genre")]);
				$(this).append(myspan);
			} else {
				var myspan=$(this).find("span");
				$(this).html($(this).attr("trackname"));
				$(this).append(myspan);
			}
		})	
	
	
		$("#MUSIC_ITEMS li").sort(sort_li) // sort elements
						  .appendTo('#MUSIC_ITEMS'); // append again to the list	

		$("#SPOT_ITEMS li").sort(sort_li) // sort elements
						  .appendTo('#SPOT_ITEMS'); // append again to the list	

		$("#SENSOR_ITEMS li").sort(sort_li) // sort elements
						  .appendTo('#SENSOR_ITEMS'); // append again to the list	
						  
						  
	})	
	
	
	$.reqGetData()
		.done(function() {
			switch ($("#LOGINTYPE").val()) {
				case "guest":
					$("#mytitle").text("Playlist details");
					break;
				case "admin":
					$("#mytitle").text("Playlist Creator");
					break;
			}
		})
		.always(function() {

			if (!$.isStatusVarsOK()) {
				return false;
			}

			$.reqData("get", {PL_ITEMS: ''})
				.done(function() {
					var res = $.plelementInit();
					if ($.isPlainObject(res)) {
						res.done(function() {
							$.initTracksTab();
						})
					} else {
						$.initTracksTab();
						$("#addtrack").addClass("ui-disabled");
						$("#plclone").addClass("ui-disabled");
						$("#plsave").addClass("ui-disabled");
						$("#pldelete").addClass("ui-disabled");
						plitemsobj = {};
					}

					if ($("#FAKETRINPL").val()!="") {
						alert("Some playlist have some fake tracks inside. Add missing tracks, go to Serivce menu and click on \"System Reboot and Scan SD content\" to solve the problem.\n\nPlaylist with fake tracks:\n" + $("#FAKETRINPL").val())
					}

				});

			$("#PL_ITEMS").change(function() {
				lastSelectedPl = $("#PL_ITEMS").val();
				$.plelementInit();
			})

			$("#btNext").once("click", function() {
				var target = $(".clTrackitem.ui-btn-active");
				if (target.length==0) return false;

				target.removeClass("ui-btn-active");

				target = target.parent("li").next().children("a");
				if (target.length==0) return false;
				target.addClass("ui-btn-active");

				$("#btPlay").trigger("click");
			})

			$("#btPrev").once("click", function() {
				var target = $(".clTrackitem.ui-btn-active");
				if (target.length==0) return false;

				target.removeClass("ui-btn-active");

				target = target.parent("li").prev().children("a");
				if (target.length==0) return false;
				target.addClass("ui-btn-active");

				$("#btPlay").trigger("click");
			})

			$("#addtrack").once("click", function() {
				var dataitems = $("#tabs a.tabnav.ui-btn-active").attr("tabdataidx");
				var obj = $("#" + dataitems + " li a.ui-btn-active").parent("li");
				$("#" + dataitems + " li a.ui-btn-active").removeClass("ui-btn-active");
				$("#divTrackCmds").addClass("ui-disabled");
				if ($("#PL_ITEMS option").length==0) {
					$("#btPause").parent("div").addClass("ui-disabled");
					$("#btStop").parent("div").addClass("ui-disabled");
				} else {
					$("#btStartPl").parent("div").removeClass("ui-disabled");
				}

				var obj2 = obj.clone();
				switch(dataitems) {
					case "MUSIC_ITEMS":
						obj2.attr("FILE_TYPE", "music");
						break;
					case "SPOT_ITEMS":
						obj2.attr("FILE_TYPE", "spot");
						break;
					case "SENSOR_ITEMS":
						obj2.attr("FILE_TYPE", "sensor");
						break;
				}

				$("#plsortable").append(obj2);

				$(".clTrackitem").once("click", $.clTrackitemInit);

				$("#plsave").buttonMarkup({theme: 'c'});
			})

			$("#plsortable").sortable({
				update: function () {
					$("#plsave").buttonMarkup({theme: 'c'});
				}
			}).disableSelection();

			$.plButtonsEnable = function(bEnable) {
				if (bEnable) {
					$("#plsave").removeClass('ui-disabled');
					$("#pldelete").removeClass('ui-disabled');
					$("#plclone").removeClass('ui-disabled');
				} else {
					$("#plsave").addClass('ui-disabled');
					$("#pldelete").addClass('ui-disabled');
					$("#plclone").addClass('ui-disabled');
				}
			}

			$("#plsave").once("click", function() {
				if (!confirm("Are you sure to save current playlist?"))
					return false;

				var data = [];
				$.each($("#plsortable li a"), function(key, val) {
					data.push({FILE_NAME: $(val).attr('trackname'), FILE_TYPE: $(val).attr('FILE_TYPE'), id3tag: [$.timeToSeconds($(val).find("span.ui-li-count").text())]});
				})
				var newobj = {};
				newobj[$("#PL_ITEMS").val()] = data;
				//console.log(newobj);
				$.reqSetData({PL_ITEM_ELEM: newobj}, true)
					.done(function() {
						$("#plsave").buttonMarkup({theme: 'a'});
					});

			})

			$("#plclear").once("click", function() {
				if (!confirm("Are you sure to clear current playlist?"))
					return false;

				$("#plsortable li").remove();
				$("#plsave").buttonMarkup({theme: 'c'});
			});

			$("#plremovetrack").once("click", function() {
				if (!confirm("Are you sure to remove selected track?"))
					return false;

				$("#plsortable .ui-btn-active").remove();

				$("#plsave").buttonMarkup({theme: 'c'});
			});

			$("#pldelete").once("click", function() {
				if (!confirm("Are you sure to delete current playlist?"))
					return false;
				var reqdata = {FILE_NAME: $("#PL_ITEMS").val(), FILE_TYPE: 'playlist', TYPE: 'deletefile'};
				$.reqData("cmd", reqdata)
					.done(function() {

						$("#PL_ITEMS").find("option:selected").remove();
						$("#PL_ITEMS").selectmenu("refresh");
						$("#PL_ITEMS").trigger("change");

						if ($("#PL_ITEMS option").length==0) {
							$.plButtonsEnable(false);
						}
						$.toast("Done");
						$("#plsave").buttonMarkup({theme: 'a'});
					});
			})

			$("#plnew, #plclone").once("click", function() {
				$("#plname").val("");
				switch ($(this).attr('cmd')) {
					case "clone":
						$("#createnewplPopup .popH").text("Create Clone Playlist");
						$("#plname").val($("#PL_ITEMS").val().slice(0,$("#PL_ITEMS").val().length-4)+"_cl"+Math.floor((Math.random() * 10000) + 1));
						$("#createnewplPopup").find("#btSetNewPl").attr("cmd", "clone");
						break;
					case "new":
						$("#createnewplPopup .popH").text("Create New Playlist");
						$("#createnewplPopup").find("#btSetNewPl").attr("cmd", "new");
						$("#plname").text("");
						break;

				}
				$("#createnewplPopup").popup("open");
			});

			$("#plname").on('keypress', function(e) {
				var charCode = (typeof e.which == "number") ? e.which : e.keyCode
				if (charCode == 13) {
					$("#btSetNewPl").trigger("click");
					return true;
				}

				if (charCode==8) {
					// backspace
					return true;
				}

				if ((e.keyCode==46) || (e.keyCode==37) || (e.keyCode==39)) {
					// del -> <-
					return true;
				}

				if (String.fromCharCode(charCode).match(/[^0-9a-zA-Z_]/g))
					return false;
			});

			$("#btSetNewPl").once("click", function() {
				if ($("#plname").val().length==0)
					return false;

				$("#plname").val($("#plname").val()+".m3u");

				switch ($(this).attr('cmd')) {
					case "new":
						$("#plsortable li").remove();
						break;
				}

				if ($("#plname").val().length > 0) {
					$("#PL_ITEMS").append("<option>" + $("#plname").val() + "</option>");
					$("#PL_ITEMS").val($("#plname").val()).selectmenu("refresh");
					$("#pltitle").text($("#PL_ITEMS").val());

					$.plButtonsEnable(true);

					var data = [];
					$.each($("#plsortable li a"), function(key, val) {
						data.push({FILE_NAME: $(val).attr('trackname'), FILE_TYPE: $(val).attr("FILE_TYPE"), id3tag: [$(val).attr('id3tag_len')]});
					})
					var newobj = {};
					newobj[$("#PL_ITEMS").val()] = data;
					if ($.isEmptyObject(plitemsobj)) {
						plitemsobj = $(newobj).first()
					} else {
						plitemsobj.push(newobj);
					}

					$("#addtrack").removeClass("ui-disabled");
					$("#plclone").removeClass("ui-disabled");
					$("#plsave").removeClass("ui-disabled");
					$("#pldelete").removeClass("ui-disabled");
					var request = $.reqSetData({PL_ITEM_ELEM: newobj}, true)
						.done(function() {
							$("#plsave").buttonMarkup({theme: 'a'});
							if ($("#btSetNewPl").attr("cmd")=="clone") {
								$("#createnewplPopup").once('popupafterclose', function() {
									$.toast("Playlist saved.");
									$("#createnewplPopup").once('popupafterclose', function() {})
								})
							}
							$("#createnewplPopup").popup("close");
						})
						.error(function() {
							if ($("#btSetNewPl").attr("cmd")=="clone") {
								$("#createnewplPopup").once('popupafterclose', function() {
									$.toast("Playlist not saved.");
									$("#createnewplPopup").once('popupafterclose', function() {})
								})
							}
							$("#createnewplPopup").popup("close");
						})
				}


			});

			if (lastSelectedPl != -1) {
				$("#PL_ITEMS").val(lastSelectedPl).selectmenu("refresh");
				$("#PL_ITEMS").trigger("change");
			}
		})
}

$.initClickTrackItem = function() {
	$(".clTrackitem").once("click", $.clTrackitemInit);
	$("#SHOWTITLE").trigger("change");
}

$.initTracksTab = function() {

	if (!$.isEmptyObject(musicitemsobj) && ($("#MUSIC_ITEMS li").length==0)) {
		$.fillupTrackList(musicitemsobj, $("#MUSIC_ITEMS"));
	}
	if (!$.isEmptyObject(spotitemsobj) && ($("#SPOT_ITEMS li").length==0)) {
		$.fillupTrackList(spotitemsobj, $("#SPOT_ITEMS"));
	}
	if (!$.isEmptyObject(sensoritemsobj) && ($("#SENSOR_ITEMS li").length==0)) {
		$.fillupTrackList(sensoritemsobj, $("#SENSOR_ITEMS"));
	}

	$("#tabs a.tabnav").bind("click", function() {

		$(".clTrackitem").removeClass("ui-btn-active");
		$("#divTrackCmds").addClass("ui-disabled");
		if ($("#PL_ITEMS option").length==0) {
			$("#btPause").parent("div").addClass("ui-disabled");
			$("#btStop").parent("div").addClass("ui-disabled");
		}
		if ($(this).hasClass("musictab")) {
			if ($.isEmptyObject(musicitemsobj)) {
				$.reqData("get", {MUSIC_ITEMS: ''})
					.done(function(data) {
						$.initClickTrackItem(data);
					});
			} else if ($("#MUSIC_ITEMS li").length==0) {
				$.fillupTrackList(musicitemsobj, $("#MUSIC_ITEMS"));
			}
		} else if ($(this).hasClass("spottab")) {

			if ($.isEmptyObject(spotitemsobj)) {
				$.reqData("get", {SPOT_ITEMS: ''})
					.done(function(data) {
						$.initClickTrackItem(data);
					});
			} else if ($("#SPOT_ITEMS li").length==0) {
				$.fillupTrackList(spotitemsobj, $("#SPOT_ITEMS"));
			}
		} else if ($(this).hasClass("sensortab")) {

			if ($.isEmptyObject(sensoritemsobj)) {
				$.reqData("get", {SENSOR_ITEMS: ''})
					.done(function(data) {
						$.initClickTrackItem(data);
					});
			} else if ($("#SENSOR_ITEMS li").length==0) {
				$.fillupTrackList(sensoritemsobj, $("#SENSOR_ITEMS"));
			}
		}

	});

	$.initClickTrackItem();

	$(".tabnav.musictab").trigger("click");

}

$.playerInit = function() {

	$.globalInit();

	// sort function callback
	function sort_li(a, b) {
		switch (+($("#TRACKSORT").val())) {
			case 0:
				return ($(b).find("a").attr("trackname")) < ($(a).find("a").attr("trackname")) ? 1 : -1;    
			case 1:
				return ($(b).find("a").attr("id3tag_title")) < ($(a).find("a").attr("id3tag_title")) ? 1 : -1;    
			case 2:
				return ($(b).find("a").attr("id3tag_author")) < ($(a).find("a").attr("id3tag_author")) ? 1 : -1;    
			case 3:
				return ($(b).find("a").attr("id3tag_album")) < ($(a).find("a").attr("id3tag_album")) ? 1 : -1;    
			case 4:
				return ($(b).find("a").attr("id3tag_genre")) < ($(a).find("a").attr("id3tag_genre")) ? 1 : -1;    
		}
		
	}	
	
	$("#TRACKSORT").change(function() {
	
		var val = +$(this).val();
		$(".clTrackitem, .clUsbfileItem").each(function() {
			if (val>0) {
				if ($(this).attr("id3tag_title") == "undefined")
					return true;
				var myspan=$(this).find("span");
				$(this).html($(this).attr("id3tag_title") + " - "  + $(this).attr("id3tag_author") + " - "  + $(this).attr("id3tag_album") + " - " + mp3Genres[$(this).attr("id3tag_genre")]);
				$(this).append(myspan);
			} else {
				var myspan=$(this).find("span");
				$(this).html($(this).attr("trackname"));
				$(this).append(myspan);
			}
		})	
	
	
		$("#MUSIC_ITEMS li").sort(sort_li) // sort elements
						  .appendTo('#MUSIC_ITEMS'); // append again to the list	

		$("#SPOT_ITEMS li").sort(sort_li) // sort elements
						  .appendTo('#SPOT_ITEMS'); // append again to the list	

		$("#SENSOR_ITEMS li").sort(sort_li) // sort elements
						  .appendTo('#SENSOR_ITEMS'); // append again to the list	
						  
						  
	})		
	
	$.reqGetData(undefined, "silent")
		.always(function() {

			if (!$.isStatusVarsOK()) {
				return false;
			}

			$.initTracksTab();

			$("#divmusic").trigger("click");
		})
}

$.relsensInit = function() {

	$.globalInit();

	$.reqGetData()
		.done(function() {

			if ($("#DEVFN").val()==4) {
				// Scheduler MODE
				$("#divRelData").addClass("ui-disabled");
			}

			if ($("#RELM").val() == '1') {
				// Player relay mode
				$("#divRELADV").show();
			}

			$("#RELM").change(function() {
				if ($(this).val() == '1') {
					$("#divRELADV").show();
				} else {
					$("#divRELADV").hide();
				}
			});

			var initialVal = $("#RELSET").val();
			$("#RELSET").on("slidestop", function() {
				$("#RELSET").val(initialVal).slider('refresh');
				$("#RELSET").trigger("change");
			});

			$("#RELSET").on("change", function() {
				$.reqSetData(this, "silent");
			});


			$("#PL_ITEMS option").remove();
			$("#PL_ITEMS").append('<option>NONE</option>');

			$.reqData("get", {PL_ITEMS: ''})
				.done(function() {
					$.reqData("get", {SENSORPLN: ""}).done(function(data) {
							if (!$.isPlainObject(data)) data = $.parseJSON(data);
							$("#PL_ITEMS").val(data.res.get.SENSORPLN).selectmenu('refresh');
						});
				});

			$("#PL_ITEMS").change(function() {
				$.reqData("set", {SENSORPLN: $(this).val()});
			})

			$("#bttSendSENSDVID").once("click", function() {
				$.reqSetData($("#SENSDVID"));
			})
		})

};

$.comrs485Init = function() {

	$.globalInit();

	/* $("#COMRS485").change(function() {
		if ($(this).val()==0) {
			$("#com_data").slideUp();
		} else {
			$("#com_data").slideDown();
		}
	}); */

	if ($("#COMRS485").val()==0) {
		$("#com_data").slideUp();
	} else {
		$("#com_data").slideDown();
	}
};



$.loadSchedData = function(myelements) {

	paramData = {};
	paramData.schedules = [];
	paramData.events = [];

	$.each(myelements, function(idx, elem) {
		switch (elem.eltype) {
			case 0:
				// playlist
				var calcend;

				for (var i=0; i<paramData.schedules.length; i++) {
					// search if is inside a playlist
					if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(elem.st))
						&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(elem.st))) {
						// aggiorna lunghezza playlist
						paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))+parseInt(elem.len));
						break;
					}
				}
				calcend = $.secondsToTime($.timeToSeconds(elem.st) + parseInt(elem.len));
				paramData.schedules.push({eltype: elem.eltype, start: elem.st, end: calcend, length: elem.len, title: elem.tit});


				break;
			case 1:
				// spot
				var calcend;
				for (var i=0; i<paramData.schedules.length; i++) {
					// search if is inside a playlist
					if ( ($.timeToSeconds(paramData.schedules[i].start)<=$.timeToSeconds(elem.st))
						&& ($.timeToSeconds(paramData.schedules[i].end)>=$.timeToSeconds(elem.st))) {
						// aggiorna lunghezza playlist
						paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))+parseInt(elem.len));
						break;
					}
				}
				calcend = $.secondsToTime($.timeToSeconds(elem.st) + parseInt(elem.len));
				paramData.schedules.push({eltype: elem.eltype, start: elem.st, end: calcend, length: elem.len, title: elem.tit, customclass: "spot"});

				break;
			case 2:
				// event
				paramData.events.push({eltype: elem.eltype, evstart: elem.st, title: elem.tit, type: elem.type, value: elem.val, idx: elem.idx });
		}
	});

	$('#mysched').graspSchedule();
}



$.schedDataInit = function(scheddata) {
	paramData = {};
	paramData.schedules = [];
	paramData.events = [];

	$("#schtype").val(scheddata.options.schtype).selectmenu("refresh");
	$("input[id^=wday]").prop("checked", false).checkboxradio("refresh");
	$("#days").val("");

	if (typeof scheddata.options.en === 'undefined') {
		scheddata.options.en = 1;
	}

	$("#schedenabled").val(scheddata.options.en).slider("refresh");

	switch (+scheddata.options.schtype) {
		case 0:
			// every day
			$("#checkweekdays").hide();
			$("#specificdays").hide();
			break;
		case 1:
			// weekly
			$.each(scheddata.options.days.split(","), function(idx, val) {
				if (val=="")
					return;
				$("#wday"+val).prop("checked", true).checkboxradio("refresh");
			});
			$("#checkweekdays").show();
			$("#specificdays").hide();
			break;
		case 2:
			// montly
			$("#checkweekdays").hide();
			$("#days").val(scheddata.options.days);
			$("#days").attr("placeholder", "ex. 1,3,8,24, 5-20 or 1,12,20-31");
			$("#specificdays").show();
			break;
		case 3:
			// specific days
			$("#checkweekdays").hide();
			$("#days").val(scheddata.options.days);
			$("#days").attr("placeholder", "ex. 25-02-2018 or 25-02-2018,26-02-2018");
			$("#specificdays").show();

			break;
	}

	if (typeof schedulerData[$("#schedlist").val()].elements !== "undefined") {
		schedulerData[$("#schedlist").val()].elements = [];
	}

	var schedItemElem = $("#schedlist").val();
	$.reqData("get", {SCHEDULER_ITEM_ELEM: schedItemElem})
		.done(function(jdata) {
			var mydata = $.parseJSON(jdata).res.get.SCHEDULER_ITEM_ELEM[schedItemElem].elements;

			//schedulerData[schedItemElem].elements = [];
			schedulerData[schedItemElem].elements = mydata;

			$.loadSchedData(schedulerData[schedItemElem].elements);
		});

}

$.clearSched = function() {

	$('#mysched').empty();
	$('#mysched').css("height", 0);
	paramData = {};
	paramData.schedules = [];
	paramData.events = [];
	$('#mysched').graspSchedule();

}

$.schedulerInit = function() {
	$.globalInit();

	$("#PL_ITEMS").empty();
	plitemsobj = {}

	$("#shiftupEv").once("click", function() {
		if ($("#shiftsecEv").val()<=0)
			return false;

		var id = parseInt($("#btSetEvent").attr("elemid"));
		for (var i=id; i<paramData.events.length; i++) {

			if ($.secondsToTime(parseInt($.timeToSeconds(paramData.events[i].evstart) - $("#shiftsecEv").val())) <= 0) {
				// raggiunte le 0
				break;
			}

			paramData.events[i].evstart = $.secondsToTime(parseInt($.timeToSeconds(paramData.events[i].evstart) - parseInt($("#shiftsecEv").val())))

		}
		$('#mysched').graspSchedule();
		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");

	})

	$("#shiftdownEv").once("click", function() {
		if ($("#shiftsecEv").val()<=0)
			return false;

		var id = parseInt($("#btSetEvent").attr("elemid"));
		for (var i=id; i<paramData.events.length; i++) {

			if ($.secondsToTime(parseInt($.timeToSeconds(paramData.events[i].evstart) + $("#shiftsecEv").val())) >= 86400) {
				// raggiunte le 24 ore
				break;
			}

			paramData.events[i].evstart = $.secondsToTime(parseInt($.timeToSeconds(paramData.events[i].evstart) + parseInt($("#shiftsecEv").val())))

		}
		$('#mysched').graspSchedule();
		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");

	})

	$("#shiftup").once("click", function() {
		if ($("#shiftsec").val()<=0)
			return false;

		var myobj = {}
		var schedData = schedulerData[$("#schedlist").val()];

		var id = parseInt($("#btSetPl").attr("elemid"));

		for (var i=id; i<schedData.elements.length; i++) {
			if (($.timeToSeconds(schedData.elements[i].st) - parseInt($("#shiftsec").val())) <= 0) {
				// raggiunte le 0
				break;
			}

			schedData.elements[i].st = $.secondsToTime($.timeToSeconds(schedData.elements[i].st) - parseInt($("#shiftsec").val()))

		}

		paramData = {};
		paramData.schedules = [];
		paramData.events = [];
		$.loadSchedData(schedulerData[$("#schedlist").val()].elements);

		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");

	})

	$("#shiftdown").once("click", function() {
		if ($("#shiftsec").val()<=0)
			return false;

		var myobj = {}
		var schedData = schedulerData[$("#schedlist").val()];

		var id = parseInt($("#btSetPl").attr("elemid"));

		for (var i=id; i<schedData.elements.length; i++) {
			if (($.timeToSeconds(schedData.elements[i].st) + parseInt($("#shiftsec").val())) >= 86400) {
				// raggiunte le 24 ore
				break;
			}

			schedData.elements[i].st = $.secondsToTime($.timeToSeconds(schedData.elements[i].st) + parseInt($("#shiftsec").val()))

		}

		paramData = {};
		paramData.schedules = [];
		paramData.events = [];
		$.loadSchedData(schedulerData[$("#schedlist").val()].elements);

		$("#btSaveScheduler").buttonMarkup({theme: 'c'});
		$(".mypopup").popup("close");

	})

	function checkIfOverlap(obj, id) {
		var isInside=false;
		var schedData = schedulerData[$("#schedlist").val()];
		if (id===undefined) id = -1;

		if (typeof schedData === "undefined")
			return false;

		if (typeof schedData.elements === "undefined")
			return false;

		var count = 0;

		for (var i=0; i<schedData.elements.length; i++) {
			if (schedData.elements[i].eltype == 2) continue; // forget about events
			if (count==id) {
				count++;
				continue; // forget about itself
			}
			if ($.timeToSeconds(schedData.elements[i].st)>$.timeToSeconds(obj.start)) {
				// check if overlap because length
				if (($.timeToSeconds(obj.start) + parseInt(obj.length)) >= $.timeToSeconds(schedData.elements[i].st)) {
					if (obj.eltype==0) {
						// is a Playlist. It cannot overlap other PL
						if (schedData.elements[i].eltype==0) {
							// it's overlapping another PL!
							return true;
						}

					} else {
						// this is a SPOT. It cannot overlap other SPOTS
						if (schedData.elements[i].eltype==1) {
							// it's overlapping another spot
							return true;
						}
					}

				}
			} else if (($.timeToSeconds(schedData.elements[i].st)+parseInt(schedData.elements[i].len))>$.timeToSeconds(obj.start)) {
				// is inside

				if (obj.eltype==0) {
					// is a Playlist. It cannot overlap other PL
					// it's overlapping another PL or existing spot!
					return true;
				} else {
					// this is a SPOT. It cannot overlap other SPOTS
					if (schedData.elements[i].eltype==1) {
						// it's overlapping another spot
						return true;
					}
				}
			}
			count++;
		}

		return false;
	}

	$.reqGetData()
		.done(function() {
			clearTimeout(clock);
			sec = parseInt($("#RTCCHH").val())*3600 + parseInt($("#RTCCMM").val())*60 + parseInt($("#RTCCSS").val());
			$("#myclock").text($.secondsToTime(sec));
			clock = setTimeout(updateClock, 1000);
			$("#mydate").text(", " + wdays[$("#RTCCWD").val()] + " " + $.strPad($("#RTCCD").val(),2) + "/" + $.strPad($("#RTCCM").val(),2) + "/" + $.strPad($("#RTCCY").val(),2));
		})
		.always(function() {

			if (!$.isStatusVarsOK()) {
				return false;
			}

			$.schedButtonsEnable = function(bEnable) {
				if (bEnable) {
					$("#btDeleteScheduler").removeClass('ui-disabled');
					$(".schedCommands").removeClass('ui-disabled');
					$("#btSaveScheduler").removeClass('ui-disabled');
					$("#btCloneScheduler").removeClass('ui-disabled');
				} else {
					$("#btDeleteScheduler").addClass('ui-disabled');
					$(".schedCommands").addClass('ui-disabled');
					$("#btSaveScheduler").addClass('ui-disabled');
					$("#btCloneScheduler").addClass('ui-disabled');
				}
			}

			$.schedButtonsEnable(false);
			// ----------------
			// DIOC - events only!
			$("#eventPopup").find("#idx").empty().selectmenu("refresh");
			$.each($.toFlags($("#DIOC").val(), 8), function(key, val) {
				var currentKey = (parseInt(key)+1);
				if ((val==0) && ($("#DOM"+currentKey).val()=="0")) {
					$("#eventPopup").find("#idx").append("<option value=\"" + currentKey + "\">Output " + currentKey + "</option>");
				}
			});
			$("#eventPopup").find("#idx").selectmenu("refresh");
			// ----------------

			$("#schedenabled").change(function() {
				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
			})

			$("#specificdays").change(function() {
				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
			})

			$("#specificdays").change(function() {
				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
			})

			$("input[id^=wday]").change(function() {
				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
			})

			$("#schtype").change(function() {
				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
				switch (+$(this).val()) {
					case 0:
						// every day
						$("#checkweekdays").hide();
						$("#specificdays").hide();
						break;
					case 1:
						// weekly
						$("#checkweekdays").show();
						$("#specificdays").hide();
						break;
					case 2:
						// monthly
						$("#checkweekdays").hide();
						$("#days").val("");
						$("#days").attr("placeholder", "ex. 1,3,8,24, 5-20 or 1,12,20-31");
						$("#specificdays").show();
						break;
					case 3:
						// specific days
						$("#checkweekdays").hide();
						$("#days").val("");
						$("#days").attr("placeholder", "ex. 25-02-2018 or 25-02-2018,26-02-2018");
						$("#specificdays").show();
						break;
				}

			})

			$.initDrawSched = function() {

				$.each(schedulerData, function(index, scheddata) {

					$("#schedlist").append("<option>" + index + "</option>");

				});
				$("#schedlist").selectmenu("refresh");

				if ($("#schedlist option").length>0) {
					$("#divSchedEn").show();
					$.schedButtonsEnable(true);
					$.schedDataInit(schedulerData[$("#schedlist").val()]);
				} else {
					$("#divSchedEn").hide();
				}

			}

			if ($.isEmptyObject(schedulerData)) {
				$.reqData("get", {SCHEDULER_ITEMS: ''})
					.done(function(data) {
						data = $.parseJSON(data).res.get.SCHEDULER_ITEMS;
						schedulerData = data;
						$.initDrawSched();
					})
			} else {
				$.initDrawSched();
			}

			$("#btNewScheduler, #btCloneScheduler").once("click", function() {
				$("#schedname").val("").textinput("refresh");
				switch ($(this).attr('cmd')) {
					case "clone":
						$("#createnewschedPopup .popH").text("Create Clone Scheduler");
						$("#createnewschedPopup").find("#btSetNewSched").attr("cmd", "clone");
						$("#schedname").val($("#schedlist").val() + "_cl" + Math.floor((Math.random() * 10000) + 1));
						break;
					case "new":
						$("#createnewschedPopup .popH").text("Create New Scheduler");
						$("#createnewschedPopup").find("#btSetNewSched").attr("cmd", "new");
						$("#days").val("").textinput("refresh");
						$("#schedname").val("");
						break;

				}
				$("#createnewschedPopup").popup("open");
			});

			$("#schedname").on('keypress', function(e) {
				var charCode = (typeof e.which == "number") ? e.which : e.keyCode
				if (charCode == 13) {
					$("#btSetNewSched").trigger("click");
					return true;
				}

				if (charCode==8) {
					// backspace
					return true;
				}

				if ((e.keyCode==46) || (e.keyCode==37) || (e.keyCode==39)) {
					// del -> <-
					return true;
				}

				if (String.fromCharCode(charCode).match(/[^0-9a-zA-Z_]/g))
					return false;

			});

			$("#days").on('keypress', function(e) {
				if (String.fromCharCode(e.keyCode).match(/[^0-9,-]/g))
					return false;
			})

			$("#btSetNewSched").once("click", function() {
				switch ($(this).attr('cmd')) {
					case "clone":
						schedulerData[$("#schedname").val()] = schedulerData[$("#schedlist").val()];
						break;
					case "new":
						schedulerData[$("#schedname").val()] = {options: {schtype: 0, days: ""}, elements: []};
						$.clearSched();
						$("#schtype").val(0).selectmenu("refresh");
						$("#schtype").trigger("change");

						break;

				}

				if ($("#schedname").val().length > 0) {
					$("#schedenabled").val(1).slider("refresh");
					$("#divSchedEn").show();
					$("#schedlist").append("<option>" + $("#schedname").val() + "</option>");
					$("#schedlist").val($("#schedname").val()).selectmenu("refresh");

					$.schedButtonsEnable(true);

					$("#btSaveScheduler").buttonMarkup({theme: 'c'});

					$.saveScheduler();

					$("#createnewschedPopup").popup("close");
				}


			});

			$.saveScheduler = function() {
				var data = {options: {}, elements: []};
				var mydays = $.trim($("#days").val());

				if (schedulerData=="") {
					schedulerData = {}
				}

				switch (parseInt($("#schtype").val())) {
					case 1: // weekly
						// convert weekdays to "days"
						mydays = ",";
						$("[id^=wday]:checked").each(function() {
							mydays += $(this).attr("id").slice(-1) + ",";
						});
						break;
					case 2: // montly
						var isValid = true;
						if (mydays[0]!=",") {
							mydays = "," + mydays;
						}
						if (mydays[mydays.length-1]!=",") {
							mydays += ",";
						}
						$.each(mydays.split(","), function(key, val) {
							if (val.indexOf("-")>0) {
								// is interval
								range=val.split("-");
								if (range.length!=2) return false;
								range[0] = parseInt(range[0]);
								range[1] = parseInt(range[1]);
								if ((range[0]==0) || (range[1]==0)) return false;
								if ((range[0]>31) || (range[1]>31) || (range[0]>range[1])) {
									isValid = false;
									return false;
								}
							}
							else if (parseInt(val)>31) {
								isValid = false;
								return false;
							}
						})
						if (!isValid) {
							alert("Invalid string formatting in field \"days\".\n\nExamples in \"monthly\": 1,3,8,24, 5-20 or 1,12,20-31\n\nExamples in \"Specific\": 25-02-2018 or 25-02-2018,26-02-2018\n\nPlease correct it and retry");
							return false;
						}
						break;
					case 3: // specific
						var isValid = true;
						if (mydays[0]!=",") {
							mydays = "," + mydays;
						}
						if (mydays[mydays.length-1]!=",") {
							mydays += ",";
						}
						$.each(mydays.split(","), function(key, val) {
							if (val.indexOf("-")>0) {
								// is date
								range=val.split("-");
								if (range.length!=3) return false;

								range[0] = parseInt(range[0]);
								range[1] = parseInt(range[1]);
								range[2] = parseInt(range[2]);
								if ((range[0]==0) || (range[1]==0) || (range[2]==0)) return false;
								if ((range[0]>31) || (range[1]>12) || (range[2]<+(new Date()).getFullYear()) || (range[2]>(+(new Date()).getFullYear()+30))) {
									isValid = false;
									return false;
								}
								if (range[1]==2) {
									if (range[0]>28) return false;
								}
								if ((range[1]==11) || (range[1]==4) || (range[1]==6) || (range[1]==9)) {
									if (range[0]>30) return false;
								}

							}
							else if (parseInt(val)>31) {
								isValid = false;
								return false;
							}
						})
						if (!isValid) {
						alert("Invalid string formatting in field \"days\".\n\nExamples in \"monthly\": 1,3,8,24, 5-20 or 1,12,20-31\n\nExamples in \"Specific\": 25-02-2018 or 25-02-2018,26-02-2018\n\nPlease correct it and retry");
							return false;
						}

						break;
				}

				data.options = {schtype: +$("#schtype").val(), days: mydays, en: parseInt($("#schedenabled").val())}
				$.each(paramData.schedules, function(id, elem) {
					data.elements.push({eltype: elem.eltype, st: elem.start, len: parseInt(elem.length), tit: elem.title});
				});
				$.each(paramData.events, function(id, elem) {
					// events only!
					if (typeof elem.idx == "undefined")
						elem.idx = 0;
					data.elements.push({eltype: elem.eltype, st: elem.evstart, type: elem.type, val: elem.value, tit: elem.title, idx: elem.idx});
				});

				data.elements.sort(
						function (a, b) {
							var a = $.timeToSeconds(a.st);
							var b = $.timeToSeconds(b.st);
							if (a > b)
								return 1;
							if (a < b)
								return -1;
							return 0;
						}
				);
				var newobj = {};
				newobj[$("#schedlist").val()] = data;
				var dataSc = {SCHEDULER_ITEMS: newobj};

				$.reqSetData(dataSc, true)
					.done(function() {
						schedulerData[$("#schedlist").val()] = data;
						$("#btSaveScheduler").buttonMarkup({theme: 'a'});
					});
			}

			$("#btSaveScheduler").once("click", function() {
				if (!confirm("Save current Scheduler?")) {
					$("#btSaveScheduler").removeClass("ui-btn-c")
					return false;
				}
				$.saveScheduler();
			})

			$("#btDeleteScheduler").once("click", function() {
				if (!confirm("Delete current Scheduler '" + $("#schedlist").find("option:selected").val() + "'?"))
					return false;

				var reqdata = {FILE_NAME: $("#schedlist").find("option:selected").val(), FILE_TYPE: 'scheduler', TYPE: 'deletefile'};
				$.reqData("cmd", reqdata)
					.done(function() {
						var key = $("#schedlist").val();
						delete schedulerData[key];

						$("#schedlist").find("option:selected").remove();
						$("#schedlist").selectmenu("refresh");
						$("#schedlist").trigger("change");

						if ($("#schedlist option").length==0) {

							$.schedButtonsEnable(false);
							$("#divSchedEn").hide();
						}
						$.toast("Done");
					});

			})

			$("#schedlist").once("focus", function() {
				if ($("#btSaveScheduler").hasClass("ui-btn-c")) {
					$("#btSaveScheduler").trigger("click");
				}
			});

			$("#schedlist").once("change", function() {

				$.clearSched();

				if ($("#schedlist option").length==0) {
					$("#divSchedEn").hide();
					return true;
				}

				lastSelectedSched  = $("#schedlist").val();

				$.schedDataInit(schedulerData[$("#schedlist").val()]);
				$("#schedlist").selectmenu("refresh");

				$('#mysched').graspSchedule();
				$("#btSaveScheduler").buttonMarkup({theme: 'a'});
			});

			$('#mysched').height($('#maincontent').outerHeight());

			$("#eventPopup").on('keypress', function(e) {
				if (e.keyCode == 13) {
					$("#eventPopup").find("#setevent").trigger("click");
				}
			});

			$("#btSetEvent").once('click', function(e) {
				var obj = {};

				obj.title = $("#eventPopup").find("#title").val();
				//0 = "relay", 1 = "output line", 2 = "sensor enable", 3 = "line in enable"
				switch ($("#eventPopup").find('[name="outtype"]:checked').val()) {
					case "relay":
						obj.type = 0;
						break;
					case "outline":
						obj.type = 1;
						obj.idx = parseInt($("#eventPopup").find("#idx").val());
						break;
					case "linein":
						obj.type = 3;
						break;
				}

				obj.value = parseInt($("#eventPopup").find('#outvalue').val());
				obj.evstart = $.strPad($("#eventPopup").find("#hours").val(), 2)+":"+$.strPad($("#eventPopup").find("#minutes").val(), 2)+":"+$.strPad($("#eventPopup").find("#seconds").val(),2);
				obj.eltype = 2;
				if (typeof $(this).attr("elemid") === "undefined") {
					// ---------------------------
					//  Add event
					// ---------------------------
					$.handleSchedElem(obj, "insert");

				} else {
					// ---------------------------
					//  Edit event
					// ---------------------------
					obj.id = parseInt($(this).attr("elemid"));
					$.handleSchedElem(obj, "edit");
				}

				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
				$(".mypopup").popup("close");
			});

			$("#eventPopup [name=outtype]").once('change', function() {
				if ($(this).val() == "outline")
					$("#divOutLineIdx").removeClass('ui-disabled');
				else
					$("#divOutLineIdx").addClass('ui-disabled');

				if ($(this).val() == "linein")
					$("#lblOutvalue").text("LineIN Enable");
				else
					$("#lblOutvalue").text("Output value");

			});

			$("#btAddEvent").once('click', function() {

				// set event popup to default
				$(".editonly").hide();
				$("#eventPopup .popH").text("Add Event");
				$("#eventPopup").find("#title").val("").attr("placeholder", "add event title here..").textinput("refresh");
				$("#eventPopup [name=outtype]").prop("checked",false).checkboxradio("refresh");
				$("#eventPopup").find("#relay").prop("checked",true).checkboxradio("refresh");
				$("#eventPopup").find("#outvalue").val(0).slider("refresh");
				$("#divOutLineIdx").addClass('ui-disabled');

				$("#btSetEvent").removeAttr("elemid"); // set for add

				if ($("#eventPopup").find('[name="outtype"]:checked').length==0) {
					// set "relay" by default
					$("#eventPopup").find("#relay").prop("checked",true);
				}

				$("#eventPopup").popup("open");
			});

			$("#btRemoveEvent").once('click', function() {
				if (!confirm("Are you sure to remove Event?"))
					return false;
				var obj = {};
				obj.eltype = 2;
				obj.id = parseInt($("#btSetEvent").attr("elemid"));
				$.handleSchedElem(obj, "remove");

				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
				$(".mypopup").popup("close");
			});

			$.handleSchedElem = function(obj, cmd) {
				var myobj = {}
				var schedData = schedulerData[$("#schedlist").val()];
				myobj.eltype = obj.eltype;
				if (myobj.eltype==2) {
					// events only
					myobj.st = obj.evstart;
					myobj.val = obj.value;
					myobj.type = obj.type;
					if (typeof obj.idx !== "undefined")
						myobj.idx = obj.idx; // outline event
				} else {
					myobj.st = obj.start;
					myobj.len = obj.length;
				}
				myobj.tit = obj.title;
				switch (cmd) {
					case "insert":
						for (var i=0; i<schedData.elements.length; i++) {
							if ($.timeToSeconds(schedData.elements[i].st)<=$.timeToSeconds(myobj.st)) continue;
							// insert element
							schedData.elements.splice(i, 0, myobj);
							break;
						}
						if (i==schedData.elements.length) {
							schedData.elements.push(myobj);
						}
						break;
					case "edit":

						var count = 0;
						if (obj.eltype==2) {
							// events only
							for (var i=0; i<schedData.elements.length; i++) {
								if (schedData.elements[i].eltype != 2) continue;
								if (count == obj.id) {
									schedData.elements.splice(i, 1);
									break;
								}
								count++;
							}

						} else {
							for (var i=0; i<schedData.elements.length; i++) {
								if (schedData.elements[i].eltype == 2) continue;
								if (count == obj.id) {
									schedData.elements.splice(i, 1);
									delete obj.end;
									break;
								}
								count++;
							}

						}


						$.handleSchedElem(obj, "insert");
						break;
					case "remove":
						var count = 0;
						if (obj.eltype==2) {
							// events only
							for (var i=0; i<schedData.elements.length; i++) {
								if (schedData.elements[i].eltype != 2) continue;
								if (count == obj.id) {
									schedData.elements.splice(i, 1);
									break;
								}
								count++;
							}

						} else {
							for (var i=0; i<schedData.elements.length; i++) {
								if (schedData.elements[i].eltype == 2) continue;
								if (count == obj.id) {
									schedData.elements.splice(i, 1);
									break;
								}
								count++;
							}

						}



						break;
				}

				paramData = {};
				paramData.schedules = [];
				paramData.events = [];
				$.loadSchedData(schedulerData[$("#schedlist").val()].elements);
			}

			$("#btAddPlaylist").once('click', function() {

				$("#PL_ITEMS option").remove();
				if ($.isEmptyObject(plitemsobj)) {
					$.reqData("get", {PL_ITEMS: ''})
						.done(function() {
							$(".showPLLen").trigger("change")
						})
				} else {
					$.fillupTrackList(plitemsobj, $("#PL_ITEMS"));
					$(".showPLLen").trigger("change");
				}

			// set playlist popup to default
				$(".editonly").hide();
				$("#playlistPopup .popH").text("Add Playlist");

				$("#btSetPl").removeAttr("elemid"); // set for add

				newtime = $.timeToSeconds(paramData.lastTime) + 1

				$("#playlistPopup").find("#hours").val($.timeTo("h", newtime));
				$("#playlistPopup").find("#minutes").val($.timeTo("m", newtime));
				$("#playlistPopup").find("#seconds").val($.timeTo("s", newtime));
				$("#playlistPopup").popup("open");
			});

			$("#btSetPl").once('click', function(e) {
				var obj = {};
				obj.title = $("#PL_ITEMS").val();
				obj.idx = $("#PL_ITEMS option:selected").attr("idx"); // set internal index for PL

				obj.start = $.strPad($("#playlistPopup #hours").val(), 2)+":"+$.strPad($("#playlistPopup #minutes").val(), 2)+":"+$.strPad($("#playlistPopup #seconds").val(),2);
				obj.length = parseInt($("#PL_ITEMS option:selected").attr("opt").split(",")[0]);

				obj.end = $.secondsToTime(parseInt($.timeToSeconds(obj.start))+parseInt(obj.length));

				obj.eltype = 0;

				if (typeof $(this).attr("elemid") === "undefined") {
					// ---------------------------
					//  Add playlist
					// ---------------------------
					if (checkIfOverlap(obj)) {
						$(".mypopup").popup("close");
						alert("Invalid operation: Playlist will overlap an existing Playlist or interrupt a Spot!");
						return false;
					}
					$.handleSchedElem(obj, "insert");

				} else {
					// ---------------------------
					//  Edit playlist
					// ---------------------------
					var id = parseInt($(this).attr("elemid"));
					obj.id = id;
					if (checkIfOverlap(obj, id)) {
						$(".mypopup").popup("close");
						alert("Invalid operation: Playlist will overlap an existing Playlist or interrupt a Spot!");
						return false;
					}
					$.handleSchedElem(obj, "edit");

				}

				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
				$(".mypopup").popup("close");

			});

			$("#btRemovePl").once('click', function() {
				if (!confirm("Are you sure to remove Playlist?"))
					return false;

				var id = parseInt($("#btSetPl").attr("elemid"));
				var obj = paramData.schedules[id];
				obj.id = id;

				$.handleSchedElem(obj, "remove");

				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
				$(".mypopup").popup("close");
			});

			$(".showPLLen").change(function() {
				$("#spanPLLen").text($.secondsToTime($(".showPLLen option:selected").attr("opt").split(",")[0], 1))
			})

			$("#SPOTREP").once('change', function() {
				if ($(this).val()==1)
					$(".isforrep").removeClass("ui-disabled");
				else
					$(".isforrep").addClass("ui-disabled");
			})
			$("#btAddSpot").once('click', function() {
				if ($.isEmptyObject(spotitemsobj)) {
					$.reqData("get", {SPOT_ITEMS: ''})
						.done(function() {
							$(".showSpotLen").trigger("change")
						})
				} else {
					$(".showSpotLen").trigger("change")
				}
				// set spot popup to default
				$("#btRemoveSpot").hide();
				$("#spotPopup .popH").text("Add Spot");

				$("#btSetSpot").removeAttr("elemid"); // set for add
				$("#divSpotRepeat").removeClass("ui-disabled");
				$("#SPOTREP").val(0).trigger("change").slider("refresh");



				$("#spotPopup").popup("open");
			});

			$.recursiveForwardShift = function(end_seconds, i, length) {
				i++;
				for (; i<paramData.schedules.length; i++) {
					if (+$.timeToSeconds(paramData.schedules[i].start)<+end_seconds) {
						paramData.schedules[i].start = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].start))+parseInt(length));
						paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))+parseInt(length));
						$.recursiveForwardShift($.timeToSeconds(paramData.schedules[i].end), i, length);
					}
				}
			}

			$.recursiveBackwardShift = function(end_seconds, i, length) {
				i++;
				for (; i<paramData.schedules.length; i++) {
					if (+$.timeToSeconds(paramData.schedules[i].start)>(parseInt(end_seconds)+parseInt(length))) {
						paramData.schedules[i].start = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].start))-parseInt(length));
						paramData.schedules[i].end = $.secondsToTime(parseInt($.timeToSeconds(paramData.schedules[i].end))-parseInt(length));
						$.recursiveBackwardShift($.timeToSeconds(paramData.schedules[i].end), i, length);
					}
				}
			}


			$("#btSetSpot").once('click', function(e) {
				var obj = {};
				obj.customclass = "spot";
				obj.title = $("#SPOT_ITEMS").val();
				obj.idx = $("#SPOT_ITEMS option:selected").attr("idx");

				obj.start = $.strPad($("#spotPopup #hours").val(), 2)+":"+$.strPad($("#spotPopup #minutes").val(), 2)+":"+$.strPad($("#spotPopup #seconds").val(),2);
				obj.length = $("#SPOT_ITEMS option:selected").attr("opt").split(",")[0];

				obj.end = $.secondsToTime(parseInt($.timeToSeconds(obj.start))+parseInt(obj.length));

				obj.eltype = 1;
				var parentid = -1;
				var elemid = parseInt($("#btSetSpot").attr("elemid"));


				if (typeof $(this).attr("elemid") === "undefined") {
					// ---------------------------
					//  Add spot
					// ---------------------------

					if (+$("#SPOTREP").val()==1) {
						// repeat it
						var repsec = +$("#repsec").val();
						var reptimes = +$("#reptimes").val()-1;

						if (checkIfOverlap(obj)) {
							$(".mypopup").popup("close");
							alert("Invalid operation: new Spot will overlap an existing Spot!");
							return false;
						}

						$.handleSchedElem(obj, "insert");
						$("#btSaveScheduler").buttonMarkup({theme: 'c'});

						while(reptimes-->0) {
							obj.start = $.secondsToTime(parseInt($.timeToSeconds(obj.start))+repsec);
							obj.end = $.secondsToTime(parseInt($.timeToSeconds(obj.start))+parseInt(obj.length));
							if (parseInt($.timeToSeconds(obj.end)) > 86400)
								break;

							if (checkIfOverlap(obj)) {
								$(".mypopup").popup("close");
								alert("Invalid operation: new Spot is overlapping an existing Spot or Playlist");
								return false;
							}

							$.handleSchedElem(obj, "insert");
							$("#btSaveScheduler").buttonMarkup({theme: 'c'});

						}
					} else {

						if (checkIfOverlap(obj)) {
							$(".mypopup").popup("close");
							alert("Invalid operation: new Spot will overlap an existing Spot!");
							return false;
						}

						$.handleSchedElem(obj, "insert");
					}
				} else {
					// ---------------------------
					//  Edit spot
					// ---------------------------
					obj.id = elemid;
					if (checkIfOverlap(obj, obj.id)) {
						$(".mypopup").popup("close");
						alert("Invalid operation: new Spot will overlap an existing Spot");
						return false;
					}

					$.handleSchedElem(obj, "edit");

				}

				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
				$(".mypopup").popup("close");
			});

			$("#btRemoveSpot").once('click', function() {
				if (!confirm("Are you sure to remove Spot?"))
					return false;

				var id = parseInt($("#btSetSpot").attr("elemid"));
				var obj = paramData.schedules[id];
				obj.id = id;

				$.handleSchedElem(obj, "remove");

				$("#btSaveScheduler").buttonMarkup({theme: 'c'});
				$(".mypopup").popup("close");
			});

			$(".showSpotLen").change(function() {
				if ($(".showSpotLen option:selected").length == 0) return false;
				$("#spanSpotLen").text($.secondsToTime($(".showSpotLen option:selected").attr("opt").split(",")[0], 1))
			})


			if (lastSelectedSched != -1) {
				$("#schedlist").val(lastSelectedSched).selectmenu("refresh");
				$("#schedlist").trigger("change");
			}
		})

		$('#maincontent').css({overflow: "visible"})
}

$.loginInit = function() {
	$.globalInit();
	$.reqGetData();

	$("#btLogin").once("click", function() {
		$("#LOGINTYPE").val($("#LOGINTYPE2").val())
		$.reqSetData([$("#LOGINTYPE"),$("#LOGINPWD")])
			.done(function() {
				$("#liHome").trigger("click");
			})
	})

	$("#btLogout").once("click", function() {
		$.reqSetData($("#LOGOUT"))
			.done(function() {
				$("#liHome").trigger("click");
			})
	})

	$("#btSetPassword").once("click", function() {

		if ($("#LOGINPWD").val()!=$("#EDITPWD").val()) {
			alert("Passwords don't match. Please correct error.")
			return false;
		}

		if (!confirm("Change password?"))
			return false;

		$("#LOGINTYPE").val($("#LOGINTYPE2").val())
		$.reqSetData([$("#LOGINTYPE"),$("#EDITPWD")])
	})

}

$.uploadInit = function() {
	$.globalInit();
		var myDropzone;
		$("div#mydrop").dropzone({

				url: "jsondata.php",
				autoProcessQueue: false,
				parallelUploads: 1,
				acceptedFiles: "audio/mpeg,audio/mp3,audio/x-mpegurl",
				init: function() {
					var validFiles = true;
					this.on("error", function(file){if (!file.accepted) this.removeFile(file);});
					var submitButton = document.querySelector("#btUploadAll")
					myDropzone = this; // closure

					submitButton.addEventListener("click", function() {
						if (!confirm("Start uploading? NOTE: Do not change page while upload process."))
							return false;
						$(".menulink").each(function() {
							if ($(this).hasClass("ui-disabled"))
								return true;
							$(this).addClass("myhidden")
							$(this).addClass("ui-disabled")
						})

						$.each(myDropzone.files, function(key, file) {
							validFiles = true;
							if ((file.type=="audio/mp3") || (file.name.indexOf(".mp3")>1) || (file.name.indexOf(".MP3")>1) ) {
								if ($("#targetdir").val()=="playlist") {
									myDropzone.removeFile(file);
									alert("Wrong file format for target directory!");
									validFiles = false;
									return false;
								}
							}
							if ((file.type=="x-mpegurl") || (file.name.indexOf(".m3u")>1) || (file.name.indexOf(".M3U")>1)) {
								if ($("#targetdir").val()!="playlist") {
									myDropzone.removeFile(file);
									alert("Wrong file format for target directory!");
									validFiles = false;
									return false;
								}
							}
						})

						if (validFiles) {
							myDropzone.processQueue(); // Tell Dropzone to process all queued files.
						} else {
							// re-enable all menu items
							$(".myhidden").removeClass("ui-disabled")
							$(".myhidden").removeClass("myhidden")
						}


					});

					// You might want to show the submit button only when
					// files are dropped here:
					this.on("addedfile", function() {
						$("#btUploadAll").removeClass("ui-disabled")
					});

					$("#btReset").click(function() {
						myDropzone.removeAllFiles(true);
					})

				},
				success: function(e) {
					$(e.previewElement).append("<div>").append("Done")
					myDropzone.processQueue();
				},
				sending: function(file, xhr, formData) {

					formData.append("targetdir", $("#targetdir").val());
				},
				queuecomplete: function(e) {
					$(".myhidden").removeClass("ui-disabled")
					$(".myhidden").removeClass("myhidden")
					musicitemsobj = {};
					spotitemsobj = {};
					sensoritemsobj = {};
					plitemsobj = {};
					paramData = {};
					paramData.schedules = [];
					paramData.events = [];
				}


			})

}

$.usbcopyInit = function() {
	$.globalInit();

		var timerUSBcheck;
		$("#btAbortCopy").hide();

		$.reqGetData()
			.always(function(data) {
				$.isStatusUSBOk();
			})

		$("#btAbortCopy").once('click', function() {
			if (!confirm("Abort copy?"))
				return false;
			var reqdata = {COPY: "stop"};
			$.reqData("cmd", reqdata)
		})

		$('#btCopy').once('click', function() {

			if ($(".clUsbfileItem.ui-btn-active").length==0)
				return false;
			if ($("#targetdir").val() == "playlist") {
				// verifica non ci siano mp3
				var error=false;
				$(".clUsbfileItem.ui-btn-active").each(function() {
					if ($(this).attr("id3tag_len")!="undefined") {
						alert("ERROR: Copy MP3 tracks into \"playlist\" destination is not allowed!\nPlease deselect MP3s or change destination.")
						error=true;
						return false;
					}
				})
				if (error)
					return false;
			} else {
				// verifica non ci siano m3u
				var error=false;
				$(".clUsbfileItem.ui-btn-active").each(function() {
					if ($(this).attr("id3tag_len")=="undefined") {
						alert("ERROR: Copy M3U Playlists into tracks destination is not allowed!\nPlease deselect M3Us or change destination.")
						error=true;
						return false;
					}
				})
				if (error)
					return false;
			}

			if (!confirm("DESTINATION: " + $("#targetdir").val() + "\nCopy selected files to SD memory?"))
				return false;

			var reqdata = {COPY:{SRCDRIVE: "USB", SRCDIR: "", DESTDRIVE: "SD", "DESTDIR": $("#targetdir").val(), FILES: []}};
			$(".clUsbfileItem.ui-btn-active").each(function() {
				var newobj = {};
				newobj[$(this).attr("trackname")] = $(this).attr("idx");
				reqdata.COPY.FILES.push(newobj);
			})
			$.reqData("cmd", reqdata)
				.done(function() {
					$("#spMsg").text("Copying form USB to SD memory... Please wait.");
					$("#btAbortCopy").show();
					$("#btCopy").addClass("ui-disabled");
					timerUSBcheck = setTimeout(checkUSB, 3000)
				})

		});

		function checkUSB() {
			// do some stuff...
			$.reqGetData($("#USBSTATUS"))
				.done(function() {
					if ((+$("#USBSTATUS").val() == 3) || (+$("#USBSTATUS").attr("value")==3) ||
						(+$("#USBSTATUS").val() == 2) || (+$("#USBSTATUS").attr("value")==2))

						timerUSBcheck = setTimeout(checkUSB, 3000); // repeat myself
					else {
						clearTimeout(timerUSBcheck);
						$("#btCopy").removeClass("ui-disabled");
						$("#btAbortCopy").hide();
						$("#spMsg").text("");

						musicitemsobj = {};
						spotitemsobj = {};
						sensoritemsobj = {};
						plitemsobj = {};
						paramData = {};
						paramData.schedules = [];
						paramData.events = [];

					}
				})
		}

		$("#btSelectAll").once("click", function() {
			$(".clUsbfileItem:visible").addClass("ui-btn-active");
		})

		$("#btDeselectAll").once("click", function() {
			$(".clUsbfileItem:visible").removeClass("ui-btn-active");
		})

}

$.serviceInit = function() {
	$.globalInit();
	$.reqGetData();

	$("#btReboot").once("click", function() {
		if (confirm("Confirm the device reboot?"))
			$.reqData("set", {SWRESET: 1})
	})

	$("#btRebootSdScan").once("click", function() {
		if (confirm("Confirm the device reboot and then the SD scan process?"))
			$.reqData("set", {SWRESDSCAN: 1})
	})

	$("#btBackup").once("click", function() {
		$.reqData("cmd", {TYPE: "backuparams", FILE_TYPE: $("#MEMTYPE").val()})
	})

	$("#btFactoryDefault").once("click", function() {
		if (!confirm("Reset to factory default?"))
			return false;
		$.reqSetData({FACTORYDEF: 0}, true);
	})
}

$.updateInit = function() {
	$.globalInit();
	$.reqGetData();

	$('#btUpload').on('click', function() {

		var form_data_arr = [];

		var err=false;

		$(".upf").each(function() {

			if ($(this).prop('files').length>0) {
				var form_data = new FormData();
				form_data.append('targetdir', "updates");
				form_data.append('file', $(this).prop('files')[0]);
				if (!$(this).prop('files')[0].name.match(/^dASys_F4_[\w\d.]+\.bin$/i)) {
					// Application: "dASys_F4_*.bin" or "dASys_F4_*.BIN" esempio reale: dASys_F4_v3.0.02.bin
					// Bootloader:  "dASys_F4_BL_*.bin" or "dASys_F4_BL*.BIN"  example: dASys_F4_BL_A3.bin
					// In directory "updates"
					err=true;
					return false;
				}
				form_data_arr.push(form_data);
			}

		})

		if (err==true) {
			alert("Invalid firmware name");
			return false;
		}

		function ajaxRequest (form_data_arr) {
			if (form_data_arr.length > 0) {
				$.ajax({
					type: 'post',
					cache: false,
					contentType: false,
					processData: false,
					dataType: 'text',
					url: 'jsondata',
					data: form_data_arr.pop(),
				})
				.done(function (result) {
					ajaxRequest(form_data_arr);
				});
			} else {
				alert('Upload done');
			}
		}


		if (confirm("CAUTION\n\nThe firmware update operation takes about 4-5 minutes.\nDo not turn off the NP10 during the firmware update.\nThe firmware update requires a restart of the device.")) {
			ajaxRequest(form_data_arr);
		}

	});

}