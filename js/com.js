// --------------------------
// CONSTANTS
// --------------------------

var constBoard = false;

// --------------------------
// hash tables
// --------------------------

var tblDEVFN = ["Standard Player", "Playlist Sequence", "Advanced Player", "Advanced Playlist", "Scheduler"];
var tblDIOM = ["Free", "Player", "Binary Code", "Playlist", "Keypad", "Museum mode", "Priority message"];
var tblRELM = ["FREE", "PLAY STATUS", "PLAY STATUS + SENSOR"];

$.parseResp = function(target, value) {
	var targetobj = $(target);
	
	if (target=="#EXTCHAR") {
		switch (+value) {
			case 0:
				break;
			case 1:
				alert("WARNING: One or more audio file names use extended character set. \nPlease rename them..");
				break;				
		}
	}
	
	if (target=="#LOGINTYPE") {
		var redirect = false;
		if ((currentLOGINTYPE!=undefined) && (currentLOGINTYPE != value)) {
			redirect=true;
		}
		currentLOGINTYPE = value;
		switch (value) {
			case "guest":
				$('.adminonly').hide();
				$('.noadmin').show();
				$(".adminonly_dimmed").parent(".ui-select").addClass("ui-disabled");
				$("#mnPlaylistCreator").text("Playlist details");	
				$("#mainpage").css({height: "100%"});
				break;
			case "admin":
				$('.adminonly').show();
				$('.noadmin').hide();
				$(".adminonly_dimmed").parent(".ui-select").removeClass("ui-disabled");
				$("#mnPlaylistCreator").text("Playlist Creator");
				$("#mainpage").css({height: "auto"});
				break;
			case "multi":
				alert("Another user is logged in. Retry later.")
				$('.adminonly').hide();
				$('.noadmin').hide();
				$(".adminonly_dimmed").parent(".ui-select").addClass("ui-disabled");
				$("#mnPlaylistCreator").text("Playlist details");
				$("#mainpage").css({height: "100%"});				
				break;
			case "none":
				$('.adminonly').hide();
				$('.noadmin').show();
				$(".adminonly_dimmed").parent(".ui-select").addClass("ui-disabled");
				$("#mnPlaylistCreator").text("Playlist details");			
				if (last_myhref!="login.html")
					$("#mnLogin").trigger("click");				
				$("#mainpage").css({height: "100%"});
				break;
		}
		targetobj.val(value);
		$("#usertype").text(value);
		if (redirect==true) $("#liHome").trigger("click");	
	}
	else if (target=="#USB_AUDIO_ITEMS") {
		var count=0;
		$.each(value, function(k, obj) {
			
			$.each(obj, function(key, options) {
				if (options.length==0)
					options[1]="playlist";
				
				$(target).append($("<li>").append(
						$('<a class="clUsbfileItem" idx="'+count+'" trackname="'+key+'" id3tag_len="' + options[0] + '" id3tag_genre="' + options[1] + '" id3tag_author="' + options[2] + '" id3tag_title="' + options[3] + '" id3tag_album="' + options[4] +'" href="#">').text(key)			
					)	
				)
				count++;
			})
		})
		
		$(target).listview().listview("refresh");
		$(".clUsbfileItem").once("click", function() {
			if ($(this).hasClass("ui-btn-active"))
				$(this).removeClass("ui-btn-active");
			else
				$(this).addClass("ui-btn-active");
		})
	}
	else if (targetobj.hasClass("tracklist")) {
		
		if (!constBoard) {
			value = $.parseJSON(value);
		}	
		
		if ($.isEmptyObject(window[targetobj.attr("trackdata")])) {
			window[targetobj.attr("trackdata")] = value;
		}
		else {
			$.merge(window[targetobj.attr("trackdata")], value);
		}
		$.fillupTrackList(value, targetobj);
			
	} else if (targetobj.is("select, input")) {
		if (targetobj.is('[factor]')) {
			value = (value*targetobj.attr('factor'));
		}
		if (targetobj.is('[step]')) {
			targetobj.val(value*targetobj.attr('step'));
		} 
		else {		
			targetobj.val(value);
		}
		
		if (targetobj.is('[data-role]')) {
			if (typeof targetobj.attr("range") !== "undefined") targetobj.slider("refresh");								
		}
		
		targetobj.trigger("change", ["load"]);
		
	} else {
		
		if (targetobj.hasClass("icoCheck")) {
			targetobj.attr("value", value);
			if (parseInt(value)>=1) {				
				targetobj.attr("title", "ON");
				targetobj.removeClass("icoCheckOFF");
				targetobj.addClass("icoCheckON");
			} else {
				targetobj.attr("title", "OFF");
				targetobj.removeClass("icoCheckON");
				targetobj.addClass("icoCheckOFF");
			}
		} 
		else if (targetobj.hasClass("formatbinary")) {
			value = $.strPad(parseInt(value , 10).toString(2), 8);
			value = value.split("");
			//value.reverse()
			$.each(value, function (i, el) {
				var idx = 8-i;
				if (parseInt(el))
					targetobj.append("<div class=\"IOcont\"><span class=\"IObit\">" + idx + "</span><span class=\"IO IOin\" title=\"[" + idx + "] IN\">I</span></div>");
				else 
					targetobj.append("<div class=\"IOcont\"><span class=\"IObit\">" + idx + "</span><span class=\"IO IOout\" title=\"[" + idx + "] OUT\">O</span></div>");
			});
									
		}
		else if (targetobj.is("[hashtable]")) {
			targetobj.text(eval(targetobj.attr("hashtable"))[value]);
		}
		else {
			if (targetobj.is('[factor]')) {
				value = (value*targetobj.attr('factor'));
			}			
			if (targetobj.is('[step]')) {
				targetobj.text(value*targetobj.attr('step'));
			} else {
				targetobj.text(value);
			}			
		}
	}	
	
}

$.getReqFields = function(targetobj) {
	var reqdata = {};
	if (typeof targetobj != 'undefined') {
		if (typeof $(targetobj).attr('id') != 'undefined') {
			reqdata[$(targetobj).attr('id')] = '';		
		}
	} else {
		$(".ui-field-contain > .isfield, .isfield, input.isfield, select.isfield").each(function() {
			if (typeof $(this).attr('id') != 'undefined')
				reqdata[$(this).attr('id')] = '';
		});
	}
	return reqdata;
}

$.setReqFields = function(targetobj) {
	var reqdata = {};
	
	function processobj(targetobj) {
		if ($(targetobj).hasClass("readonlyfield"))
			return false;
		
		if (typeof targetobj != 'undefined') {
			if ($.isPlainObject(targetobj))
				return targetobj;
			
			if (($(targetobj).length==1) && (typeof $(targetobj).attr('id') != 'undefined')) {
				
				if ($(targetobj).is('[step]')) {
					reqdata[$(targetobj).attr('id')] = $(targetobj).val()/$(targetobj).attr('step');
				} else {
					reqdata[$(targetobj).attr('id')] = $(targetobj).val();
				}				
			} else {
				$(targetobj).each(function() {
					if (typeof $(this).attr('id') != 'undefined') {
						
						if ($(this).hasClass("readonlyfield"))
							return false;
						
						if ($(this).is('[step]')) {
							reqdata[$(this).attr('id')] = $(this).val()/$(this).attr('step');
						} else {
							reqdata[$(this).attr('id')] = $(this).val();
						}
					}
				});			
			}
		} else {
			$("input.isfield, select.isfield").each(function() {
				if (typeof $(this).attr('id') != 'undefined') {
					
					if ($(this).hasClass("readonlyfield"))
						return false;
					
					if ($(this).is('[step]')) {
						reqdata[$(this).attr('id')] = $(this).val()/$(this).attr('step');
					} else {
						reqdata[$(this).attr('id')] = $(this).val();
					}
				}
			});	
		}
	}
	
	if (typeof targetobj == 'undefined') {
		res = processobj(targetobj)
		if ($.isPlainObject(res))
			return res
		
	} else if ((!$(targetobj).is("select")) && ($.isArray(targetobj) || targetobj.length>1)) {
		$.each(targetobj, function(key, val) {
			processobj(val)
		})
	} else {
		res = processobj(targetobj)
		if ($.isPlainObject(res))
			return res
	}
	
	return reqdata;
}

$.reqData = function(cmd, reqdata, targetobj, options) {
	if (typeof options === "undefined")
		options = "";
	
	var reqjson = '{"req":{"' + cmd + '":{';
	
	if ($.isEmptyObject(reqdata))
		return false;
	
	// --------------------------
	// casi particolari
	if (typeof reqdata["DIOM"] != 'undefined') {
		reqjson += '"DIOM":"' + reqdata["DIOM"] + '",';
		delete reqdata["DIOM"];
	}
	if (typeof reqdata["DIOC"] != 'undefined') {
		reqjson += '"DIOC":"' + reqdata["DIOC"] + '",';
		delete reqdata["DIOC"];		
	}		

	// --------------------------
	
	$.each(reqdata, function( index, value ) {
		if (!$.isPlainObject(value))
			reqjson += '"' + index + '":"' + value + '",';
		else {
			reqjson = reqjson.slice(0, -1);
			reqjson += $.stringify(reqdata);
			reqjson = reqjson.slice(0, -1);
			reqjson += ",";
		}
	});
	
	reqjson = reqjson.slice(0, -1);
	reqjson += "}}}";
	
	//Avoid requests to the Fw 
	//return;
	
	$("body").addClass('ui-disabled');
	$.mobile.loading('show');	

	if (constBoard) {
		var myurl = "jsondata";
		var mydata = "json=" + reqjson;
		var mycontentType = "application/json"; // send as JSON
	} else {
		var myurl = "jsondata.php";
		var mydata = {json:reqjson};
		var mycontentType = 'application/x-www-form-urlencoded; charset=UTF-8';
	}	
	
	var next = "";
	var tracklist = "";
	var request = $.ajax({
	  url: myurl,
	  method: "POST",
	  contentType: mycontentType,
	  data: mydata
	});

	request.done(function(indata) {
		try {
			data = JSON.parse(indata);
		} catch(e) {
			console.log(e + "\n" + indata); // error in the above string (in this case, yes)!
			alert("Json error");
		}		
		//data = $.parseJSON(data);
		$.each(data, function( index, value ) {
			if (index=="res") {
				$.each(value, function( index, value ) {
					if (index=="get") {
						$.each(value, function( index, value ) {
							switch (index) {
								case "MUSIC_ITEMS":
								case "SPOT_ITEMS":
								case "SENSOR_ITEMS":
								case "PL_ITEMS":
								case "USB_AUDIO_ITEMS":
									tracklist = index;									
									break;	
								case "NEXT":
									next = value;
									break;
							}
							$.parseResp('#' + index, value);														
						});
					}
				});				
			}
		});
		
		// idle timer
		clearTimeout(idleTimer);
		idleTimer = setTimeout(function() {goLogout()}, constIDLE_TIMEOUT);
		
		if (((cmd=="set") && (options != "silent")) || (options=="toast"))
			$.toast("Done");
	});

	request.error(function(data) {
		$.toast("Error on 'reqData()'");
	});

	request.always(function() {
		if (next!="") {
			var data = {};
			data[tracklist] = '';
			data['NEXT'] = next;
			$.reqData("get", data);
		} else {
			$.mobile.loading('hide');
			$("body").removeClass('ui-disabled');
		}
	});
	return request;
}

$.reqGetData = function(targetobj, options) {	
	return $.reqData("get", $.getReqFields(targetobj), targetobj, options);
}

$.reqSetData = function(targetobj, options) {
	return $.reqData("set", $.setReqFields(targetobj), targetobj, options);
}
