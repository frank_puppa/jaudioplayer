<?php

$di = new RecursiveDirectoryIterator('.');
foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
	if (strpos($filename, '-')) {
		if (rename ($filename , strtr($filename, '-', '_')))
			echo strtr($filename, '-', '_').'<br/>';
	}
	if ((strpos($filename, '.css')) || (strpos($filename, '.js'))) {
		$strfile = file_get_contents($filename);
		preg_match_all("#/[\w-]+/[\w-]+(.png)|(.gif)#", $strfile, $out, PREG_PATTERN_ORDER);
		if (empty($out[0]))
			continue;
		echo $filename.'<br/>';
		
		foreach ($out[0] as $value) {
				$patterns[] = "#".$value."#";
				$replacements[] = strtr($value, '-', '_');				
		}
		$patterns[] = "#ajax-loader.gif#";
		$replacements[] = "ajax_loader.gif";
		
				
		file_put_contents ($filename, preg_replace($patterns, $replacements, $strfile));	
		echo '--> '.$filename.'<br/>';		
		//echo "<textarea rows=60 cols=60>".urldecode(preg_replace($patterns, $replacements, $strfile))."</textarea>";
	}
}

?>