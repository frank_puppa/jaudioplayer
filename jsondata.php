<?php

if (!empty($_FILES)) {
	die(json_encode($_FILES));
}



$data = json_decode($_REQUEST['json'], true);
file_put_contents("data.json", $_REQUEST['json']);
$datareq = $data['req'];

usleep (5000);

if (isset($datareq['get'])) {
	$dataget = $datareq['get'];

	if (isset($dataget['LOGINTYPE'])) {
		$dataget['LOGINTYPE'] = "admin"; // guest, admin, none
	}	
	
	if (isset($dataget['USBSTATUS'])) {
		$dataget['USBSTATUS'] = "1"; // 1 present, 0 not present, 2 busy
	}
	
	if (isset($dataget['SDPRESENT'])) {
		$dataget['SDPRESENT'] = "1"; // 1 ok, 0 error
	}	

	if (isset($dataget['SDSIZE'])) {
		$dataget['SDSIZE'] = "1024"; // in mb
	}	

	if (isset($dataget['SDFREE'])) {
		$dataget['SDFREE'] = "50"; // in mb
	}	

	
	if (isset($dataget['DEVFN'])) {
		$dataget['DEVFN'] = "4"; // 1 playlist, 4 scheduler
	}
	if (isset($dataget['DIOM'])) {
		$dataget['DIOM'] = "5";
	}	
	/*
			<option value="0">Free</option>
			<option value="1">Player</option>
			<option value="2">Binary Code</option>
			<option value="3">Playlist direct</option>
			<option value="4">Keypad</option>
			<option value="5">Museum mode</option>
			<option value="6">Priority message</option>				
	
	*/
	
	// # bits for binary code
	if (isset($dataget['DIBC'])) {
		$dataget['DIBC'] = "5";
	}	
	
	// [secondi, genere, autore, titolo]
	
	
	if (isset($dataget['MUSIC_ITEMS'])) {
		//$dataget['MUSIC_ITEMS'] = '[{"sbarabin.MP3": [135, 5]}, {"sbarabaus.MP3": [145, 5]}, {"000066.eyeyerye.MP3": [12, 2]}, {"pinco pallino.MP3": [265, 4]}, {"67_eryeryery.MP3": [156, 7]}, {"page:"} ]';
		 $dataget['MUSIC_ITEMS'] = '[{"60 USA J.Walkùp.mp3": [34,3, "autore1", "titolo1", "album1"]}, {"Cràig_USA.mp3": [43,2, "autore2", "titolo2", "album1"]}, {"Jinglè_Bellissima.mp3": [43,2, "autore3", "titolo3", "album1"]}, {"pallino.MP3": [43,2, "autore1", "titolo4", "album1"]}, {"pinco.MP3": [43,2, "autore2", "titolo5", "album1"]}, {"Jingle_Principessa.mp3": [34,3, "autore5", "titolo6", "album1"]}, {"bbbbbblabla.mp3": [34,3, "autore3", "titolo7", "album1"]}, {"blabla.mp3": [34,3, "autore1", "titolooo", "album1"]}, {"blabla2.mp3": [35,1, "autore4", "titolo dfd dfd f", "album3"]}, {"blabla3.mp3": [56,6, "autore5", "titolo fdsfa ds afsda", "album21"]} ]';
		 //$dataget['NEXT'] = '3746976';
	}

	if (isset($dataget['PL_ITEMS'])) {
		//$dataget['PL_ITEMS'] = '[]';
		$dataget['PL_ITEMS'] = '[{"THE POLICE HITS 01.m3u": [1442, "nota1"]}, {"CULTURE CLUB 01.m3u": [1869, "nota2"]}, {"playlist1.m3u": [1748, "aaaa"]}]';
	}


	if (isset($dataget['SPOT_ITEMS'])) {
		$dataget['SPOT_ITEMS'] = '[{"60 USA J.Walkup spot.mp3": [135, 5, "autore1", "titolod dddserhg", "album1"]}, {"Craig_USA spot.mp3": [145, 5, "autore6", "titolo 453 546 2", "album1"]}, {"000066.spot3.MP3": [12, 2, "autore1", "titolo987n gfh fg", "album2"]}, {"spot4 pallino.MP3": [265, 4, "autore7", "titolo 675hj gh e", "album4"]}, {"spot5.MP3": [156, 7, "autore11", "titolo654 f dgr5y 54t3", "album1"]} ]';
	}

	if (isset($dataget['SENSOR_ITEMS'])) {
		$dataget['SENSOR_ITEMS'] = '[{"sens1.MP3": [135, 5, "autore1", "titolo  g4t 4 4", "album1"]}, {"sens2.MP3": [145, 5, "autore3", "titolo 57 6 4567 4567 ", "album1"]}, {"000066.sens3.MP3": [12, 2, "autore1", "titolotyuy tyt ", "album1"]}, {"sens4 pallino.MP3": [265, 4, "autore5", "titolo5 5673456 734", "album1"]}, {"sens5.MP3": [156, 7, "autore2", "titolo1", "album1"]} ]';
	}


	if (isset($dataget['BRANDID'])) {
		$dataget['BRANDID'] = "mio brandid";
	}
	if (isset($dataget['SWVER'])) {
		$dataget['SWVER'] = "mio swver";
	}
	if (isset($dataget['HWVER'])) {
		$dataget['HWVER'] = "mio hwver";
	}
	if (isset($dataget['BLVER'])) {
		$dataget['BLVER'] = "mio blver";
	}
	if (isset($dataget['ETHDHCP'])) {
		$dataget['ETHDHCP'] = "0";
	}
	if (isset($dataget['ETHIPAD'])) {
		$dataget['ETHIPAD'] = "192.168.0.12";
	}
	if (isset($dataget['ETHNM'])) {
		$dataget['ETHNM'] = "255.255.255.0";
	}
	if (isset($dataget['ETHGAD'])) {
		$dataget['ETHGAD'] = "192.168.0.1";
	}
	if (isset($dataget['ETHHN'])) {
		$dataget['ETHHN'] = "hostname";
	}
	if (isset($dataget['MACADDR'])) {
		$dataget['MACADDR'] = "78:25:44:77:5D:FF";
	}
	if (isset($dataget['ETHDNS1'])) {
		$dataget['ETHDNS1'] = "8.8.8.8";
	}	
	if (isset($dataget['ETHDNS2'])) {
		$dataget['ETHDNS2'] = "10.10.8.8";
	}	
	
	if (isset($dataget['COMBR'])) {
		$dataget['COMBR'] = "4";
	}
	if (isset($dataget['COMDB'])) {
		$dataget['COMDB'] = "8";
	}
	if (isset($dataget['COMDB'])) {
		$dataget['COMSB'] = "1";
	}
	if (isset($dataget['COMPB'])) {
		$dataget['COMPB'] = "0";
	}
	if (isset($dataget['COMCK'])) {
		$dataget['COMCK'] = "1";
	}
	if (isset($dataget['COMLFCR'])) {
		$dataget['COMLFCR'] = "0";
	}
	if (isset($dataget['COMTO'])) {
		$dataget['COMTO'] = "20";
	}
	if (isset($dataget['COMDL'])) {
		$dataget['COMDL'] = "20";
	}

	if (isset($dataget['COMPT'])) {
		$dataget['COMPT'] = "1";
	}
	if (isset($dataget['COMIDDEV'])) {
		$dataget['COMIDDEV'] = "3";
	}

	if (isset($dataget['AUADMIXON'])) {
		$dataget['AUADMIXON'] = "1";
	}
	if (isset($dataget['AUPCMON'])) {
		$dataget['AUPCMON'] = "0";
	}
	if (isset($dataget['DIOC'])) {
		$dataget['DIOC'] = "195";
	}



	if (isset($dataget['SENSON'])) {
		$dataget['SENSON'] = "1";
	}
	if (isset($dataget['SENSDEL'])) {
		$dataget['SENSDEL'] = "34";
	}
	if (isset($dataget['SENSAPT'])) {
		$dataget['SENSAPT'] = "1256";
	}
	if (isset($dataget['SENSVAL'])) {
		$dataget['SENSVAL'] = "1";
	}
	if (isset($dataget['SENSTO'])) {
		$dataget['SENSTO'] = "1123";
	}
	if (isset($dataget['SENSST'])) {
		$dataget['SENSST'] = "2123";
	}


	if (isset($dataget['RELM'])) {
		$dataget['RELM'] = "3";
	}
	if (isset($dataget['RELSET'])) {
		$dataget['RELSET'] = "1";
	}
	if (isset($dataget['RELINV'])) {
		$dataget['RELINV'] = "1";
	}
	if (isset($dataget['RELADV'])) {
		$dataget['RELADV'] = "254";
	}


	if (isset($dataget['AUVOL'])) {
		$dataget['AUVOL'] = "-24";
	}
	if (isset($dataget['LPLAY'])) {
		$dataget['LPLAY'] = "1";
	}
	if (isset($dataget['RPLAY'])) {
		$dataget['RPLAY'] = "1";
	}
	if (isset($dataget['POAP'])) {
		$dataget['POAP'] = "1";
	}
	if (isset($dataget['PLDEL'])) {
		$dataget['PLDEL'] = "987";
	}
	if (isset($dataget['DITO'])) {
		$dataget['DITO'] = "97";
	}
	if (isset($dataget['DIRP'])) {
		$dataget['DIRP'] = "1";
	}

	if (isset($dataget['FDIN'])) {
		$dataget['FDIN'] = "100";
	}
	if (isset($dataget['FDOUT'])) {
		$dataget['FDOUT'] = "250";
	}
	if (isset($dataget['CRFDI'])) {
		$dataget['CRFDI'] = "300";
	}
	if (isset($dataget['CRFDO'])) {
		$dataget['CRFDO'] = "100";
	}	
	
	if (isset($dataget['STBM'])) {
		$dataget['STBM'] = "2";
	}	
	if (isset($dataget['STBTO'])) {
		$dataget['STBTO'] = "200";
	}	
	
	
	if (isset($dataget['AUVOL'])) {
		$dataget['AUVOL'] = "12";
	}
	if (isset($dataget['AUM'])) {
		$dataget['AUM'] = "2";
	}
	if (isset($dataget['AULINL'])) {
		$dataget['AULINL'] = "-21";
	}
	if (isset($dataget['AULINL2'])) {
		$dataget['AULINL2'] = "-22";
	}
	if (isset($dataget['AUMIXC'])) {
		$dataget['AUMIXC'] = "1";
	}
	if (isset($dataget['AUOT'])) {
		$dataget['AUOT'] = "1";
	}
	if (isset($dataget['AUOI'])) {
		$dataget['AUOI'] = "1";
	}
	if (isset($dataget['AUBA'])) {
		$dataget['AUBA'] = "5";
	}
	if (isset($dataget['AUBF'])) {
		$dataget['AUBF'] = "2";
	}
	if (isset($dataget['AUTA'])) {
		$dataget['AUTA'] = "-2";
	}
	if (isset($dataget['AUTF'])) {
		$dataget['AUTF'] = "2";
	}
	if (isset($dataget['AUTF'])) {
		$dataget['AUTF'] = "2";
	}
	if (isset($dataget['AUEQ5ON'])) {
		$dataget['AUEQ5ON'] = "0";
	}
	if (isset($dataget['AUEQ5L1'])) {
		$dataget['AUEQ5L1'] = "1";
	}
	if (isset($dataget['AUEQ5F1'])) {
		$dataget['AUEQ5F1'] = "97";
	}
	if (isset($dataget['AUEQ5L2'])) {
		$dataget['AUEQ5L2'] = "24";
	}
	if (isset($dataget['AUEQ5F2'])) {
		$dataget['AUEQ5F2'] = "324";
	}
	if (isset($dataget['AUEQ5L3'])) {
		$dataget['AUEQ5L3'] = "-12";
	}
	if (isset($dataget['AUEQ5F3'])) {
		$dataget['AUEQ5F3'] = "2612";
	}
	if (isset($dataget['AUEQ5L4'])) {
		$dataget['AUEQ5L4'] = "-7";
	}
	if (isset($dataget['AUEQ5F4'])) {
		$dataget['AUEQ5F4'] = "7485";
	}
	if (isset($dataget['AUEQ5L5'])) {
		$dataget['AUEQ5L5'] = "-2";
	}



	if (isset($dataget['DICP'])) {
		$dataget['DICP'] = "1";
	}
	if (isset($dataget['DIRP'])) {
		$dataget['DIRP'] = "1";
	}
	if (isset($dataget['DIINT'])) {
		$dataget['DIINT'] = "1";
	}


	if (isset($dataget['DIOIM'])) {
		$dataget['DIOIM'] = "0";
	}

	if (isset($dataget['DOPT1'])) {
		$dataget['DOPT1'] = "10";
	}
	if (isset($dataget['DOPT2'])) {
		$dataget['DOPT2'] = "10";
	}
	if (isset($dataget['DOPT3'])) {
		$dataget['DOPT3'] = "10";
	}
	if (isset($dataget['DOPT4'])) {
		$dataget['DOPT4'] = "10";
	}
	if (isset($dataget['DOPT5'])) {
		$dataget['DOPT5'] = "20";
	}
	if (isset($dataget['DOPT6'])) {
		$dataget['DOPT6'] = "20";
	}
	if (isset($dataget['DOPT7'])) {
		$dataget['DOPT7'] = "10";
	}
	if (isset($dataget['DOPT8'])) {
		$dataget['DOPT8'] = "10";
	}

	if (isset($dataget['DOPM'])) {
		$dataget['DOPM'] = "16";
	}

	if (isset($dataget['DOM1'])) {
		$dataget['DOM1'] = "0";
	}
	if (isset($dataget['DOM2'])) {
		$dataget['DOM2'] = "0";
	}
	if (isset($dataget['DOM3'])) {
		$dataget['DOM3'] = "0";
	}
	if (isset($dataget['DOM4'])) {
		$dataget['DOM4'] = "0";
	}
	if (isset($dataget['DOM5'])) {
		$dataget['DOM5'] = "1";
	}
	if (isset($dataget['DOM6'])) {
		$dataget['DOM6'] = "1";
	}
	if (isset($dataget['DOM7'])) {
		$dataget['DOM7'] = "2";
	}
	if (isset($dataget['DOM8'])) {
		$dataget['DOM8'] = "2";
	}

	if (isset($dataget['DOSET8'])) {
		$dataget['DOSET8'] = "0";
	}
	if (isset($dataget['DOSET1'])) {
		$dataget['DOSET1'] = "0";
	}
	if (isset($dataget['DOSET2'])) {
		$dataget['DOSET2'] = "0";
	}
	if (isset($dataget['DOSET3'])) {
		$dataget['DOSET3'] = "0";
	}
	if (isset($dataget['DOSET4'])) {
		$dataget['DOSET4'] = "0";
	}
	if (isset($dataget['DOSET5'])) {
		$dataget['DOSET5'] = "0";
	}
	if (isset($dataget['DOSET6'])) {
		$dataget['DOSET6'] = "0";
	}
	if (isset($dataget['DOSET7'])) {
		$dataget['DOSET7'] = "0";
	}
	if (isset($dataget['DOSET8'])) {
		$dataget['DOSET8'] = "0";
	}
	if (isset($dataget['DISTS'])) {
		$dataget['DISTS'] = "126";
	}	

	
	if (isset($dataget['RTCCWD'])) {
		$dataget['RTCCWD'] = "2";
	}	
	
	if (isset($dataget['RTCCD'])) {
		$dataget['RTCCD'] = "5";
	}	
	if (isset($dataget['RTCCM'])) {
		$dataget['RTCCM'] = "11";
	}	
	if (isset($dataget['RTCCY'])) {
		$dataget['RTCCY'] = "2018";
	}	
	if (isset($dataget['RTCCHH'])) {
		$dataget['RTCCHH'] = "23";
	}	
	if (isset($dataget['RTCCMM'])) {
		$dataget['RTCCMM'] = "59";
	}	
	if (isset($dataget['RTCCSS'])) {
		$dataget['RTCCSS'] = "35";
	}	
	
	
	if (isset($dataget['EXTCHAR'])) {
		$dataget['EXTCHAR'] = "0";
	}	
	
	if (isset($dataget['NTPEN'])) {
		$dataget['NTPEN'] = "1";
	}	
	if (isset($dataget['TIMEZONE'])) {
		$dataget['TIMEZONE'] = "40,+01:00";
	}	
	if (isset($dataget['NTPADDR1'])) {
		$dataget['NTPADDR1'] = "0.pool.ntp.org";
	}	
	if (isset($dataget['NTPADDR2'])) {
		$dataget['NTPADDR2'] = "pincopallino";
	}
	if (isset($dataget['NTPSYNCPD'])) {
		$dataget['NTPSYNCPD'] = "3";
	}
	if (isset($dataget['NTPSYNCTM'])) {
		$dataget['NTPSYNCTM'] = "12:32";
	}	
	
	

	if (isset($dataget['SCHEDULER_ITEMS'])) {
		$dataget['SCHEDULER_ITEMS'] =[];
		$dataget['SCHEDULER_ITEMS']['ISE_DEMO0800']["options"]["schtype"] = 0;
		$dataget['SCHEDULER_ITEMS']['ISE_DEMO0800']["options"]["days"] = "";		
		
		$dataget['SCHEDULER_ITEMS']['scheduler1.json']["options"]["schtype"] = 1;
		$dataget['SCHEDULER_ITEMS']['scheduler1.json']["options"]["days"] = ",2,3,5,6,7,";
		//$dataget['SCHEDULER_ITEMS']['scheduler1.json']["elements"] = json_decode('[{"eltype":0,"st":"1:40:00","len":"1360","tit":"playlist1.m3u"},{"eltype":1,"st":"1:40:00","len":"120","tit":"spot1.MP3"},{"eltype":2,"st":"1:40:00","tit":"event1","type":0,"val":"0"}]', true);

		$dataget['SCHEDULER_ITEMS']['scheduler2.json']["options"]["schtype"] = 0;
		$dataget['SCHEDULER_ITEMS']['scheduler2.json']["options"]["days"] = "";
		//$dataget['SCHEDULER_ITEMS']['scheduler2.json']["elements"] = json_decode('[{"eltype":0,"st":"2:20:00","len":"2460","tit":"pincopall1.m3u"},{"eltype":1,"st":"1:40:00","len":"120","tit":"spotpinc1.MP3"},{"eltype":2,"st":"2:50:00","tit":"eventeee1","type":0,"val":"0"}]', true);

		$dataget['SCHEDULER_ITEMS']['1200']["options"]["schtype"] = 0;
		$dataget['SCHEDULER_ITEMS']['1200']["options"]["days"] = "";		
		

	}

	if (isset($dataget['SCHEDULER_ITEM_ELEM'])) {
		switch ($dataget['SCHEDULER_ITEM_ELEM']) {
			case "scheduler1.json":
				$dataget['SCHEDULER_ITEM_ELEM'] = json_decode('{"scheduler1.json": {"elements":[{"eltype":	0,"st":	"08:00:00","len":	31,"tit":	"test1.m3u"}, {"eltype":	2,"st":	"08:00:00","type":	1,"val":	1,"tit":	"","idx":	2}, {"eltype":	2,"st":	"08:00:00","type":	0,"val":	0,"tit":	"","idx":	0}, {"eltype":	2,"st":	"08:00:12","type":	1,"val":	0,"tit":	"","idx":	2}, {"eltype":	1,"st":	"08:00:20","len":	11,"tit":	"spot18.mp3"}, {"eltype":	2,"st":"08:00:37","type":	0,"val":	1,"tit":	"","idx":	0}]}}', true);
				break;
			case "scheduler2.json":
				$dataget['SCHEDULER_ITEM_ELEM'] = json_decode('{"scheduler2.json": {"elements":[{"eltype":	0,"st":	"08:00:00","len":	31,"tit":	"test1.m3u"}, {"eltype":	1,"st":	"08:00:20","len":	11,"tit":	"spot18.mp3"}]}}', true);
				break;
			case "ISE_DEMO0800":
				$dataget['SCHEDULER_ITEM_ELEM'] = json_decode('{"ISE_DEMO0800": {"elements":[{"eltype":1,"st":"08:00:00","len":167,"tit":"Parov Stelar - All Night.mp3"},{"eltype":2,"st":"08:00:05","type":1,"val":1,"tit":"ON Outdoor Lighting","idx":6},{"eltype":2,"st":"08:00:10","type":1,"val":1,"tit":"ON Live Room Lighting","idx":7},{"eltype":2,"st":"08:00:14","type":1,"val":1,"tit":"Power ON rack Amplifier","idx":8},{"eltype":2,"st":"08:00:30","type":1,"val":1,"tit":"Power ON rack Amplifier","idx":8},{"eltype":2,"st":"08:00:31","type":1,"val":1,"tit":"Power ON rack Amplifier","idx":8},{"eltype":2,"st":"08:00:32","type":1,"val":1,"tit":"Power ON rack Amplifier","idx":8},{"eltype":2,"st":"08:00:33","type":1,"val":1,"tit":"Power ON rack Amplifier","idx":8},{"eltype":2,"st":"08:00:34","type":1,"val":1,"tit":"Power ON rack Amplifier","idx":8},{"eltype":2,"st":"08:00:35","type":1,"val":1,"tit":"Power ON rack Amplifier","idx":8},{"eltype":2,"st":"08:00:36","type":1,"val":1,"tit":"Power ON rack Amplifier","idx":8},{"eltype":2,"st":"08:00:44","type":0,"val":1,"tit":"Door UNLock","idx":0},{"eltype":0,"st":"08:02:48","len":1442,"tit":"THE POLICE HITS 01.m3u"},{"eltype":1,"st":"08:04:00","len":16,"tit":"60 USA J.Walkup.mp3"},{"eltype":0,"st":"08:27:07","len":1869,"tit":"CULTURE CLUB 01.m3u"},{"eltype":1,"st":"08:30:00","len":10,"tit":"Craig_USA.mp3"},{"eltype":0,"st":"09:58:27","len":2894,"tit":"MICHAEL JACKSON 01.m3u"},{"eltype":1,"st":"09:59:44","len":135,"tit":"60 USA J.Walkup spot.mp3"},{"eltype":1,"st":"10:05:44","len":36,"tit":"Jingle_Bellissima.mp3"},{"eltype":1,"st":"10:10:00","len":44,"tit":"Jingle_Principessa.mp3"}]}}', true);
				break;
			case "1200":
				$dataget['SCHEDULER_ITEM_ELEM'] = json_decode('{"1200":{"options":{"schtype":0,"days":",02-03-2018,"},"elements":[{"eltype":0,"st":"12:00:01","len":4456,"tit":"THE POLICE HITS 01.m3u"},{"eltype":1,"st":"12:10:00","len":11,"tit":"Veronica.mp3"},{"eltype":1,"st":"12:20:00","len":11,"tit":"Veronica.mp3"},{"eltype":1,"st":"12:30:00","len":11,"tit":"Veronica.mp3"},{"eltype":1,"st":"12:40:00","len":11,"tit":"Veronica.mp3"},{"eltype":1,"st":"12:50:00","len":11,"tit":"Veronica.mp3"},{"eltype":0,"st":"13:15:13","len":4456,"tit":"THE POLICE HITS 01.m3u"},{"eltype":1,"st":"13:20:00","len":11,"tit":"Veronica.mp3"},{"eltype":1,"st":"13:30:00","len":11,"tit":"Veronica.mp3"},{"eltype":1,"st":"13:40:00","len":11,"tit":"Veronica.mp3"},{"eltype":1,"st":"13:50:00","len":11,"tit":"Veronica.mp3"},{"eltype":1,"st":"14:00:00","len":11,"tit":"Veronica.mp3"},{"eltype":1,"st":"14:10:00","len":11,"tit":"Veronica.mp3"}],"ITEM":"end"}}');
				break;
		}
	}
/*
	if (isset($dataget['SCHEDULER_ITEM_ELEM']['scheduler2.json'])) {
		$dataget['SCHEDULER_ITEM_ELEM']['scheduler2.json']["elements"] = json_decode('[{"eltype":0,"st":"4:40:00","len":"1360","tit":"playlist1.m3u"},{"eltype":1,"st":"1:20:00","len":"120","tit":"spot1.MP3"},{"eltype":2,"st":"2:40:00","tit":"event1","type":0,"val":"0"}]', true);
	}
	 */

 	if (isset($dataget['PL_ITEM_ELEM'])) {
		switch ($dataget['PL_ITEM_ELEM']) {
			case "THE POLICE HITS 01.m3u":
				$dataget['PL_ITEM_ELEM'] = json_decode('{"THE POLICE HITS 01.m3u": {"elements": [{"FILE_NAME": "pinco pallinò.MP3","FILE_TYPE": "music","id3tag": [34,3, "autore2", "titolo9"]}, {"FILE_NAME": "pinco pallinò.MP3","FILE_TYPE": "music","id3tag": [34,3, "autore2", "titolo9"]}, {"FILE_NAME": "pinco pallinò.MP3","FILE_TYPE": "music","id3tag": [34,3, "autore2", "titolo9"]}, {"FILE_NAME": "pinco pallinò.MP3","FILE_TYPE": "music","id3tag": [34,3, "autore2", "titolo9"]}, {"FILE_NAME": "pinco pallinò.MP3","FILE_TYPE": "music","id3tag": [34,3, "autore2", "titolo9"]}, {"FILE_NAME": "pinco pallinò.MP3","FILE_TYPE": "music","id3tag": [34,3, "autore2", "titolo9"]}, {"FILE_NAME": "pinco pallinò.MP3","FILE_TYPE": "music","id3tag": [34,3, "autore2", "titolo9"]}, {"FILE_NAME": "000066.eyeyerye.MP3","FILE_TYPE": "music","id3tag": [34,3, "autore2", "titolo9", "album1"]}, {"FILE_NAME": "blabla.mp3","FILE_TYPE": "music","id3tag": [34,3, "autore8", "titolo1", "album1"]}]}}', true);
				break;
			case "playlist2.m3u":
				$dataget['PL_ITEM_ELEM'] = json_decode('{"CULTURE CLUB 01.m3u": {"elements":[{"FILE_NAME": "000066.èyèyerye.MP3","FILE_TYPE": "music","id3tag": [34,3]}, {"FILE_NAME": "pinco pallino.MP3","FILE_TYPE": "music","id3tag": [34,3]}]}}', true);
				break;
			case "aaa.m3u":
				$dataget['PL_ITEM_ELEM'] = json_decode('{"aaa.m3u": {"elements":[{"FILE_NAME": "pinco pallino.MP3","FILE_TYPE": "music","id3tag": [34,3]}, {"FILE_NAME": "000066.èyèyerye.MP3","FILE_TYPE": "music","id3tag": [34,3]}]}}', true);
				break;
		}
	}

 	if (isset($dataget['USB_AUDIO_ITEMS'])) {
		$dataget['USB_AUDIO_ITEMS'] = json_decode('[{"bbbancò.mp3": [54, 4, "autore2", "titolo9", "album3"], "playlist5.m3u": [], "playlist4.m3u": [], "playlist3.m3u": [], "playlist2.m3u": [], "aààanco.mp3": [54, 4, "autore2", "titolo9", "album1"], "aààanco xyzino.MP3": [64, 2, "autore3", "titolo8", "album1"], "pinco xyzino.MP3": [24, 1, "autore2", "titolo5"], "pinco pallinooo.MP3": [33, 3, "autore", "titolo3", "album1"], "pinco pallino.MP3": [34, 3, "autore", "titolo", "album1"], "000066.eyeyerye.MP3": [43,2, "autore2", "titolo2", "album1"], "playlist.m3u": []}]', true);	
		//$dataget['NEXT'] = 1;
	}	
	
	if (isset($dataget['SENSORPLN'])) {
		$dataget['SENSORPLN'] = "playlist2.m3u";
	}	
	
	$data['res']['get'] = $dataget;
	unset($data['req']);
}
else if (isset($datareq['cmd'])) {
	$datacmd = $datareq['cmd'];
	if (isset($datacmd['COPY'])) {
		$datacmd['COPY'] = "ok";
	}
	
	$data['res']['cmd'] = $datacmd;
	unset($data['req']);	
}

header('Content-type: text/plain');
echo json_encode($data);
//echo '{"response": {"set": {"par1": "valore1", "par2": "valore2", "par3": "valore3"}}}';

//echo "{\"tracks\":{\"filename\":\"1.mp3\",\"artist\": \"Grayceon\",\"title\": \"Sounds Like Thunder\",\"duration\": \"4:13\"}}\0";

exit;
?>
